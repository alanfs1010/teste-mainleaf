﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressPoint : MonoBehaviour
{
    private void CallNextStage()
    {
        PlayerInformations.completedFirstPuzzle = true;
        GameManager.Instance.LoadNextScene();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer.Equals(LayerMask.NameToLayer("Player")))
        {
            CallNextStage();
        }
    }
}
