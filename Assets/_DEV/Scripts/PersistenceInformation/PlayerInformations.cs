﻿using MainLeaf.SaveSystem;

[System.Serializable]
public class Informations
{
    public int coins;

    public Informations()
    {
        coins = 0;
    }
}

public static class PlayerInformations
{
    public static Informations informations = new Informations();
    public static bool completedFirstPuzzle = false;

    public static void SaveInformations()
    {
        SaveSystem.SaveData(informations);
    }

    public static void LoadInformations()
    {
        SaveSystem.LoadData(out informations);
    }

    public static void ResetLocalInformations()
    {
        informations = new Informations();
        completedFirstPuzzle = false;
    }

}
