﻿using UnityEngine;

namespace MainLeaf.SaveSystem
{
    public static class SaveSystem
    {
        const string KEY = "Informations";

        public static void SaveData<T>(T data)
        {
            PlayerPrefs.SetString(KEY, JsonUtility.ToJson(data));
        }

        public static void LoadData<T>(out T outPut)
        {
            if (HasSavedData())
            {
                outPut = JsonUtility.FromJson<T>(PlayerPrefs.GetString(KEY));
            }
            else
                outPut = default(T);
        }

        public static bool HasSavedData()
        {
            return PlayerPrefs.HasKey(KEY);
        }

        public static void DeleteAllSaveData()
        {
            PlayerPrefs.DeleteAll();
        }
    }
}