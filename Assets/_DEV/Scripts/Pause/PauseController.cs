﻿using UnityEngine;

public class PauseController : MonoBehaviour
{
    private void OnEnable()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void ResumeClick()
    {
        GameManager.Instance.UnpauseGame();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void RestartClick()
    {
        GameManager.Instance.RestartLevel();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void QuitGameClick()
    {
        Application.Quit();
    }
}
