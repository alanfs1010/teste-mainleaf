﻿public class Coin : Collectable
{
    protected override void OnValidate()
    {
        base.OnValidate();
        collectableType = CollectableType.COIN;
    }

    public override bool ReadyToCollect()
    {
        return true;
    }

    public override void TryToCollect()
    {
        PlayerInformations.informations.coins += information.CollectableValue;
        CoinUpdater.UpdateText();
        GameManager.Instance.PoolSystem.PutOnList(this);
    }
}
