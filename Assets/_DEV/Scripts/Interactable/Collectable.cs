﻿using UnityEngine;

public abstract class Collectable : MonoBehaviour, ICollectable
{
    [Header("Collectable id")]
    [SerializeField] protected int id;
    [SerializeField] protected CollectableType collectableType;
    [Header("Collectable Material Reference")]
    [SerializeField] protected Renderer collectableRenderer;
    protected CollectableConfiguration information;
    protected MaterialPropertyBlock propertyBlock;

    public CollectableType CollectableType { get { return collectableType; } }

    protected virtual void OnValidate()
    {
        if (collectableRenderer == null)
            collectableRenderer = GetComponent<Renderer>();
    }

    protected virtual void Start()
    {
        SetupCollectable();
    }

    protected virtual void SetupCollectable()
    {
        information = new CollectableConfiguration(GameManager.Instance.CollectableData.GetCollectableInformation(collectableType, id));
        propertyBlock = new MaterialPropertyBlock();
        collectableRenderer.GetPropertyBlock(propertyBlock);
        propertyBlock.SetColor("_Color", information.UseFixedColor ? information.FixedColor : GameManager.Instance.CollectableData.GetRandomColor());
        collectableRenderer.SetPropertyBlock(propertyBlock);
    }

    public abstract bool ReadyToCollect();

    public abstract void TryToCollect();
}
