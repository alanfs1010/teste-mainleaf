﻿using UnityEngine;
using UnityEngine.InputSystem;

public class Grabbable : InteractableBase
{
    [Header("destiny to reach")]
    [SerializeField] private GameObject destiny;
    [SerializeField] private Transform subTarget;
    private Player playerScript;

    public GameObject Destiny { get { return destiny; } }

    public Rigidbody GetRigidbody() => rigidBody;

    private void Start()
    {
        playerScript = ServiceLocator.Resolve<Player>();
        SetLayer();
        if (PlayerInformations.completedFirstPuzzle)
        {
            transform.position = subTarget != null ? subTarget.position : destiny.transform.position;
        }
    }

    private void FixedUpdate()
    {
        if (subTarget != null && reachDestiny)
        {
            if (transform.position.y != subTarget.position.y)
            {
                if (transform.position.y > subTarget.position.y)
                    transform.Translate(Vector3.down * Time.fixedDeltaTime);
                else
                    transform.position = subTarget.position;
            }
        }
    }

    public override void Interaction()
    {
        ChangeGrabState(true);
        playerScript.PlayerInput.UngrabAction += DisableInteraction;
    }

    public override void DisableInteraction(InputAction.CallbackContext ctx)
    {
        ChangeGrabState(false);
        playerScript.PlayerInput.UngrabAction -= DisableInteraction;
    }

    private void ChangeGrabState(bool state)
    {
        if (state)
        {
            playerScript.SetGrabbableToCurrent(this);
        }
        rigidBody.velocity = Vector3.zero;
        rigidBody.isKinematic = !state;
        playerScript.DuringPushing = state;
    }

    public void ReachDestiny()
    {
        reachDestiny = true;
        playerScript.PlayerInput.UngrabInput(new InputAction.CallbackContext());
        transform.position = destiny.transform.position;
    }
}
