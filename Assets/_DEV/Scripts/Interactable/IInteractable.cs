﻿using UnityEngine.InputSystem;
interface IInteractable
{
    void SetLayer();
    void TryInteraction();
    void Interaction();
    void DisableInteraction(InputAction.CallbackContext ctx);
    bool EnableToInteract();
}
