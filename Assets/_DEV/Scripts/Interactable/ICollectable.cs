﻿interface ICollectable 
{
    void TryToCollect();
    bool ReadyToCollect();
}
