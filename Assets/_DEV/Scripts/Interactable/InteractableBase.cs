﻿using UnityEngine;
using UnityEngine.InputSystem;

public abstract class InteractableBase : MonoBehaviour, IInteractable
{
    [SerializeField] protected Rigidbody rigidBody;
    [SerializeField] private string interactableLayer = "Interactable";
    [SerializeField] protected bool reachDestiny = false;

    public abstract void DisableInteraction(InputAction.CallbackContext ctx);

    public virtual bool EnableToInteract()
    {
        return rigidBody.velocity.y == 0 && !reachDestiny;
    }

    public abstract void Interaction();

    public void SetLayer()
    {
         gameObject.layer = LayerMask.NameToLayer(interactableLayer);
    }

    public virtual void TryInteraction()
    {
        if (EnableToInteract()) Interaction();
    }
}
