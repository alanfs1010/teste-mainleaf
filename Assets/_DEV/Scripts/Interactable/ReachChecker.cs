﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReachChecker : MonoBehaviour
{
    [SerializeField] private Grabbable grabbable;

    private void OnValidate()
    {
        if (grabbable == null)
            grabbable = transform.parent.GetComponent<Grabbable>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.Equals(grabbable.Destiny))
        {
            grabbable.ReachDestiny();
        }
    }
}
