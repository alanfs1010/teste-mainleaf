﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerCrounch : PlayerComponent
{
    [Header("Collider Types")]
    [SerializeField] private Collider normalCollider;
    [SerializeField] private Collider crounchCollider;
    public override void Initialize()
    {
        base.Initialize();
        player.PlayerInput.CrounchAction += ActivateCrounchState;
        player.PlayerInput.UnCrounchAction += DesactivateCrounchState;
    }

    private void OnEnable()
    {
        if (!Initialized && !firstEnable)
        {
            player.PlayerInput.CrounchAction += ActivateCrounchState;
            player.PlayerInput.UnCrounchAction += DesactivateCrounchState;
        }
    }

    private void OnDisable()
    {
        player.PlayerInput.CrounchAction -= ActivateCrounchState;
        player.PlayerInput.UnCrounchAction -= DesactivateCrounchState;
    }

    private void ActivateCrounchState(InputAction.CallbackContext ctx)
    {
        if (CanCrounch())
        {
            normalCollider.enabled = false;
            crounchCollider.enabled = true;
            player.UpdateCrounchState(true);
        }
    }

    private void DesactivateCrounchState(InputAction.CallbackContext ctx)
    {
        if (crounchCollider.enabled)
        {
            normalCollider.enabled = true;
            crounchCollider.enabled = false;
            player.UpdateCrounchState(false);
        }
    }

    private bool CanCrounch()
    {
        return player.GetCharacterRb().velocity.y == 0;
    }
}
