﻿using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(PlayerInput))]
public sealed class Player : CharacterBase
{
    [Header("Player Input Reference")]
    [SerializeField] private PlayerInput playerInput;

    private InputAction horizontalAction { get { return playerInput.InputMap.Moviment.HorizontalMoviment; } }
    private InputAction verticalAction { get { return playerInput.InputMap.Moviment.Vertical; } }
    private float horizontalVelocity { get { return horizontalAction.ReadValue<float>(); } }
    private float verticalVelocity { get { return verticalAction.ReadValue<float>(); } }
    //states
    private bool pushing = false;
    private bool crounched = false;
    //animator hashes
    private int pushingHash;
    private int crounchHash;

    private float timerToPush = 0;
    public bool MovimentPermission = false;

    private IPlayerComponent[] allComponents;
    private Grabbable currentGrab;

    public Grabbable CurrentGrab => currentGrab;
    public Transform PlayerModelTransform => characterRenderer.transform.parent;
    public bool DuringPushing { get { return pushing; } set { pushing = value; } }
    public PlayerInput PlayerInput { get { return playerInput; } }

    protected override void Awake()
    {
        base.Awake();
        ServiceLocator.Register<Player>(this);
        if (playerInput == null)
        {
            playerInput = GetComponent<PlayerInput>();
        }
        allComponents = GetComponents<IPlayerComponent>();
        foreach (IPlayerComponent component in allComponents)
        {
            component.Initialize();
        }
        pushingHash = Animator.StringToHash("Pushing");
        crounchHash = Animator.StringToHash("Crounch");
    }

    private void Update()
    {
        if (DuringPushing)
        {
            timerToPush += Time.deltaTime;
        }
    }

    protected override void WalkVerification()
    {
        if (!playerInput.InputMap.Moviment.enabled) return;

        if (DuringPushing)
        {
            if (timerToPush > (MovimentPermission ? .75f : .5f))
            {
                MovimentPermission = !MovimentPermission;
                timerToPush = 0;
            }
            if (MovimentPermission)
            {
                if (verticalVelocity > 0) ApplyForwardVelocity();
                else if (verticalVelocity < 0) ApplyBackwardVelocity();
            }
            else
            {
                ResetVelocity();
                CurrentGrab.GetRigidbody().velocity = Vector3.zero;
            }
            UpdateAnimator();
            return;
        }
        if (horizontalAction.phase.Equals(InputActionPhase.Waiting) && verticalAction.phase.Equals(InputActionPhase.Waiting))
        {
            ResetVelocity();
            walking = false;
            StabilizeRotationAngle();
        }
        else
        {
            walking = true;
            base.WalkVerification();
        }
    }

    protected override void UpdateRotation()
    {
        if (DuringPushing) return;
        Quaternion directionRot = Quaternion.Euler(0f, Mathf.Rad2Deg * Mathf.Atan2(horizontalVelocity, verticalVelocity), 0f);
        directionRot = directionRot * Camera.main.transform.rotation;
        directionRot.x = 0;
        directionRot.z = 0;
        PlayerModelTransform.rotation = directionRot;
    }

    protected override void UpdateAnimator()
    {
        base.UpdateAnimator();
        characterAnimator.SetFloat(pushingHash, DuringPushing ? Mathf.Abs(verticalVelocity) : 0); // during grab == 1
        characterAnimator.SetBool(crounchHash, crounched);
    }

    protected override void ApplyBackwardVelocity()
    {
        if (!DuringPushing)
        {
            base.ApplyBackwardVelocity();
        }
        else
        {
            CurrentGrab.GetRigidbody().velocity = (characterRenderer.transform.parent.forward * (velocity * Time.fixedDeltaTime)) * -1;
            base.ApplyBackwardVelocity();
        }
    }

    protected override void ApplyForwardVelocity()
    {
        if (!DuringPushing)
        {
            base.ApplyForwardVelocity();
        }
        else
        {
            CurrentGrab.GetRigidbody().velocity = characterRenderer.transform.parent.forward * (velocity * Time.fixedDeltaTime);
            base.ApplyForwardVelocity();
        }
    }

    public void SetGrabbableToCurrent(Grabbable current)
    {
        currentGrab = current;
        StabilizeRotationAngle();
    }

    public void StabilizeRotationAngle()
    {
        Vector3 directionRot = Quaternion.identity.eulerAngles;
        if (PlayerModelTransform.localEulerAngles.y > 0)
        {
            if (PlayerModelTransform.localEulerAngles.y > 45)
            {
                if (PlayerModelTransform.localEulerAngles.y < 135)
                {
                    directionRot.y = 90;
                }
                else if (PlayerModelTransform.localEulerAngles.y < 225)
                {
                    directionRot.y = 180;
                }
                else if (PlayerModelTransform.localEulerAngles.y < 315)
                {
                    directionRot.y = 270;
                }
            }
        }
        else
        {
            if (PlayerModelTransform.localEulerAngles.y < -45)
            {
                if (PlayerModelTransform.localEulerAngles.y > -135)
                {
                    directionRot.y = -90;
                }
                else if (PlayerModelTransform.localEulerAngles.y > -225)
                {
                    directionRot.y = -180;
                }
                else if (PlayerModelTransform.localEulerAngles.y > -315)
                {
                    directionRot.y = -270;
                }
            }
        }
        PlayerModelTransform.localRotation = Quaternion.Euler(directionRot);
    }

    private void ResetVelocity()
    {
        Vector3 velo = Vector3.zero;
        velo.y = GetCharacterRb().velocity.y;
        GetCharacterRb().velocity = velo;
    }

    public void UpdateCrounchState(bool state)
    {
        crounched = state;
    }

}