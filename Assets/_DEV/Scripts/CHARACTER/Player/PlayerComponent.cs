﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerComponent : MonoBehaviour, IPlayerComponent
{
    public bool Initialized { get; private set; }
    protected Player player;
    protected bool firstEnable = true;

    public virtual void Initialize()
    {
        player = ServiceLocator.Resolve<Player>();
        Initialized = true;
        firstEnable = false;
    }
}
