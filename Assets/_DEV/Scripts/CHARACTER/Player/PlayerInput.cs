﻿using UnityEngine;
using System;
using UnityEngine.InputSystem;

public class PlayerInput : PlayerComponent
{
    public InputSettings InputMap { get; private set; }

    public event Action<InputAction.CallbackContext> JumpAction;
    public event Action<InputAction.CallbackContext> CrounchAction;
    public event Action<InputAction.CallbackContext> UnCrounchAction;
    public event Action<InputAction.CallbackContext> GrabAction;
    public event Action<InputAction.CallbackContext> UngrabAction;

    public override void Initialize()
    {
        base.Initialize();
        EnableInput();
    }

    #region Input System

    private void OnDisable()
    {
        DisableInput();
    }

    private void EnableInput()
    {
        InputMap = new InputSettings();
        InputMap.Interaction.BasicInteraction.started += GrabInput;
        InputMap.Interaction.BasicInteraction.canceled += UngrabInput;
        InputMap.Moviment.Crounch.started += CrounchInput;
        InputMap.Moviment.Crounch.canceled += UncrounchInput;
        InputMap.Moviment.Jump.performed += JumpInput;
        InputMap.Interaction.Enable();
        InputMap.Moviment.Enable();
    }

    private void DisableInput()
    {
        InputMap.Interaction.BasicInteraction.started -= GrabInput;
        InputMap.Interaction.BasicInteraction.canceled -= UngrabInput;
        InputMap.Moviment.Crounch.started -= CrounchInput;
        InputMap.Moviment.Crounch.canceled -= UncrounchInput;
        InputMap.Moviment.Jump.performed -= JumpInput;
        InputMap.Interaction.Disable();
        InputMap.Moviment.Disable();
    }

    private void CrounchInput(InputAction.CallbackContext ctx) { CrounchAction?.Invoke(ctx); }
    private void UncrounchInput(InputAction.CallbackContext ctx) { UnCrounchAction?.Invoke(ctx); }
    private void JumpInput(InputAction.CallbackContext ctx) { JumpAction?.Invoke(ctx); }
    private void GrabInput(InputAction.CallbackContext ctx) { GrabAction?.Invoke(ctx); }
    public void UngrabInput(InputAction.CallbackContext ctx) { UngrabAction?.Invoke(ctx); }

    #endregion

}
