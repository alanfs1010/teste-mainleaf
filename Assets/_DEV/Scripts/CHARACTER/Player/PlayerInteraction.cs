﻿using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInteraction : PlayerComponent
{
    [Header("Interaction Options")]
    public LayerMask InteractionLayer;

    private Collider[] grabPossibilities;

    public override void Initialize()
    {
        base.Initialize();
        player.PlayerInput.GrabAction += GrabAction;
        player.PlayerInput.UngrabAction += UngrabAction;
    }

    private void OnEnable()
    {
        if(!Initialized && !firstEnable)
        {
            player.PlayerInput.GrabAction += GrabAction;
            player.PlayerInput.UngrabAction += UngrabAction;
        }
    }

    private void OnDisable()
    {
        player.PlayerInput.GrabAction -= GrabAction;
        player.PlayerInput.UngrabAction -= UngrabAction;
    }

    private void GrabAction(InputAction.CallbackContext ctx)
    {
        grabPossibilities = Physics.OverlapSphere(transform.position + player.PlayerModelTransform.forward, 0.25f, InteractionLayer);
        if (grabPossibilities != null && grabPossibilities.Length > 0)
        {
            for(int i =0; i< grabPossibilities.Length; i++)
            {
                if(grabPossibilities[i].TryGetComponent(out IInteractable obj))
                {
                    obj.TryInteraction();
                    break;
                }
            }
        }
    }

    private void UngrabAction(InputAction.CallbackContext ctx)
    {
        // if is anything grabbed, do the ungrab act's
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<ICollectable>(out ICollectable collectable))
            collectable.TryToCollect();
    }
}
