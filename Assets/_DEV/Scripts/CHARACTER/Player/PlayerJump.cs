﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerJump : PlayerComponent
{
    [SerializeField] private float jumpForce = 5;
    [SerializeField] private float jumpForceHigh = 8;
    [Header("Custom Option's")]
    [SerializeField] private PlayerInteraction playerInteraction;
    public override void Initialize()
    {
        base.Initialize();
        player.PlayerInput.JumpAction += Jump;
    }

    private void OnEnable()
    {
        if (!Initialized && !firstEnable)
        {
            player.PlayerInput.JumpAction += Jump;
        }
    }

    private void OnDisable()
    {
        player.PlayerInput.JumpAction -= Jump;
    }

    private void Jump(InputAction.CallbackContext ctx)
    {
        if (CanJump())
        {
            if (playerInteraction != null && HasGrabPossibilities())
                player.GetCharacterRb().AddForce(Vector3.up * jumpForceHigh, ForceMode.Impulse);
            else
                player.GetCharacterRb().AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    }

    public bool HasGrabPossibilities()
    {
        return Physics.OverlapSphere(transform.position + player.PlayerModelTransform.forward, 0.25f, playerInteraction.InteractionLayer).Length > 0;
    }

    private bool CanJump()
    {
        return !player.DuringPushing && Mathf.Abs(player.GetCharacterRb().velocity.y) < 0.1f;
    }

}