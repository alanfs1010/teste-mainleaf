﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPoint : MonoBehaviour
{
    [SerializeField] private Collider[] ignoreColliders;

    private void Awake()
    {
        Collider col = GetComponent<Collider>();
        foreach (Collider ignore in ignoreColliders)
            Physics.IgnoreCollision(ignore, col);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out EnemyBehaviour enemy))
        {
            if (enemy.CheckCorrectTarget(this))
                enemy.ReachTarget();
        }
    }
}
