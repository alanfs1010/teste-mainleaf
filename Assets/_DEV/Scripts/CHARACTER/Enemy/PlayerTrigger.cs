﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTrigger : MonoBehaviour
{
    [SerializeField] private LayerMask layerIgnore;
    [SerializeField] private Transform rayOrigin;

    private void OnTriggerEnter(Collider other)
    {
        if (GameManager.Instance.IsGameRunning())
        {
            if (other.gameObject.layer.Equals(LayerMask.NameToLayer("Player")))
            {
                if (Physics.Raycast(new Ray(rayOrigin.position, other.gameObject.transform.position - rayOrigin.position), out RaycastHit hit, 10, layerIgnore))
                {
                    if (hit.collider.gameObject.layer.Equals(LayerMask.NameToLayer("Player")))
                    {
                        GameManager.Instance.CallEndGame("Você foi visto! Tente novamente.");
                    }
                }
            }
        }
    }
}
