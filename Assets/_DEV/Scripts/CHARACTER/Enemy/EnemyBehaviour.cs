﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : CharacterBase, IHealthStats
{
    [SerializeField] private Transform[] targetPoints;
    [SerializeField] private int timeInPatrol;
    private int currentTarget;
    private bool waiting = false;

    protected override void Awake()
    {
        base.Awake();
        int current = 0;
        targetPoints[0].position = new Vector3(targetPoints[0].position.x, transform.position.y, targetPoints[0].position.z);
        for (int i = 1; i < targetPoints.Length; i++)
        {
            targetPoints[i].position = new Vector3(targetPoints[i].position.x, transform.position.y, targetPoints[i].position.z);
            if (Vector3.Distance(transform.position, targetPoints[i].position) < Vector3.Distance(transform.position, targetPoints[current].position))
                current = i;
        }
        currentTarget = current;
    }

    protected override void WalkVerification()
    {
        UpdateRotation();
        if (!waiting)
        {
            ApplyForwardVelocity();
        }
    }

    public bool CheckCorrectTarget(TargetPoint target)
    {
        for(int i = 0; i < targetPoints.Length; i++)
        {
            if(targetPoints[i].Equals(target.transform))
                return true;
        }
        return false;
    }

    public void ReachTarget()
    {
        waiting = true;
        GetCharacterRb().velocity = Vector3.zero;
        SetNextTarget();
    }

    async void SetNextTarget()
    {
        await System.Threading.Tasks.Task.Delay(1000 * timeInPatrol);
        currentTarget++;
        if (currentTarget >= targetPoints.Length) currentTarget = 0;
        waiting = false;
    }

    protected override void UpdateRotation()
    {
        targetPoints[currentTarget].position = new Vector3(targetPoints[currentTarget].position.x, transform.position.y, targetPoints[currentTarget].position.z);
        transform.LookAt(targetPoints[currentTarget]);
    }
}
