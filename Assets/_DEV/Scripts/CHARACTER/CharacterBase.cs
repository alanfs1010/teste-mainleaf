﻿using UnityEngine;

public abstract class CharacterBase : MonoBehaviour
{
    [Header("Reference's")]
    [SerializeField] protected Rigidbody characterRB;  
    [SerializeField] protected Renderer characterRenderer; // to customize color, material or textures
    [SerializeField] protected Animator characterAnimator;
    [Header("Customizable Values")]
    [SerializeField] protected float velocity = 25;
    [SerializeField] protected float grabVelocity = 10;
    [SerializeField] protected bool walking = false;

    private int velocityHash;

    /// <summary>
    /// Return the rigid body attached to the character
    /// </summary>
    /// <returns></returns>
    public Rigidbody GetCharacterRb() => characterRB;

    protected virtual void Awake()
    {
        velocityHash = Animator.StringToHash("Velocity");
    }

    protected virtual void FixedUpdate()
    {
        WalkVerification();
        UpdateAnimator();
    }

    protected virtual void WalkVerification()
    {
        UpdateRotation();
        ApplyForwardVelocity();
    }

    protected abstract void UpdateRotation();

    protected virtual void ApplyForwardVelocity()
    {
        Vector3 velo = (new Vector3(characterRenderer.transform.parent.forward.x, 0, characterRenderer.transform.parent.forward.z) * (velocity * Time.fixedDeltaTime));
        velo.y = characterRB.velocity.y;
        characterRB.velocity = velo;
    }

    protected virtual void ApplyBackwardVelocity()
    {
        Vector3 velo = (new Vector3(characterRenderer.transform.parent.forward.x, 0, characterRenderer.transform.parent.forward.z) * (velocity * Time.fixedDeltaTime)) * -1;
        velo.y = characterRB.velocity.y;
        characterRB.velocity = velo;
    }

    protected virtual void UpdateAnimator()
    {
        characterAnimator.SetFloat(velocityHash, characterRB.velocity.normalized.magnitude);
    }

}
