﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum CollectableType { COIN, POWERUP, HEALTH, MUNITION} // ADD MORE IF NECESSARY

[System.Serializable]
public class CollectableConfiguration
{
    public bool UseFixedColor;
    public Color FixedColor;
    public CollectableType CollectableType;
    public int CollectableValue;

    public CollectableConfiguration()
    {
        UseFixedColor = false;
        FixedColor = Color.blue;
        CollectableType = CollectableType.COIN;
        CollectableValue = 1;
    }

    public CollectableConfiguration(CollectableConfiguration info)
    {
        UseFixedColor = info.UseFixedColor;
        FixedColor = info.FixedColor;
        CollectableType = info.CollectableType;
        CollectableValue = info.CollectableValue;
    }
}

[CreateAssetMenu(fileName ="CollectableData", menuName ="MainLeaf/Collectable Data")]
public class CollectableData : ScriptableObject
{
    [SerializeField] private CollectableConfiguration[] collectableCoin;
    [SerializeField] private CollectableConfiguration[] collectablePowerUp;
    [SerializeField] private CollectableConfiguration[] collectableHealth;
    [SerializeField] private CollectableConfiguration[] collectableMunition;
    [SerializeField] private Color[] colorPossibilities;

    public CollectableConfiguration GetCollectableInformation(CollectableType type, int id)
    {
        switch (type)
        {
            case CollectableType.COIN:
                return collectableCoin[id];
            case CollectableType.POWERUP:
                return collectablePowerUp[id];
            case CollectableType.HEALTH:
                return collectableHealth[id];
            case CollectableType.MUNITION:
                return collectableMunition[id];
            default:
                return null;
        }
    }

    public Color GetRandomColor()
    {
        return colorPossibilities[Random.Range(0, colorPossibilities.Length)];
    }
}
