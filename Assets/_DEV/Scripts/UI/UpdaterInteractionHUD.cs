﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdaterInteractionHUD : MonoBehaviour
{
    private int count = 0;

    private void OnLevelWasLoaded(int level)
    {
        count = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer.Equals(LayerMask.NameToLayer("Interactable")))
        {
            count++;
            InteractionUpdater.UpdateText(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer.Equals(LayerMask.NameToLayer("Interactable")))
            count--;
        if(count <= 0)
        {
            count = 0;
            InteractionUpdater.UpdateText(false);
        }
    }
}
