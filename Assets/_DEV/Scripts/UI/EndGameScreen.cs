﻿using UnityEngine;
using TMPro;

public class EndGameScreen : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI messageTxt;
    private bool sucessTemp = false;

    private void OnEnable()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void SetupMessage(string message, bool sucess = false)
    {
        sucessTemp = sucess;
        messageTxt.text = message;
        gameObject.SetActive(true);
    }

    public void RestartClick()
    {
        if (sucessTemp)
            PlayerInformations.ResetLocalInformations();
        GameManager.Instance.RestartAllLevel();
        gameObject.SetActive(false);
    }

    public void QuitGameClick()
    {
        Application.Quit();
    }
}
