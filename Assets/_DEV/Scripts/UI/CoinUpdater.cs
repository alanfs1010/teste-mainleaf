﻿using UnityEngine;
using TMPro;

public class CoinUpdater : MonoBehaviour
{
    private static TextMeshProUGUI CoinTMP;

    private void Awake()
    {
        if (CoinTMP == null)
            CoinTMP = GetComponent<TextMeshProUGUI>();
        UpdateText();
    }

    public static void UpdateText()
    {
        CoinTMP.text = $"Coins: {PlayerInformations.informations.coins}";
    }
}
