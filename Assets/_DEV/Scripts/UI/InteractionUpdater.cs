﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InteractionUpdater : MonoBehaviour
{
    private static TextMeshProUGUI ActionTMP;

    private void Awake()
    {
        if (ActionTMP == null)
            ActionTMP = GetComponent<TextMeshProUGUI>();
    }

    public static void UpdateText(bool hasInteraction)
    {
        ActionTMP.text = hasInteraction ? "Interact" : "N/A";
    }
}