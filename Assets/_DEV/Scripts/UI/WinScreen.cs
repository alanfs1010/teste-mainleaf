﻿using UnityEngine;
using TMPro;

public class WinScreen : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI coinTMP;

    private void Start()
    {
        coinTMP.text = coinTMP.text.Replace("#", PlayerInformations.informations.coins.ToString());
    }

    public void Restart()
    {
        PlayerInformations.ResetLocalInformations();
        GameManager.Instance.RestartAllLevel();
        CoinUpdater.UpdateText();
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
