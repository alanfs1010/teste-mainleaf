﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreen : MonoBehaviour
{
    [SerializeField] private GameObject pauseScreen;
    [SerializeField] private EndGameScreen endScreen;
    private void Start()
    {
        GameManager.Instance.SetLoadScreen(gameObject);
        GameManager.Instance.SetEndScreen(endScreen);
        GameManager.Instance.SetPauseScreen(pauseScreen);
    }
}
