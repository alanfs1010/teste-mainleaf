﻿using System;
using System.Collections.Generic;

public static class ServiceLocator
{
    private static Dictionary<Type, object> Services = new Dictionary<Type, object>();
    public static bool ContainService<T>(T s) => Services.ContainsKey(typeof(T));

    public static void Register<T>(object serviceInstance)
    {
        Services[typeof(T)] = serviceInstance;
    }

    public static T Resolve<T>()
    {
        return (T)Services[typeof(T)];
    }

    public static void Reset()
    {
        Services.Clear();
    }

}
