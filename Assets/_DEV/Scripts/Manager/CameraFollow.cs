﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform[] points;
    [SerializeField] private int cameraVelocity = 10;
    private int current = 0;

    private void FixedUpdate()
    {
        if (Vector3.Distance(transform.position, points[current].position) > 1)
            transform.position = Vector3.MoveTowards(transform.position, points[current].position, Time.fixedDeltaTime * cameraVelocity);
    }

    public void SetNewPosition(Transform refPosition)
    {
        transform.position = refPosition.position;
    }

    public void TriggerNextPoint()
    {
        current++;
    }
}
