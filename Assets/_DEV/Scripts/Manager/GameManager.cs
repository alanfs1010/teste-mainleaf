﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class GameManager : SingletonMonoBehaviour<GameManager>
{
    [Header("Data")]
    [SerializeField] private CollectableData collectableData;
    [Header("Loading Screen")]
    [SerializeField] private GameObject loadingImage;
    [Header("Pause")]
    [SerializeField] private GameObject pauseScreen;
    [Header("End Game Screen")]
    [SerializeField] private EndGameScreen endGameScreen;

    private PoolSystem poolSystem = new PoolSystem(); // acess to pool system ( not used in this project )
    private InputSettings inputMap;

    public CollectableData CollectableData { get { return collectableData; } }
    public PoolSystem PoolSystem { get { return poolSystem; } }
    public void SetPauseScreen(GameObject obj) => pauseScreen = obj;
    public void SetEndScreen(EndGameScreen obj) => endGameScreen = obj;
    public void SetLoadScreen(GameObject obj) => loadingImage = obj;

    private void OnLevelWasLoaded(int level)
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Start()
    {
        I = this;
        DontDestroyOnLoad(this);
        EnableInput();
        loadingImage.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void EnableInput()
    {
        inputMap = new InputSettings();
        inputMap.Pause.PauseTrigger.performed += PauseInput;
        inputMap.Pause.Enable();
    }

    public void LoadNextScene()
    {
        string nextMap = "WinScreen";
        if (ServiceLocator.Resolve<Scenery>().NextMap.Length > 1)
        {
            nextMap = ServiceLocator.Resolve<Scenery>().NextMap;
        }
        UnpauseGame();

        StartCoroutine(LoadScene(nextMap));

    }

    public void CallEndGame(string endGameMessage, bool sucess = false)
    {
        endGameScreen.SetupMessage(endGameMessage, sucess);
        PauseGame();
    }

    private void PauseInput(InputAction.CallbackContext ctx)
    {
        if (pauseScreen == null) return;
        if (pauseScreen.activeSelf)
            UnpauseGame();
        else
            PauseGame(false);
    }

    public void PauseGame( bool ignorePauseScreen = true)
    {
        if (!ignorePauseScreen)
            pauseScreen.SetActive(true);
        Time.timeScale = 0;
    }

    public void UnpauseGame()
    {
        if (pauseScreen != null)
        {
            pauseScreen.SetActive(false);
        }
        Time.timeScale = 1;
    }

    public void RestartLevel()
    {
        StartCoroutine(LoadScene(SceneManager.GetActiveScene().name));
        UnpauseGame();
    }

    public void RestartAllLevel()
    {
        StartCoroutine(LoadScene("FirstStage"));
        UnpauseGame();
    }

    public bool IsGameRunning()
    {
        return Time.timeScale != 0 && (loadingImage == null || !loadingImage.activeSelf);
    }

    private IEnumerator LoadScene(string nextMap)
    {
        if (loadingImage != null)
        {
            loadingImage?.SetActive(true);
        }
        yield return null;
        yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        yield return SceneManager.LoadSceneAsync(nextMap);
        yield return new WaitForSeconds(1);
        if (loadingImage != null)
        {
            loadingImage.SetActive(false);
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
