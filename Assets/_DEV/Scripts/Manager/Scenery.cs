﻿using UnityEngine;

public class Scenery : MonoBehaviour
{
    [SerializeField] private string nextMap;
    public string NextMap { get { return nextMap; } }

    private void Awake()
    {
        ServiceLocator.Register<Scenery>(this);
    }
}
