﻿using System.Collections.Generic;
using UnityEngine;

public class PoolSystem
{
    private Dictionary<CollectableType, Queue<Collectable>> collectablePoolList = new Dictionary<CollectableType, Queue<Collectable>>();

    // simple pool system to create and recreate objects ( this example is a pool list about collectables only )

    public Collectable GetFromPoolList(CollectableType type)
    {
        if (collectablePoolList.ContainsKey(type))
        {
            if(collectablePoolList[type].Count > 0)
            {
                return collectablePoolList[type].Dequeue();
            }
        }
        else
        {
            CreateListOnPull(type);
        }

        return CreateNewCollectable(type);
    }

    public void PutOnList(Collectable collectable)
    {
        if (!collectablePoolList.ContainsKey(collectable.CollectableType))
            CreateListOnPull(collectable.CollectableType);
        collectablePoolList[collectable.CollectableType].Enqueue(collectable);
        collectable.gameObject.SetActive(false);
        collectable.transform.SetParent(GameManager.Instance.transform);
        collectable.transform.localPosition = Vector3.zero;
    }

    private Collectable CreateNewCollectable(CollectableType type)
    {
        return null; // Create a instantiate from prefab... maybe?  just'n case
    }

    private void CreateListOnPull(CollectableType type)
    {
        collectablePoolList.Add(type, new Queue<Collectable>());
    }

}
