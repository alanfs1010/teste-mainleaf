﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NextCameraPoint : MonoBehaviour
{
    [SerializeField] private UnityEvent TriggerEvent;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer.Equals(LayerMask.NameToLayer("Player")))
            TriggerEvent?.Invoke();
    }
}
