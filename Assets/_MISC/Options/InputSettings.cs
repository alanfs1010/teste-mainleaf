// GENERATED AUTOMATICALLY FROM 'Assets/_MISC/Options/InputSettings.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputSettings : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputSettings()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputSettings"",
    ""maps"": [
        {
            ""name"": ""Moviment"",
            ""id"": ""5d95f03d-bf6c-456a-a981-52dc0915fe63"",
            ""actions"": [
                {
                    ""name"": ""HorizontalMoviment"",
                    ""type"": ""Button"",
                    ""id"": ""27698d4a-86e3-43aa-81a2-6ce15eaa820a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Vertical"",
                    ""type"": ""Button"",
                    ""id"": ""851f5aea-b507-4f02-82ca-4a3d82169b40"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""fafd6c78-7aa2-45e0-a3e1-bafb39ad8fa2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Crounch"",
                    ""type"": ""Button"",
                    ""id"": ""54ca8d6a-dcae-437e-8fdc-cdb6af30edc9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Horizontal"",
                    ""id"": ""0ec00433-7bcd-4d55-9fbe-eba1eb3a4944"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HorizontalMoviment"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""76963359-5a14-45a6-90a7-181899a721ad"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""HorizontalMoviment"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""ead018d3-cfad-4c80-a0f8-b745f2c5b1be"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""HorizontalMoviment"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Vertical"",
                    ""id"": ""abc1164c-1414-4dd7-93f9-b00c0f8b757a"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Vertical"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""09bcbad7-b920-429f-8c4d-e0f05b3c5aaa"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Vertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""1adf9ca7-1903-49e8-a575-e48176ef9be0"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Vertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""f04130f7-34fb-42d6-9341-7b20b1483915"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""80e2e219-6f84-4cd2-a6fb-916339072dcc"",
                    ""path"": ""<Keyboard>/ctrl"",
                    ""interactions"": ""Hold"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Crounch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Interaction"",
            ""id"": ""504bf887-eb97-4f33-9a4e-57af67bf5bae"",
            ""actions"": [
                {
                    ""name"": ""BasicInteraction"",
                    ""type"": ""Button"",
                    ""id"": ""51bde098-fdf8-494d-89e4-f11f8e994638"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Hold""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""f1e0c886-3ee9-4558-bfba-a396e6c96c2f"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""BasicInteraction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bb30c7f2-22d5-4949-95c8-6b232c72c102"",
                    ""path"": ""<VirtualMouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""BasicInteraction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Pause"",
            ""id"": ""b5395dbd-264f-48c5-9126-045c26f4824d"",
            ""actions"": [
                {
                    ""name"": ""PauseTrigger"",
                    ""type"": ""Button"",
                    ""id"": ""f6866a38-1178-4c8b-8ed1-8c7f0abeffa1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""f68fb0c7-6608-4958-87f5-67a53f383f91"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""PauseTrigger"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""PC"",
            ""bindingGroup"": ""PC"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<VirtualMouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Moviment
        m_Moviment = asset.FindActionMap("Moviment", throwIfNotFound: true);
        m_Moviment_HorizontalMoviment = m_Moviment.FindAction("HorizontalMoviment", throwIfNotFound: true);
        m_Moviment_Vertical = m_Moviment.FindAction("Vertical", throwIfNotFound: true);
        m_Moviment_Jump = m_Moviment.FindAction("Jump", throwIfNotFound: true);
        m_Moviment_Crounch = m_Moviment.FindAction("Crounch", throwIfNotFound: true);
        // Interaction
        m_Interaction = asset.FindActionMap("Interaction", throwIfNotFound: true);
        m_Interaction_BasicInteraction = m_Interaction.FindAction("BasicInteraction", throwIfNotFound: true);
        // Pause
        m_Pause = asset.FindActionMap("Pause", throwIfNotFound: true);
        m_Pause_PauseTrigger = m_Pause.FindAction("PauseTrigger", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Moviment
    private readonly InputActionMap m_Moviment;
    private IMovimentActions m_MovimentActionsCallbackInterface;
    private readonly InputAction m_Moviment_HorizontalMoviment;
    private readonly InputAction m_Moviment_Vertical;
    private readonly InputAction m_Moviment_Jump;
    private readonly InputAction m_Moviment_Crounch;
    public struct MovimentActions
    {
        private @InputSettings m_Wrapper;
        public MovimentActions(@InputSettings wrapper) { m_Wrapper = wrapper; }
        public InputAction @HorizontalMoviment => m_Wrapper.m_Moviment_HorizontalMoviment;
        public InputAction @Vertical => m_Wrapper.m_Moviment_Vertical;
        public InputAction @Jump => m_Wrapper.m_Moviment_Jump;
        public InputAction @Crounch => m_Wrapper.m_Moviment_Crounch;
        public InputActionMap Get() { return m_Wrapper.m_Moviment; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MovimentActions set) { return set.Get(); }
        public void SetCallbacks(IMovimentActions instance)
        {
            if (m_Wrapper.m_MovimentActionsCallbackInterface != null)
            {
                @HorizontalMoviment.started -= m_Wrapper.m_MovimentActionsCallbackInterface.OnHorizontalMoviment;
                @HorizontalMoviment.performed -= m_Wrapper.m_MovimentActionsCallbackInterface.OnHorizontalMoviment;
                @HorizontalMoviment.canceled -= m_Wrapper.m_MovimentActionsCallbackInterface.OnHorizontalMoviment;
                @Vertical.started -= m_Wrapper.m_MovimentActionsCallbackInterface.OnVertical;
                @Vertical.performed -= m_Wrapper.m_MovimentActionsCallbackInterface.OnVertical;
                @Vertical.canceled -= m_Wrapper.m_MovimentActionsCallbackInterface.OnVertical;
                @Jump.started -= m_Wrapper.m_MovimentActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_MovimentActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_MovimentActionsCallbackInterface.OnJump;
                @Crounch.started -= m_Wrapper.m_MovimentActionsCallbackInterface.OnCrounch;
                @Crounch.performed -= m_Wrapper.m_MovimentActionsCallbackInterface.OnCrounch;
                @Crounch.canceled -= m_Wrapper.m_MovimentActionsCallbackInterface.OnCrounch;
            }
            m_Wrapper.m_MovimentActionsCallbackInterface = instance;
            if (instance != null)
            {
                @HorizontalMoviment.started += instance.OnHorizontalMoviment;
                @HorizontalMoviment.performed += instance.OnHorizontalMoviment;
                @HorizontalMoviment.canceled += instance.OnHorizontalMoviment;
                @Vertical.started += instance.OnVertical;
                @Vertical.performed += instance.OnVertical;
                @Vertical.canceled += instance.OnVertical;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Crounch.started += instance.OnCrounch;
                @Crounch.performed += instance.OnCrounch;
                @Crounch.canceled += instance.OnCrounch;
            }
        }
    }
    public MovimentActions @Moviment => new MovimentActions(this);

    // Interaction
    private readonly InputActionMap m_Interaction;
    private IInteractionActions m_InteractionActionsCallbackInterface;
    private readonly InputAction m_Interaction_BasicInteraction;
    public struct InteractionActions
    {
        private @InputSettings m_Wrapper;
        public InteractionActions(@InputSettings wrapper) { m_Wrapper = wrapper; }
        public InputAction @BasicInteraction => m_Wrapper.m_Interaction_BasicInteraction;
        public InputActionMap Get() { return m_Wrapper.m_Interaction; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(InteractionActions set) { return set.Get(); }
        public void SetCallbacks(IInteractionActions instance)
        {
            if (m_Wrapper.m_InteractionActionsCallbackInterface != null)
            {
                @BasicInteraction.started -= m_Wrapper.m_InteractionActionsCallbackInterface.OnBasicInteraction;
                @BasicInteraction.performed -= m_Wrapper.m_InteractionActionsCallbackInterface.OnBasicInteraction;
                @BasicInteraction.canceled -= m_Wrapper.m_InteractionActionsCallbackInterface.OnBasicInteraction;
            }
            m_Wrapper.m_InteractionActionsCallbackInterface = instance;
            if (instance != null)
            {
                @BasicInteraction.started += instance.OnBasicInteraction;
                @BasicInteraction.performed += instance.OnBasicInteraction;
                @BasicInteraction.canceled += instance.OnBasicInteraction;
            }
        }
    }
    public InteractionActions @Interaction => new InteractionActions(this);

    // Pause
    private readonly InputActionMap m_Pause;
    private IPauseActions m_PauseActionsCallbackInterface;
    private readonly InputAction m_Pause_PauseTrigger;
    public struct PauseActions
    {
        private @InputSettings m_Wrapper;
        public PauseActions(@InputSettings wrapper) { m_Wrapper = wrapper; }
        public InputAction @PauseTrigger => m_Wrapper.m_Pause_PauseTrigger;
        public InputActionMap Get() { return m_Wrapper.m_Pause; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PauseActions set) { return set.Get(); }
        public void SetCallbacks(IPauseActions instance)
        {
            if (m_Wrapper.m_PauseActionsCallbackInterface != null)
            {
                @PauseTrigger.started -= m_Wrapper.m_PauseActionsCallbackInterface.OnPauseTrigger;
                @PauseTrigger.performed -= m_Wrapper.m_PauseActionsCallbackInterface.OnPauseTrigger;
                @PauseTrigger.canceled -= m_Wrapper.m_PauseActionsCallbackInterface.OnPauseTrigger;
            }
            m_Wrapper.m_PauseActionsCallbackInterface = instance;
            if (instance != null)
            {
                @PauseTrigger.started += instance.OnPauseTrigger;
                @PauseTrigger.performed += instance.OnPauseTrigger;
                @PauseTrigger.canceled += instance.OnPauseTrigger;
            }
        }
    }
    public PauseActions @Pause => new PauseActions(this);
    private int m_PCSchemeIndex = -1;
    public InputControlScheme PCScheme
    {
        get
        {
            if (m_PCSchemeIndex == -1) m_PCSchemeIndex = asset.FindControlSchemeIndex("PC");
            return asset.controlSchemes[m_PCSchemeIndex];
        }
    }
    public interface IMovimentActions
    {
        void OnHorizontalMoviment(InputAction.CallbackContext context);
        void OnVertical(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnCrounch(InputAction.CallbackContext context);
    }
    public interface IInteractionActions
    {
        void OnBasicInteraction(InputAction.CallbackContext context);
    }
    public interface IPauseActions
    {
        void OnPauseTrigger(InputAction.CallbackContext context);
    }
}
