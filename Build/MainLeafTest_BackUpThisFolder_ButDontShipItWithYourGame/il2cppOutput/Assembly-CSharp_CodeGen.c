﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.InputSystem.InputActionAsset InputSettings::get_asset()
extern void InputSettings_get_asset_m4236597DD28CD00C8176AC8FCA3BE387D37028EA (void);
// 0x00000002 System.Void InputSettings::.ctor()
extern void InputSettings__ctor_mEE801DA7B6C60FCCCED5F04E1972B502AA39BA92 (void);
// 0x00000003 System.Void InputSettings::Dispose()
extern void InputSettings_Dispose_mA98F9FDA5DFAF52A0477CF7A13DED399C933C96D (void);
// 0x00000004 System.Nullable`1<UnityEngine.InputSystem.InputBinding> InputSettings::get_bindingMask()
extern void InputSettings_get_bindingMask_m7BA71AAD392BC7947C1C7DD695066D8977029DF9 (void);
// 0x00000005 System.Void InputSettings::set_bindingMask(System.Nullable`1<UnityEngine.InputSystem.InputBinding>)
extern void InputSettings_set_bindingMask_m3721DA2514F553320ECE65CB8035D80C80EACEA5 (void);
// 0x00000006 System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>> InputSettings::get_devices()
extern void InputSettings_get_devices_mDA1C1EFCC2873E104A6E06A62ECC08ED29257C5D (void);
// 0x00000007 System.Void InputSettings::set_devices(System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>>)
extern void InputSettings_set_devices_m166EC41F78E60EF1A278FC6474E040FDBC838FEF (void);
// 0x00000008 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme> InputSettings::get_controlSchemes()
extern void InputSettings_get_controlSchemes_mEA3E9FD66B2FC66DF1160F50E00F592FF15AD62C (void);
// 0x00000009 System.Boolean InputSettings::Contains(UnityEngine.InputSystem.InputAction)
extern void InputSettings_Contains_m2EDC0C9F4C0A92D23B7211EC9A2277242259B879 (void);
// 0x0000000A System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputAction> InputSettings::GetEnumerator()
extern void InputSettings_GetEnumerator_m3781E4070EB7EB84D114DFC11B57ACEB3F3E8267 (void);
// 0x0000000B System.Collections.IEnumerator InputSettings::System.Collections.IEnumerable.GetEnumerator()
extern void InputSettings_System_Collections_IEnumerable_GetEnumerator_mE50ADD3A3ECA7BD26867A3B0B1152CAA57AA99A6 (void);
// 0x0000000C System.Void InputSettings::Enable()
extern void InputSettings_Enable_mE655C8F3B4703A9974102FF4E34D692ED67EE355 (void);
// 0x0000000D System.Void InputSettings::Disable()
extern void InputSettings_Disable_m2DC68D0B5B238EB23DB3709DDCCB30A013BD2026 (void);
// 0x0000000E InputSettings/MovimentActions InputSettings::get_Moviment()
extern void InputSettings_get_Moviment_m3D0E760760C58088DD6E5423C45695BECC8F19AD (void);
// 0x0000000F InputSettings/InteractionActions InputSettings::get_Interaction()
extern void InputSettings_get_Interaction_m63FEB93219AC2C32D746614778FA7C8A01E3909F (void);
// 0x00000010 InputSettings/PauseActions InputSettings::get_Pause()
extern void InputSettings_get_Pause_mDACE07FC85EA27778B4EBB137048B5CDA076195A (void);
// 0x00000011 UnityEngine.InputSystem.InputControlScheme InputSettings::get_PCScheme()
extern void InputSettings_get_PCScheme_m7623D6F71FD64F19D0C2C616E263C1544A99FD97 (void);
// 0x00000012 UnityEngine.Rigidbody CharacterBase::GetCharacterRb()
extern void CharacterBase_GetCharacterRb_mE1DF98BB5895403C5927A63DE484BA4FF447053D (void);
// 0x00000013 System.Void CharacterBase::FixedUpdate()
extern void CharacterBase_FixedUpdate_m7CBF120D8FB2F2728EECBBC4235C58FF7132B985 (void);
// 0x00000014 System.Void CharacterBase::WalkVerifications()
extern void CharacterBase_WalkVerifications_m703F0B7422146A7FAA223B6FF7AF079FF7761E02 (void);
// 0x00000015 System.Void CharacterBase::UpdateRotation()
// 0x00000016 System.Void CharacterBase::ApplyForwardVelocity()
extern void CharacterBase_ApplyForwardVelocity_m30B47CB0CEF3A34C40D2BB6907DBCDCA805EF0F8 (void);
// 0x00000017 System.Void CharacterBase::ApplyBackwardVelocity()
extern void CharacterBase_ApplyBackwardVelocity_mF1D1BB7B6F7A80C19E1D5DC94FFF41C956B0FBB4 (void);
// 0x00000018 System.Void CharacterBase::UpdateAnimator()
extern void CharacterBase_UpdateAnimator_m2E535CC66B1D82B44B98CEADF2BB2F248F89682D (void);
// 0x00000019 System.Void CharacterBase::.ctor()
extern void CharacterBase__ctor_m6E960F31ADCE737E0EC02758D9A52F7D2AC26AEE (void);
// 0x0000001A System.Void EnemyBehaviour::Awake()
extern void EnemyBehaviour_Awake_mA55367702DF0688184ED6D220E15D1FA4B77B699 (void);
// 0x0000001B System.Void EnemyBehaviour::WalkVerifications()
extern void EnemyBehaviour_WalkVerifications_m4D5416846E7EBB0C7EBFF9CD3C62038879A2FBA2 (void);
// 0x0000001C System.Boolean EnemyBehaviour::CheckCorrectTarget(TargetPoint)
extern void EnemyBehaviour_CheckCorrectTarget_m8669732047422D04933979AF864AE889F9A86939 (void);
// 0x0000001D System.Void EnemyBehaviour::ReachTarget()
extern void EnemyBehaviour_ReachTarget_mFC9798693541A76E3D8CC68A48ED21BDE42E55B8 (void);
// 0x0000001E System.Void EnemyBehaviour::SetNextTarget()
extern void EnemyBehaviour_SetNextTarget_mB8CF35776B9E6C8B70C34532FDE9E3367D3A6EE9 (void);
// 0x0000001F System.Void EnemyBehaviour::UpdateRotation()
extern void EnemyBehaviour_UpdateRotation_mA1B3B311CE6BE333239B0E74BE691CFD140A56B7 (void);
// 0x00000020 System.Void EnemyBehaviour::.ctor()
extern void EnemyBehaviour__ctor_m3721742BCD67360F80567F0090EAA9486500899A (void);
// 0x00000021 System.Void PlayerTrigger::Update()
extern void PlayerTrigger_Update_m2FCD9BAC40922DF919449C259F9EAD953AC3301D (void);
// 0x00000022 System.Void PlayerTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void PlayerTrigger_OnTriggerEnter_m071D5B38AD4454EC989F5522986AF464C9F42B47 (void);
// 0x00000023 System.Void PlayerTrigger::.ctor()
extern void PlayerTrigger__ctor_mBFE94A3385FEA7CC23F08E54A290C1552A791286 (void);
// 0x00000024 System.Void TargetPoint::Awake()
extern void TargetPoint_Awake_m3FE8F2EF2B160716707D93032D7F901C742F3875 (void);
// 0x00000025 System.Void TargetPoint::OnTriggerEnter(UnityEngine.Collider)
extern void TargetPoint_OnTriggerEnter_mC90A500F7C222479DB431F6EDFA80A620C6408C7 (void);
// 0x00000026 System.Void TargetPoint::.ctor()
extern void TargetPoint__ctor_m0798798B87EFD5D2896EF1E58FBB0FEFECB1B8C4 (void);
// 0x00000027 System.Void IPlayerComponent::Initialize()
// 0x00000028 UnityEngine.Transform Player::get__playerModelTransform()
extern void Player_get__playerModelTransform_mB5809DCE5774ED29C94E552EE1FADBAC82094225 (void);
// 0x00000029 System.Boolean Player::get__duringPushing()
extern void Player_get__duringPushing_mB2BBFAF86D0A3EC8C3F2AFFAD5D7702F8250994B (void);
// 0x0000002A System.Void Player::set__duringPushing(System.Boolean)
extern void Player_set__duringPushing_m3FA99528CB208EEFA198123A0F127A04E84E47B5 (void);
// 0x0000002B PlayerInput Player::get__playerInput()
extern void Player_get__playerInput_mB18FFC96EAA4878902CA2B06A720AD7E31D2B734 (void);
// 0x0000002C System.Void Player::Awake()
extern void Player_Awake_mD94498D36D7F1E5FDB727209B4B6043A6F228E5C (void);
// 0x0000002D System.Void Player::Update()
extern void Player_Update_m6F977BAE3756AB7073D64042B766B442E4EC6FD2 (void);
// 0x0000002E System.Void Player::WalkVerifications()
extern void Player_WalkVerifications_m4254864C0A88290D8E9AAE29F6484FD2B0D2B0DB (void);
// 0x0000002F System.Void Player::UpdateRotation()
extern void Player_UpdateRotation_mCF953FC5A034CC35D9E6EA7C6A3897F1A72D55B3 (void);
// 0x00000030 System.Void Player::UpdateAnimator()
extern void Player_UpdateAnimator_m39750AAADBE5625180E38D990F8AEEBADC430B60 (void);
// 0x00000031 System.Void Player::ApplyBackwardVelocity()
extern void Player_ApplyBackwardVelocity_m37827B1378EC626801F6E8BD3D8FE097E721B152 (void);
// 0x00000032 System.Void Player::ApplyForwardVelocity()
extern void Player_ApplyForwardVelocity_m158761ABCC708B1BE0BD0B59FA500B7AFFFA634F (void);
// 0x00000033 System.Void Player::ResetVelocity()
extern void Player_ResetVelocity_m300429D0271EB77ACF0CB851DA3C59EFEB8BCF32 (void);
// 0x00000034 System.Void Player::UpdateCrounchState(System.Boolean)
extern void Player_UpdateCrounchState_mE57DA62AE973A5735E23CA8E0F4FB477718E0BF8 (void);
// 0x00000035 System.Void Player::.ctor()
extern void Player__ctor_m8F4AB650C5E2DE406B3C65EA8F662013458D85E2 (void);
// 0x00000036 System.Void PlayerComponent::Initialize()
extern void PlayerComponent_Initialize_mB4058A1A2CCB36216CFB483EB33EAAC9DD538FFF (void);
// 0x00000037 System.Void PlayerComponent::.ctor()
extern void PlayerComponent__ctor_m7C251856B197D64A0685D266C1908EF0A71FD027 (void);
// 0x00000038 System.Void PlayerCrounch::Initialize()
extern void PlayerCrounch_Initialize_mD1166AA7513688A7BC8C0E75EE8646B7326B151D (void);
// 0x00000039 System.Void PlayerCrounch::OnEnable()
extern void PlayerCrounch_OnEnable_m963C48DF4150337DEC7ABAC1BDF3429A18DC34CC (void);
// 0x0000003A System.Void PlayerCrounch::OnDisable()
extern void PlayerCrounch_OnDisable_m657177C58B9FED2E6452FD96DB7E15B5BEB20DC6 (void);
// 0x0000003B System.Void PlayerCrounch::ActivateCrounchState(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerCrounch_ActivateCrounchState_m57D0B31E97DD912F5B7F49DA1D41DD41EB71316B (void);
// 0x0000003C System.Void PlayerCrounch::DesactivateCrounchState(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerCrounch_DesactivateCrounchState_mA4ACA73CBF57896E96163424F5FF31C1ABA002A8 (void);
// 0x0000003D System.Boolean PlayerCrounch::CanCrounch()
extern void PlayerCrounch_CanCrounch_m96E449451D7CFA6386D4A1B5D98DA383EC180CB3 (void);
// 0x0000003E System.Void PlayerCrounch::.ctor()
extern void PlayerCrounch__ctor_m25985836C11DBB0C7E5EEEBD0EE29D5E1A5290DC (void);
// 0x0000003F System.Void PlayerInput::add_JumpAction(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
extern void PlayerInput_add_JumpAction_mEE8FA46E9CA26DD2A087DE523E7307635535A3DB (void);
// 0x00000040 System.Void PlayerInput::remove_JumpAction(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
extern void PlayerInput_remove_JumpAction_mD8A5CFC5DBC0D2AC2D51FA354BBC446B2DD92CB5 (void);
// 0x00000041 System.Void PlayerInput::add_CrounchAction(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
extern void PlayerInput_add_CrounchAction_m9AD3D01AD8D8558696B888C93005C5F4E4D04509 (void);
// 0x00000042 System.Void PlayerInput::remove_CrounchAction(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
extern void PlayerInput_remove_CrounchAction_m91C437C7363AA1C6E10A02287CAAB117C5197747 (void);
// 0x00000043 System.Void PlayerInput::add_UnCrounchAction(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
extern void PlayerInput_add_UnCrounchAction_m82ABCFE5FF3174DBE78F7B19558B267DFAD5BAD4 (void);
// 0x00000044 System.Void PlayerInput::remove_UnCrounchAction(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
extern void PlayerInput_remove_UnCrounchAction_m57495D95768E8689B12EA781B5B4D9879973AFD4 (void);
// 0x00000045 System.Void PlayerInput::add_GrabAction(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
extern void PlayerInput_add_GrabAction_m91D7CC500BC80848621C90D0BB0C627265C6DB3A (void);
// 0x00000046 System.Void PlayerInput::remove_GrabAction(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
extern void PlayerInput_remove_GrabAction_mB1A55857AF5B0174D1B62A0EC553BC7BBB332C87 (void);
// 0x00000047 System.Void PlayerInput::add_UnGrabAction(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
extern void PlayerInput_add_UnGrabAction_m4BBC719F34D064195052A6211B7A372848366854 (void);
// 0x00000048 System.Void PlayerInput::remove_UnGrabAction(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
extern void PlayerInput_remove_UnGrabAction_m4BB5518F86036C102F4A06E25BD7E2A4A9CD3386 (void);
// 0x00000049 System.Void PlayerInput::Initialize()
extern void PlayerInput_Initialize_m53BB711E0285EE4199361A8DE4201541469D7592 (void);
// 0x0000004A System.Void PlayerInput::OnDisable()
extern void PlayerInput_OnDisable_m660D1B6EC3D536D232000F07339781DE1DE3CE76 (void);
// 0x0000004B System.Void PlayerInput::EnableInput()
extern void PlayerInput_EnableInput_m49708CED0661D906F4C089AA069AF4FE4EB3665B (void);
// 0x0000004C System.Void PlayerInput::DisableInput()
extern void PlayerInput_DisableInput_m211D7C2D4FCA1B965460DA1F2339CB4D07F00917 (void);
// 0x0000004D System.Void PlayerInput::CrounchInput(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerInput_CrounchInput_mED3CDD5A5F62248C0E2A941AE28DA90FD96D3207 (void);
// 0x0000004E System.Void PlayerInput::UnCrounchInput(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerInput_UnCrounchInput_mBAF046AAD1DE824EEA32061E1A5F039AE95387AF (void);
// 0x0000004F System.Void PlayerInput::JumpInput(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerInput_JumpInput_mF9EDA88B41430F05B189D552B1C722297DFAF845 (void);
// 0x00000050 System.Void PlayerInput::GrabInput(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerInput_GrabInput_m0EEBA7EAA2ADE12AB8CE9380A655C044D299CCF1 (void);
// 0x00000051 System.Void PlayerInput::UnGrabInput(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerInput_UnGrabInput_mAF23B799E38918F474BE3C1FDEFD26B2A9947802 (void);
// 0x00000052 System.Void PlayerInput::.ctor()
extern void PlayerInput__ctor_mA6A5B5AF425706AE2F809DC30DE2E16C92BFC7C7 (void);
// 0x00000053 System.Void PlayerInteraction::Initialize()
extern void PlayerInteraction_Initialize_m78D0185D86B0893CB16013C99018A0C946D30B13 (void);
// 0x00000054 System.Void PlayerInteraction::OnEnable()
extern void PlayerInteraction_OnEnable_m943D501F408CD61F2F82975B1D189566818720E9 (void);
// 0x00000055 System.Void PlayerInteraction::OnDisable()
extern void PlayerInteraction_OnDisable_mA25EA9C9057DBB2F4CB12FE227C39843F4C98C5B (void);
// 0x00000056 System.Void PlayerInteraction::GrabAction(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerInteraction_GrabAction_m38546C1D883216EDFE97A7DC4184DAEB02E475EF (void);
// 0x00000057 System.Void PlayerInteraction::UnGrabAction(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerInteraction_UnGrabAction_m7ED2E4C1275F466020B9625C74F5B5A43C964AFF (void);
// 0x00000058 System.Void PlayerInteraction::OnTriggerEnter(UnityEngine.Collider)
extern void PlayerInteraction_OnTriggerEnter_mE0CBCD17DB9910EFA43894F40E891DE8FB61F393 (void);
// 0x00000059 System.Void PlayerInteraction::.ctor()
extern void PlayerInteraction__ctor_mD1058C5969AC6F66FF838496BD1D88CB7B6F676C (void);
// 0x0000005A System.Void PlayerJump::Initialize()
extern void PlayerJump_Initialize_m235E4085E2B8F3C4B88242D03FC4FE0543539B82 (void);
// 0x0000005B System.Void PlayerJump::OnEnable()
extern void PlayerJump_OnEnable_mD21359467FD01E9057357F8DAD623248237467BF (void);
// 0x0000005C System.Void PlayerJump::OnDisable()
extern void PlayerJump_OnDisable_mC2DAA99278ECF8E9583DA2BBD1CE40D1879FF3FF (void);
// 0x0000005D System.Void PlayerJump::Jump(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerJump_Jump_mE614C1B09152248C01FA4AB39D25587D885C4197 (void);
// 0x0000005E System.Boolean PlayerJump::HasGrabPossibilities()
extern void PlayerJump_HasGrabPossibilities_m5F80AABA4278F34E7EFD4C4B0BB39112D1C2CBEC (void);
// 0x0000005F System.Boolean PlayerJump::EnableToJump()
extern void PlayerJump_EnableToJump_m919451D3889108EE9FF0991F1B33AB456DD00FFE (void);
// 0x00000060 System.Void PlayerJump::.ctor()
extern void PlayerJump__ctor_mD3F62AD9206A237A736AECD109B82988DF9316AC (void);
// 0x00000061 System.Void Coin::OnValidate()
extern void Coin_OnValidate_mC18F67293EBB470B49912BB65557D2C96E7752FC (void);
// 0x00000062 System.Boolean Coin::ReadyToCollect()
extern void Coin_ReadyToCollect_m8D63FBF97A4A5772FD7B04D6FA8A14C712C75E73 (void);
// 0x00000063 System.Void Coin::TryToCollect()
extern void Coin_TryToCollect_m024146A256BA1FA7934882C2549EC7640651FF41 (void);
// 0x00000064 System.Void Coin::.ctor()
extern void Coin__ctor_m95BF6BF6C4B1A55ED5CAE43FB49371C56E990350 (void);
// 0x00000065 System.Void Collectable::OnValidate()
extern void Collectable_OnValidate_m975921EA291C0DEC3F7E28404EFD837D3B617812 (void);
// 0x00000066 System.Void Collectable::Start()
extern void Collectable_Start_m2E96ECB5BBD424083DF2305219FBCBAD6E6B3F7A (void);
// 0x00000067 System.Void Collectable::SetupCollectable()
extern void Collectable_SetupCollectable_m410E25E562190A77497C79059E0EB209981EE6E9 (void);
// 0x00000068 System.Boolean Collectable::ReadyToCollect()
// 0x00000069 System.Void Collectable::TryToCollect()
// 0x0000006A System.Void Collectable::.ctor()
extern void Collectable__ctor_m2BB5693EB0E8D466151D66585FB889275D811358 (void);
// 0x0000006B UnityEngine.Rigidbody Grabbable::GetRigidbody()
extern void Grabbable_GetRigidbody_m282D83730035C698FB7F1766FBE07B9E2A662423 (void);
// 0x0000006C System.Void Grabbable::Start()
extern void Grabbable_Start_m275A574B74E6CA0D1DC0291F2314BE662FC9AD5C (void);
// 0x0000006D System.Void Grabbable::FixedUpdate()
extern void Grabbable_FixedUpdate_m795FE5182137F8800C4B7BDE6F7B2D2A3309A598 (void);
// 0x0000006E System.Void Grabbable::Interaction()
extern void Grabbable_Interaction_m89C72C5D11C92AE4A3EB471C0205179E2B398F18 (void);
// 0x0000006F System.Void Grabbable::DisableInteraction(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void Grabbable_DisableInteraction_m5EE9E530620062BB552D1136A5F7AD03C07283B4 (void);
// 0x00000070 System.Void Grabbable::ChangeGrabState(System.Boolean)
extern void Grabbable_ChangeGrabState_m0C43D48B5BBE30048B6A178F56703841618ABA44 (void);
// 0x00000071 System.Void Grabbable::ReachDestiny()
extern void Grabbable_ReachDestiny_mA3D31751C25848A46CB1D99E6E01DA8E32F94564 (void);
// 0x00000072 System.Void Grabbable::.ctor()
extern void Grabbable__ctor_m641A1F81714AF055A1F85C7FC8FDECF888112138 (void);
// 0x00000073 System.Void ICollectable::TryToCollect()
// 0x00000074 System.Boolean ICollectable::ReadyToCollect()
// 0x00000075 System.Void IInteractable::SetLayer()
// 0x00000076 System.Void IInteractable::TryToInteract()
// 0x00000077 System.Void IInteractable::Interaction()
// 0x00000078 System.Void IInteractable::DisableInteraction(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000079 System.Boolean IInteractable::EnableToInteract()
// 0x0000007A System.Void InteractableBase::DisableInteraction(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x0000007B System.Boolean InteractableBase::EnableToInteract()
extern void InteractableBase_EnableToInteract_m1693A81295DC53E0FCB840353F1D29A4CBAC518C (void);
// 0x0000007C System.Void InteractableBase::Interaction()
// 0x0000007D System.Void InteractableBase::SetLayer()
extern void InteractableBase_SetLayer_m781F7546FF93DE383DA15960D51DD7A83967FA6F (void);
// 0x0000007E System.Void InteractableBase::TryToInteract()
extern void InteractableBase_TryToInteract_mAED344B1E13375A47FC1028F29FC2B8A5833C305 (void);
// 0x0000007F System.Void InteractableBase::.ctor()
extern void InteractableBase__ctor_m33DDA9BFED31F92EA307D56615CB7E803FE92201 (void);
// 0x00000080 System.Void ReachChecker::OnValidate()
extern void ReachChecker_OnValidate_m727BAD1402CD4D84556B582F9D998A84B3DAA7C6 (void);
// 0x00000081 System.Void ReachChecker::OnTriggerEnter(UnityEngine.Collider)
extern void ReachChecker_OnTriggerEnter_mDEB35E1007DC36D11B6A40BD1E0AB266206513B6 (void);
// 0x00000082 System.Void ReachChecker::.ctor()
extern void ReachChecker__ctor_m95248DD5648DF9FD245A63092AFFBFC9A45945F0 (void);
// 0x00000083 System.Void CameraFollow::FixedUpdate()
extern void CameraFollow_FixedUpdate_mBF9AC25722C9FF8A0251A308BEC67DC528150756 (void);
// 0x00000084 System.Void CameraFollow::TriggerNextPoint()
extern void CameraFollow_TriggerNextPoint_m7D7CFE4A39A32129E437EA63D0DBA6EE78DE2B06 (void);
// 0x00000085 System.Void CameraFollow::.ctor()
extern void CameraFollow__ctor_m27AF37B0C19243374F9376EBB2C40A2F605DB16E (void);
// 0x00000086 System.Void GameManager::Start()
extern void GameManager_Start_mD77CCDBF1DA8EC5C3AE7ED955DE4E7F54B79C88E (void);
// 0x00000087 System.Void GameManager::EnableInput()
extern void GameManager_EnableInput_mD556BE9172F5CC6DA49698BEBCED5DA03193012B (void);
// 0x00000088 System.Void GameManager::LoadNextScene()
extern void GameManager_LoadNextScene_m037BC22FA1086B430EF220C9C6D7C2AC20A5103B (void);
// 0x00000089 System.Void GameManager::CallEndGame(System.String,System.Boolean)
extern void GameManager_CallEndGame_mEACB31CD6B2BBCBCC019A0ADCA58CAFEA77845CD (void);
// 0x0000008A System.Void GameManager::PauseInput(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void GameManager_PauseInput_m5347541C8F144F2A9A269CCB57236C5C5CDDC161 (void);
// 0x0000008B System.Void GameManager::PauseGame(System.Boolean)
extern void GameManager_PauseGame_m1DBBD131F2BB1B8CA15F3DC581E738E1CF07624D (void);
// 0x0000008C System.Void GameManager::UnpauseGame()
extern void GameManager_UnpauseGame_mB8759C758F530A1B40D0D5F02A3062B26C32F94B (void);
// 0x0000008D System.Void GameManager::RestartLevel()
extern void GameManager_RestartLevel_m2FA950FABF9CB7D9349F4E7B58253E22BBF92CEC (void);
// 0x0000008E System.Void GameManager::RestartAllLevel()
extern void GameManager_RestartAllLevel_mDAA15362432E00230628C389BDB64CD66F644C35 (void);
// 0x0000008F System.Boolean GameManager::IsGameRunning()
extern void GameManager_IsGameRunning_mE40810AB8B805C906D5DD95871E27C18BEB6E70B (void);
// 0x00000090 System.Collections.IEnumerator GameManager::LoadScene(System.String)
extern void GameManager_LoadScene_m17BE2C6D6E3433DB4FA1A57D8D2ADBC150ED9E5A (void);
// 0x00000091 System.Void GameManager::.ctor()
extern void GameManager__ctor_mF7F1107D38DE91EB8A57C1C3BB1A932C50CD9693 (void);
// 0x00000092 System.Void NextCameraPoint::OnTriggerEnter(UnityEngine.Collider)
extern void NextCameraPoint_OnTriggerEnter_m7177095B39B0C31DC3A1864D0708C5CE4CEDA9D3 (void);
// 0x00000093 System.Void NextCameraPoint::.ctor()
extern void NextCameraPoint__ctor_m0153A89DD3781DE65EC6EA80F4A726F8B44C306E (void);
// 0x00000094 Collectable PoolSystem::GetFromPoolList(CollectableType)
extern void PoolSystem_GetFromPoolList_mAA3E6AAADC28A2FA726483B0A1E46EA733A95B9B (void);
// 0x00000095 System.Void PoolSystem::PutOnList(Collectable)
extern void PoolSystem_PutOnList_m20B0DCF0D2314DAC9DA023BC0F6A2FF2BDB981E6 (void);
// 0x00000096 Collectable PoolSystem::CreateNewCollectable(CollectableType)
extern void PoolSystem_CreateNewCollectable_m72FAD4CB62A3B9B34CB60E187956EBD015977D4A (void);
// 0x00000097 System.Void PoolSystem::CreateListOnPull(CollectableType)
extern void PoolSystem_CreateListOnPull_m2F7E63658FFE755FDA4CB8411E8F94A53FDDE589 (void);
// 0x00000098 System.Void PoolSystem::.ctor()
extern void PoolSystem__ctor_m92523CEDD4A217F1518B3AE5221C1C709F0E2BA7 (void);
// 0x00000099 System.String Scenery::get__nextMap()
extern void Scenery_get__nextMap_mFE1E6535FC2C95DF11859467145B4FB1C1F22932 (void);
// 0x0000009A System.Void Scenery::Awake()
extern void Scenery_Awake_m8A363F4A58DBBF2765CBF4424E8013FEF071734C (void);
// 0x0000009B System.Void Scenery::.ctor()
extern void Scenery__ctor_mBB8DEE0C0F936D4A4D2122A29AC9C39664D90B67 (void);
// 0x0000009C System.Void PauseController::ResumeClick()
extern void PauseController_ResumeClick_mB3D60532CF891AF94D530DAABF34797A15C15B77 (void);
// 0x0000009D System.Void PauseController::RestartClick()
extern void PauseController_RestartClick_mF0B4EBB9F1B56002EE352159EF6FC525D462C0E6 (void);
// 0x0000009E System.Void PauseController::QuitGameClick()
extern void PauseController_QuitGameClick_mEEAEA2DBFCC8E2AE2315814E8EB9C5306ACAAB54 (void);
// 0x0000009F System.Void PauseController::.ctor()
extern void PauseController__ctor_m01CFD46F5CEB580FD8B22258578900425E021CB4 (void);
// 0x000000A0 System.Void Informations::.ctor()
extern void Informations__ctor_m041538C6208D0E57D2DBD5C78193D1E3F086F49B (void);
// 0x000000A1 System.Void PlayerInformations::SaveInformations()
extern void PlayerInformations_SaveInformations_m39B0047E3DA2E8CFA2A074A29B84A3F625F33D69 (void);
// 0x000000A2 System.Void PlayerInformations::LoadInformations()
extern void PlayerInformations_LoadInformations_m9FCE5EB5C85C737B2CA67B2D69C248380858E5A8 (void);
// 0x000000A3 System.Void PlayerInformations::ResetLocalInformations()
extern void PlayerInformations_ResetLocalInformations_mA1CD5D8D4733E3CDE399409867905A1EB9327B8A (void);
// 0x000000A4 System.Void PlayerInformations::.cctor()
extern void PlayerInformations__cctor_m7F773432E256E875CB5064C5A5F8286C7D6C5EC4 (void);
// 0x000000A5 System.Void ProgressPoint::CallNextStage()
extern void ProgressPoint_CallNextStage_m066E81E60146017EBBD042DDF778BE14C98C955B (void);
// 0x000000A6 System.Void ProgressPoint::OnTriggerEnter(UnityEngine.Collider)
extern void ProgressPoint_OnTriggerEnter_m0785B1B0B886F3E7A52C0FADFF1C4E9F5D134A49 (void);
// 0x000000A7 System.Void ProgressPoint::.ctor()
extern void ProgressPoint__ctor_m587510D988022EEC80F233F8C701DA44740BA74B (void);
// 0x000000A8 System.Boolean ServiceLocator::ContainService(T)
// 0x000000A9 System.Void ServiceLocator::Register(System.Object)
// 0x000000AA T ServiceLocator::Resolve()
// 0x000000AB System.Void ServiceLocator::Reset()
extern void ServiceLocator_Reset_mCF23D3BD5BF7570EC2DDCF376E5CF5FDCCCFB20F (void);
// 0x000000AC System.Void ServiceLocator::.cctor()
extern void ServiceLocator__cctor_m254F5AFCA5E373D7D4153A68B0C7F666748C8A9B (void);
// 0x000000AD System.Void CoinUpdater::Awake()
extern void CoinUpdater_Awake_mE7089F2891F96B758573F85BF6EC6D7FFFF845B3 (void);
// 0x000000AE System.Void CoinUpdater::UpdateText()
extern void CoinUpdater_UpdateText_m8DF4FFB491D78ABDE000CB6275722F9929AD5187 (void);
// 0x000000AF System.Void CoinUpdater::.ctor()
extern void CoinUpdater__ctor_m48F598783DD1BDA49B2C7C98914ACA40E1919344 (void);
// 0x000000B0 System.Void EndGameScreen::SetupMessage(System.String,System.Boolean)
extern void EndGameScreen_SetupMessage_m61C4686076064FCCBB75C68791D8B66E29CB65E8 (void);
// 0x000000B1 System.Void EndGameScreen::RestartClick()
extern void EndGameScreen_RestartClick_m3890EED9564BCED4E01909F4374F78344280227E (void);
// 0x000000B2 System.Void EndGameScreen::QuitGameClick()
extern void EndGameScreen_QuitGameClick_m490CFC4C51CA4AD45F506BC2AE9523EF4DD3BCAA (void);
// 0x000000B3 System.Void EndGameScreen::.ctor()
extern void EndGameScreen__ctor_mCEB2EC5114040D599657F1A4A797E31C4EED37AE (void);
// 0x000000B4 System.Void InteractionUpdater::Awake()
extern void InteractionUpdater_Awake_mA1D50B5102722D48C848F708E39245C1E0A0FB03 (void);
// 0x000000B5 System.Void InteractionUpdater::UpdateText(System.Boolean)
extern void InteractionUpdater_UpdateText_m1951509D4BFC59D3D8A5D7D8188E42E209E181DC (void);
// 0x000000B6 System.Void InteractionUpdater::.ctor()
extern void InteractionUpdater__ctor_m61755C53BB9A82B9F2565E9BF0BE4738C2406441 (void);
// 0x000000B7 System.Void UpdaterInteractionHUD::OnLevelWasLoaded(System.Int32)
extern void UpdaterInteractionHUD_OnLevelWasLoaded_m914D96A8782A9FA7E66F7FFF3F6E608E114E82EA (void);
// 0x000000B8 System.Void UpdaterInteractionHUD::OnTriggerEnter(UnityEngine.Collider)
extern void UpdaterInteractionHUD_OnTriggerEnter_m926433D69449BFBB1EEB7657C530B94142755170 (void);
// 0x000000B9 System.Void UpdaterInteractionHUD::OnTriggerExit(UnityEngine.Collider)
extern void UpdaterInteractionHUD_OnTriggerExit_m9288C8C5F76278733F280923208D51CA63AFE109 (void);
// 0x000000BA System.Void UpdaterInteractionHUD::.ctor()
extern void UpdaterInteractionHUD__ctor_mCB5E3FE40CD2EB38E18164063F1C045BA37BBDD3 (void);
// 0x000000BB System.Void WinScreen::Start()
extern void WinScreen_Start_mA6E89311D95BA4DEB6B152D9830AC0DB9B945D67 (void);
// 0x000000BC System.Void WinScreen::Restart()
extern void WinScreen_Restart_m82E25FB1BB5ABBCC32ACF446B73D118789591896 (void);
// 0x000000BD System.Void WinScreen::QuitGame()
extern void WinScreen_QuitGame_mA54BF67872A939862CBA5D65D0953CBAD4539B8E (void);
// 0x000000BE System.Void WinScreen::.ctor()
extern void WinScreen__ctor_m88AE52FC406FC9C6234A7C3479197BCE7803A7DF (void);
// 0x000000BF T SingletonMonoBehaviour`1::get_Instance()
// 0x000000C0 System.Void SingletonMonoBehaviour`1::Awake()
// 0x000000C1 System.Void SingletonMonoBehaviour`1::.ctor()
// 0x000000C2 System.Void CollectableConfiguration::.ctor()
extern void CollectableConfiguration__ctor_mC1E29BADD2CCE630092A94D4A3155BAA663224EC (void);
// 0x000000C3 System.Void CollectableConfiguration::.ctor(CollectableConfiguration)
extern void CollectableConfiguration__ctor_m0A9C39CA0B536728CBF88164C0C8FADEA2C8BDA5 (void);
// 0x000000C4 CollectableConfiguration CollectableData::GetCollectableInformations(CollectableType,System.Int32)
extern void CollectableData_GetCollectableInformations_m5502E57A58711D935DE8B42BB4EDED57C322DCB2 (void);
// 0x000000C5 UnityEngine.Color CollectableData::GetRandomColor()
extern void CollectableData_GetRandomColor_mCDC9701B55C8184BDF6837C4DA78B63B5E312E70 (void);
// 0x000000C6 System.Void CollectableData::.ctor()
extern void CollectableData__ctor_m7B6E1E2EC5B389AE1D0DA890A04E2CDD2FDE5B9B (void);
// 0x000000C7 System.Void MainLeaf.SaveSystem.SaveSystem::SaveData(T)
// 0x000000C8 System.Void MainLeaf.SaveSystem.SaveSystem::LoadData(T&)
// 0x000000C9 System.Boolean MainLeaf.SaveSystem.SaveSystem::HasSavedData()
extern void SaveSystem_HasSavedData_m36D56885F8F131591154A1B14B5A69BF699C0C6E (void);
// 0x000000CA System.Void MainLeaf.SaveSystem.SaveSystem::DeleteAllSaveData()
extern void SaveSystem_DeleteAllSaveData_m0E2DEFDCE70B176D64C68D0E40D9F08564086BDC (void);
// 0x000000CB System.Void InputSettings/MovimentActions::.ctor(InputSettings)
extern void MovimentActions__ctor_mA00A699511FDFAAB4112FE38A3DD675A54EAF4BF (void);
// 0x000000CC UnityEngine.InputSystem.InputAction InputSettings/MovimentActions::get_HorizontalMoviment()
extern void MovimentActions_get_HorizontalMoviment_mA4A14EBD845780B4C881B26EDC6D1C77395A4881 (void);
// 0x000000CD UnityEngine.InputSystem.InputAction InputSettings/MovimentActions::get_Vertical()
extern void MovimentActions_get_Vertical_mB5E4EEFC172506E4C6A511855A6C15418A591B05 (void);
// 0x000000CE UnityEngine.InputSystem.InputAction InputSettings/MovimentActions::get_Jump()
extern void MovimentActions_get_Jump_m4A3488F82C1C446D1AD974E3E8F7A3F12DB086F0 (void);
// 0x000000CF UnityEngine.InputSystem.InputAction InputSettings/MovimentActions::get_Crounch()
extern void MovimentActions_get_Crounch_m5A8A27A24DEC577D902D0A26CCBBAE8E230F9E6F (void);
// 0x000000D0 UnityEngine.InputSystem.InputActionMap InputSettings/MovimentActions::Get()
extern void MovimentActions_Get_m115F22C88007DF96139C4AEE6764B395C488A0A4 (void);
// 0x000000D1 System.Void InputSettings/MovimentActions::Enable()
extern void MovimentActions_Enable_mF6EFD45144C17319EF6FCDC251F6148D5C675C6C (void);
// 0x000000D2 System.Void InputSettings/MovimentActions::Disable()
extern void MovimentActions_Disable_m2691A7EB2354BE8C94449395FDBE8A742222BD54 (void);
// 0x000000D3 System.Boolean InputSettings/MovimentActions::get_enabled()
extern void MovimentActions_get_enabled_m33F86FAB507A0813DAA692DD3FE500A92CFE8CE5 (void);
// 0x000000D4 UnityEngine.InputSystem.InputActionMap InputSettings/MovimentActions::op_Implicit(InputSettings/MovimentActions)
extern void MovimentActions_op_Implicit_m5ACB08638CD7D16A9B73959F93576E4FF2C5E5C7 (void);
// 0x000000D5 System.Void InputSettings/MovimentActions::SetCallbacks(InputSettings/IMovimentActions)
extern void MovimentActions_SetCallbacks_mDB25872EEC3182FF53432D04101D3DA5DE50209F (void);
// 0x000000D6 System.Void InputSettings/InteractionActions::.ctor(InputSettings)
extern void InteractionActions__ctor_mE807502D1FF73EB6DAB84B09AB5AC6736AA9C384 (void);
// 0x000000D7 UnityEngine.InputSystem.InputAction InputSettings/InteractionActions::get_BasicInteraction()
extern void InteractionActions_get_BasicInteraction_m1D9656BBCB97AC2CE86F5099F03DFAEC8AC3B5BA (void);
// 0x000000D8 UnityEngine.InputSystem.InputActionMap InputSettings/InteractionActions::Get()
extern void InteractionActions_Get_m41BFA5428E3E17049BE5FD619A1D18C2622C7457 (void);
// 0x000000D9 System.Void InputSettings/InteractionActions::Enable()
extern void InteractionActions_Enable_mC5D13278342F5D1608912C07619DB934A5F5DCDA (void);
// 0x000000DA System.Void InputSettings/InteractionActions::Disable()
extern void InteractionActions_Disable_mEAD4A94D8A6BF4F9D76E3519C0F7048F474A7241 (void);
// 0x000000DB System.Boolean InputSettings/InteractionActions::get_enabled()
extern void InteractionActions_get_enabled_mA5C574CAE52E796135AB8F63C4D4A6ACE1C930FF (void);
// 0x000000DC UnityEngine.InputSystem.InputActionMap InputSettings/InteractionActions::op_Implicit(InputSettings/InteractionActions)
extern void InteractionActions_op_Implicit_m401093D72F3B8DF6681AB16D2B734342EB4E0AD5 (void);
// 0x000000DD System.Void InputSettings/InteractionActions::SetCallbacks(InputSettings/IInteractionActions)
extern void InteractionActions_SetCallbacks_m156BF1D58260DDED6D0F48E064146EDEBE1B3D93 (void);
// 0x000000DE System.Void InputSettings/PauseActions::.ctor(InputSettings)
extern void PauseActions__ctor_m8B81C5DECB939C6B51EF23DFDEA326D4A6766A94 (void);
// 0x000000DF UnityEngine.InputSystem.InputAction InputSettings/PauseActions::get_PauseTrigger()
extern void PauseActions_get_PauseTrigger_m3E2FE52839399C76FF9A172480F5FDE6A8DBBB37 (void);
// 0x000000E0 UnityEngine.InputSystem.InputActionMap InputSettings/PauseActions::Get()
extern void PauseActions_Get_mA0F9B80D0B295918E7EB20743825B60C9E149CBE (void);
// 0x000000E1 System.Void InputSettings/PauseActions::Enable()
extern void PauseActions_Enable_m481E3346D4A1D15697F6AB78ED77A9D3440406D7 (void);
// 0x000000E2 System.Void InputSettings/PauseActions::Disable()
extern void PauseActions_Disable_mB7095F13D8652252984507B6A9084B14CB82E97A (void);
// 0x000000E3 System.Boolean InputSettings/PauseActions::get_enabled()
extern void PauseActions_get_enabled_m1B202081F66FA94889A3E4B6386B600640D7AB3B (void);
// 0x000000E4 UnityEngine.InputSystem.InputActionMap InputSettings/PauseActions::op_Implicit(InputSettings/PauseActions)
extern void PauseActions_op_Implicit_m88013223AADEB38B19BBB8421F7FBF1F0DF96783 (void);
// 0x000000E5 System.Void InputSettings/PauseActions::SetCallbacks(InputSettings/IPauseActions)
extern void PauseActions_SetCallbacks_m528C82A200766B6A393648409FB0273875F457E8 (void);
// 0x000000E6 System.Void InputSettings/IMovimentActions::OnHorizontalMoviment(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x000000E7 System.Void InputSettings/IMovimentActions::OnVertical(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x000000E8 System.Void InputSettings/IMovimentActions::OnJump(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x000000E9 System.Void InputSettings/IMovimentActions::OnCrounch(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x000000EA System.Void InputSettings/IInteractionActions::OnBasicInteraction(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x000000EB System.Void InputSettings/IPauseActions::OnPauseTrigger(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x000000EC System.Void EnemyBehaviour/<SetNextTarget>d__8::MoveNext()
extern void U3CSetNextTargetU3Ed__8_MoveNext_mD2B11DFB0DFD8F0E370C1E51FDBBF1589FB280C6 (void);
// 0x000000ED System.Void EnemyBehaviour/<SetNextTarget>d__8::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSetNextTargetU3Ed__8_SetStateMachine_m1040D950004C15E24BFBC9391631E00CA2D2C134 (void);
// 0x000000EE System.Void GameManager/<LoadScene>d__16::.ctor(System.Int32)
extern void U3CLoadSceneU3Ed__16__ctor_mE1A9BA58509B7FB702E403623EBCD8A9F05B5924 (void);
// 0x000000EF System.Void GameManager/<LoadScene>d__16::System.IDisposable.Dispose()
extern void U3CLoadSceneU3Ed__16_System_IDisposable_Dispose_m2BFC079564895AC8D202C826F9B9B1F565484601 (void);
// 0x000000F0 System.Boolean GameManager/<LoadScene>d__16::MoveNext()
extern void U3CLoadSceneU3Ed__16_MoveNext_m185DE2998A2CCFB89307EDF93671D8CBD24D72BA (void);
// 0x000000F1 System.Object GameManager/<LoadScene>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadSceneU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE4A74973C0667F8ACE296AAC36BA50CABCF0C9D6 (void);
// 0x000000F2 System.Void GameManager/<LoadScene>d__16::System.Collections.IEnumerator.Reset()
extern void U3CLoadSceneU3Ed__16_System_Collections_IEnumerator_Reset_mDF942E7CB9F2BCBAB50C5920B2F70392C20A0D9F (void);
// 0x000000F3 System.Object GameManager/<LoadScene>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CLoadSceneU3Ed__16_System_Collections_IEnumerator_get_Current_m612A9E4B9816F8FF5D36C2D744435BAEBDAB0393 (void);
static Il2CppMethodPointer s_methodPointers[243] = 
{
	InputSettings_get_asset_m4236597DD28CD00C8176AC8FCA3BE387D37028EA,
	InputSettings__ctor_mEE801DA7B6C60FCCCED5F04E1972B502AA39BA92,
	InputSettings_Dispose_mA98F9FDA5DFAF52A0477CF7A13DED399C933C96D,
	InputSettings_get_bindingMask_m7BA71AAD392BC7947C1C7DD695066D8977029DF9,
	InputSettings_set_bindingMask_m3721DA2514F553320ECE65CB8035D80C80EACEA5,
	InputSettings_get_devices_mDA1C1EFCC2873E104A6E06A62ECC08ED29257C5D,
	InputSettings_set_devices_m166EC41F78E60EF1A278FC6474E040FDBC838FEF,
	InputSettings_get_controlSchemes_mEA3E9FD66B2FC66DF1160F50E00F592FF15AD62C,
	InputSettings_Contains_m2EDC0C9F4C0A92D23B7211EC9A2277242259B879,
	InputSettings_GetEnumerator_m3781E4070EB7EB84D114DFC11B57ACEB3F3E8267,
	InputSettings_System_Collections_IEnumerable_GetEnumerator_mE50ADD3A3ECA7BD26867A3B0B1152CAA57AA99A6,
	InputSettings_Enable_mE655C8F3B4703A9974102FF4E34D692ED67EE355,
	InputSettings_Disable_m2DC68D0B5B238EB23DB3709DDCCB30A013BD2026,
	InputSettings_get_Moviment_m3D0E760760C58088DD6E5423C45695BECC8F19AD,
	InputSettings_get_Interaction_m63FEB93219AC2C32D746614778FA7C8A01E3909F,
	InputSettings_get_Pause_mDACE07FC85EA27778B4EBB137048B5CDA076195A,
	InputSettings_get_PCScheme_m7623D6F71FD64F19D0C2C616E263C1544A99FD97,
	CharacterBase_GetCharacterRb_mE1DF98BB5895403C5927A63DE484BA4FF447053D,
	CharacterBase_FixedUpdate_m7CBF120D8FB2F2728EECBBC4235C58FF7132B985,
	CharacterBase_WalkVerifications_m703F0B7422146A7FAA223B6FF7AF079FF7761E02,
	NULL,
	CharacterBase_ApplyForwardVelocity_m30B47CB0CEF3A34C40D2BB6907DBCDCA805EF0F8,
	CharacterBase_ApplyBackwardVelocity_mF1D1BB7B6F7A80C19E1D5DC94FFF41C956B0FBB4,
	CharacterBase_UpdateAnimator_m2E535CC66B1D82B44B98CEADF2BB2F248F89682D,
	CharacterBase__ctor_m6E960F31ADCE737E0EC02758D9A52F7D2AC26AEE,
	EnemyBehaviour_Awake_mA55367702DF0688184ED6D220E15D1FA4B77B699,
	EnemyBehaviour_WalkVerifications_m4D5416846E7EBB0C7EBFF9CD3C62038879A2FBA2,
	EnemyBehaviour_CheckCorrectTarget_m8669732047422D04933979AF864AE889F9A86939,
	EnemyBehaviour_ReachTarget_mFC9798693541A76E3D8CC68A48ED21BDE42E55B8,
	EnemyBehaviour_SetNextTarget_mB8CF35776B9E6C8B70C34532FDE9E3367D3A6EE9,
	EnemyBehaviour_UpdateRotation_mA1B3B311CE6BE333239B0E74BE691CFD140A56B7,
	EnemyBehaviour__ctor_m3721742BCD67360F80567F0090EAA9486500899A,
	PlayerTrigger_Update_m2FCD9BAC40922DF919449C259F9EAD953AC3301D,
	PlayerTrigger_OnTriggerEnter_m071D5B38AD4454EC989F5522986AF464C9F42B47,
	PlayerTrigger__ctor_mBFE94A3385FEA7CC23F08E54A290C1552A791286,
	TargetPoint_Awake_m3FE8F2EF2B160716707D93032D7F901C742F3875,
	TargetPoint_OnTriggerEnter_mC90A500F7C222479DB431F6EDFA80A620C6408C7,
	TargetPoint__ctor_m0798798B87EFD5D2896EF1E58FBB0FEFECB1B8C4,
	NULL,
	Player_get__playerModelTransform_mB5809DCE5774ED29C94E552EE1FADBAC82094225,
	Player_get__duringPushing_mB2BBFAF86D0A3EC8C3F2AFFAD5D7702F8250994B,
	Player_set__duringPushing_m3FA99528CB208EEFA198123A0F127A04E84E47B5,
	Player_get__playerInput_mB18FFC96EAA4878902CA2B06A720AD7E31D2B734,
	Player_Awake_mD94498D36D7F1E5FDB727209B4B6043A6F228E5C,
	Player_Update_m6F977BAE3756AB7073D64042B766B442E4EC6FD2,
	Player_WalkVerifications_m4254864C0A88290D8E9AAE29F6484FD2B0D2B0DB,
	Player_UpdateRotation_mCF953FC5A034CC35D9E6EA7C6A3897F1A72D55B3,
	Player_UpdateAnimator_m39750AAADBE5625180E38D990F8AEEBADC430B60,
	Player_ApplyBackwardVelocity_m37827B1378EC626801F6E8BD3D8FE097E721B152,
	Player_ApplyForwardVelocity_m158761ABCC708B1BE0BD0B59FA500B7AFFFA634F,
	Player_ResetVelocity_m300429D0271EB77ACF0CB851DA3C59EFEB8BCF32,
	Player_UpdateCrounchState_mE57DA62AE973A5735E23CA8E0F4FB477718E0BF8,
	Player__ctor_m8F4AB650C5E2DE406B3C65EA8F662013458D85E2,
	PlayerComponent_Initialize_mB4058A1A2CCB36216CFB483EB33EAAC9DD538FFF,
	PlayerComponent__ctor_m7C251856B197D64A0685D266C1908EF0A71FD027,
	PlayerCrounch_Initialize_mD1166AA7513688A7BC8C0E75EE8646B7326B151D,
	PlayerCrounch_OnEnable_m963C48DF4150337DEC7ABAC1BDF3429A18DC34CC,
	PlayerCrounch_OnDisable_m657177C58B9FED2E6452FD96DB7E15B5BEB20DC6,
	PlayerCrounch_ActivateCrounchState_m57D0B31E97DD912F5B7F49DA1D41DD41EB71316B,
	PlayerCrounch_DesactivateCrounchState_mA4ACA73CBF57896E96163424F5FF31C1ABA002A8,
	PlayerCrounch_CanCrounch_m96E449451D7CFA6386D4A1B5D98DA383EC180CB3,
	PlayerCrounch__ctor_m25985836C11DBB0C7E5EEEBD0EE29D5E1A5290DC,
	PlayerInput_add_JumpAction_mEE8FA46E9CA26DD2A087DE523E7307635535A3DB,
	PlayerInput_remove_JumpAction_mD8A5CFC5DBC0D2AC2D51FA354BBC446B2DD92CB5,
	PlayerInput_add_CrounchAction_m9AD3D01AD8D8558696B888C93005C5F4E4D04509,
	PlayerInput_remove_CrounchAction_m91C437C7363AA1C6E10A02287CAAB117C5197747,
	PlayerInput_add_UnCrounchAction_m82ABCFE5FF3174DBE78F7B19558B267DFAD5BAD4,
	PlayerInput_remove_UnCrounchAction_m57495D95768E8689B12EA781B5B4D9879973AFD4,
	PlayerInput_add_GrabAction_m91D7CC500BC80848621C90D0BB0C627265C6DB3A,
	PlayerInput_remove_GrabAction_mB1A55857AF5B0174D1B62A0EC553BC7BBB332C87,
	PlayerInput_add_UnGrabAction_m4BBC719F34D064195052A6211B7A372848366854,
	PlayerInput_remove_UnGrabAction_m4BB5518F86036C102F4A06E25BD7E2A4A9CD3386,
	PlayerInput_Initialize_m53BB711E0285EE4199361A8DE4201541469D7592,
	PlayerInput_OnDisable_m660D1B6EC3D536D232000F07339781DE1DE3CE76,
	PlayerInput_EnableInput_m49708CED0661D906F4C089AA069AF4FE4EB3665B,
	PlayerInput_DisableInput_m211D7C2D4FCA1B965460DA1F2339CB4D07F00917,
	PlayerInput_CrounchInput_mED3CDD5A5F62248C0E2A941AE28DA90FD96D3207,
	PlayerInput_UnCrounchInput_mBAF046AAD1DE824EEA32061E1A5F039AE95387AF,
	PlayerInput_JumpInput_mF9EDA88B41430F05B189D552B1C722297DFAF845,
	PlayerInput_GrabInput_m0EEBA7EAA2ADE12AB8CE9380A655C044D299CCF1,
	PlayerInput_UnGrabInput_mAF23B799E38918F474BE3C1FDEFD26B2A9947802,
	PlayerInput__ctor_mA6A5B5AF425706AE2F809DC30DE2E16C92BFC7C7,
	PlayerInteraction_Initialize_m78D0185D86B0893CB16013C99018A0C946D30B13,
	PlayerInteraction_OnEnable_m943D501F408CD61F2F82975B1D189566818720E9,
	PlayerInteraction_OnDisable_mA25EA9C9057DBB2F4CB12FE227C39843F4C98C5B,
	PlayerInteraction_GrabAction_m38546C1D883216EDFE97A7DC4184DAEB02E475EF,
	PlayerInteraction_UnGrabAction_m7ED2E4C1275F466020B9625C74F5B5A43C964AFF,
	PlayerInteraction_OnTriggerEnter_mE0CBCD17DB9910EFA43894F40E891DE8FB61F393,
	PlayerInteraction__ctor_mD1058C5969AC6F66FF838496BD1D88CB7B6F676C,
	PlayerJump_Initialize_m235E4085E2B8F3C4B88242D03FC4FE0543539B82,
	PlayerJump_OnEnable_mD21359467FD01E9057357F8DAD623248237467BF,
	PlayerJump_OnDisable_mC2DAA99278ECF8E9583DA2BBD1CE40D1879FF3FF,
	PlayerJump_Jump_mE614C1B09152248C01FA4AB39D25587D885C4197,
	PlayerJump_HasGrabPossibilities_m5F80AABA4278F34E7EFD4C4B0BB39112D1C2CBEC,
	PlayerJump_EnableToJump_m919451D3889108EE9FF0991F1B33AB456DD00FFE,
	PlayerJump__ctor_mD3F62AD9206A237A736AECD109B82988DF9316AC,
	Coin_OnValidate_mC18F67293EBB470B49912BB65557D2C96E7752FC,
	Coin_ReadyToCollect_m8D63FBF97A4A5772FD7B04D6FA8A14C712C75E73,
	Coin_TryToCollect_m024146A256BA1FA7934882C2549EC7640651FF41,
	Coin__ctor_m95BF6BF6C4B1A55ED5CAE43FB49371C56E990350,
	Collectable_OnValidate_m975921EA291C0DEC3F7E28404EFD837D3B617812,
	Collectable_Start_m2E96ECB5BBD424083DF2305219FBCBAD6E6B3F7A,
	Collectable_SetupCollectable_m410E25E562190A77497C79059E0EB209981EE6E9,
	NULL,
	NULL,
	Collectable__ctor_m2BB5693EB0E8D466151D66585FB889275D811358,
	Grabbable_GetRigidbody_m282D83730035C698FB7F1766FBE07B9E2A662423,
	Grabbable_Start_m275A574B74E6CA0D1DC0291F2314BE662FC9AD5C,
	Grabbable_FixedUpdate_m795FE5182137F8800C4B7BDE6F7B2D2A3309A598,
	Grabbable_Interaction_m89C72C5D11C92AE4A3EB471C0205179E2B398F18,
	Grabbable_DisableInteraction_m5EE9E530620062BB552D1136A5F7AD03C07283B4,
	Grabbable_ChangeGrabState_m0C43D48B5BBE30048B6A178F56703841618ABA44,
	Grabbable_ReachDestiny_mA3D31751C25848A46CB1D99E6E01DA8E32F94564,
	Grabbable__ctor_m641A1F81714AF055A1F85C7FC8FDECF888112138,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InteractableBase_EnableToInteract_m1693A81295DC53E0FCB840353F1D29A4CBAC518C,
	NULL,
	InteractableBase_SetLayer_m781F7546FF93DE383DA15960D51DD7A83967FA6F,
	InteractableBase_TryToInteract_mAED344B1E13375A47FC1028F29FC2B8A5833C305,
	InteractableBase__ctor_m33DDA9BFED31F92EA307D56615CB7E803FE92201,
	ReachChecker_OnValidate_m727BAD1402CD4D84556B582F9D998A84B3DAA7C6,
	ReachChecker_OnTriggerEnter_mDEB35E1007DC36D11B6A40BD1E0AB266206513B6,
	ReachChecker__ctor_m95248DD5648DF9FD245A63092AFFBFC9A45945F0,
	CameraFollow_FixedUpdate_mBF9AC25722C9FF8A0251A308BEC67DC528150756,
	CameraFollow_TriggerNextPoint_m7D7CFE4A39A32129E437EA63D0DBA6EE78DE2B06,
	CameraFollow__ctor_m27AF37B0C19243374F9376EBB2C40A2F605DB16E,
	GameManager_Start_mD77CCDBF1DA8EC5C3AE7ED955DE4E7F54B79C88E,
	GameManager_EnableInput_mD556BE9172F5CC6DA49698BEBCED5DA03193012B,
	GameManager_LoadNextScene_m037BC22FA1086B430EF220C9C6D7C2AC20A5103B,
	GameManager_CallEndGame_mEACB31CD6B2BBCBCC019A0ADCA58CAFEA77845CD,
	GameManager_PauseInput_m5347541C8F144F2A9A269CCB57236C5C5CDDC161,
	GameManager_PauseGame_m1DBBD131F2BB1B8CA15F3DC581E738E1CF07624D,
	GameManager_UnpauseGame_mB8759C758F530A1B40D0D5F02A3062B26C32F94B,
	GameManager_RestartLevel_m2FA950FABF9CB7D9349F4E7B58253E22BBF92CEC,
	GameManager_RestartAllLevel_mDAA15362432E00230628C389BDB64CD66F644C35,
	GameManager_IsGameRunning_mE40810AB8B805C906D5DD95871E27C18BEB6E70B,
	GameManager_LoadScene_m17BE2C6D6E3433DB4FA1A57D8D2ADBC150ED9E5A,
	GameManager__ctor_mF7F1107D38DE91EB8A57C1C3BB1A932C50CD9693,
	NextCameraPoint_OnTriggerEnter_m7177095B39B0C31DC3A1864D0708C5CE4CEDA9D3,
	NextCameraPoint__ctor_m0153A89DD3781DE65EC6EA80F4A726F8B44C306E,
	PoolSystem_GetFromPoolList_mAA3E6AAADC28A2FA726483B0A1E46EA733A95B9B,
	PoolSystem_PutOnList_m20B0DCF0D2314DAC9DA023BC0F6A2FF2BDB981E6,
	PoolSystem_CreateNewCollectable_m72FAD4CB62A3B9B34CB60E187956EBD015977D4A,
	PoolSystem_CreateListOnPull_m2F7E63658FFE755FDA4CB8411E8F94A53FDDE589,
	PoolSystem__ctor_m92523CEDD4A217F1518B3AE5221C1C709F0E2BA7,
	Scenery_get__nextMap_mFE1E6535FC2C95DF11859467145B4FB1C1F22932,
	Scenery_Awake_m8A363F4A58DBBF2765CBF4424E8013FEF071734C,
	Scenery__ctor_mBB8DEE0C0F936D4A4D2122A29AC9C39664D90B67,
	PauseController_ResumeClick_mB3D60532CF891AF94D530DAABF34797A15C15B77,
	PauseController_RestartClick_mF0B4EBB9F1B56002EE352159EF6FC525D462C0E6,
	PauseController_QuitGameClick_mEEAEA2DBFCC8E2AE2315814E8EB9C5306ACAAB54,
	PauseController__ctor_m01CFD46F5CEB580FD8B22258578900425E021CB4,
	Informations__ctor_m041538C6208D0E57D2DBD5C78193D1E3F086F49B,
	PlayerInformations_SaveInformations_m39B0047E3DA2E8CFA2A074A29B84A3F625F33D69,
	PlayerInformations_LoadInformations_m9FCE5EB5C85C737B2CA67B2D69C248380858E5A8,
	PlayerInformations_ResetLocalInformations_mA1CD5D8D4733E3CDE399409867905A1EB9327B8A,
	PlayerInformations__cctor_m7F773432E256E875CB5064C5A5F8286C7D6C5EC4,
	ProgressPoint_CallNextStage_m066E81E60146017EBBD042DDF778BE14C98C955B,
	ProgressPoint_OnTriggerEnter_m0785B1B0B886F3E7A52C0FADFF1C4E9F5D134A49,
	ProgressPoint__ctor_m587510D988022EEC80F233F8C701DA44740BA74B,
	NULL,
	NULL,
	NULL,
	ServiceLocator_Reset_mCF23D3BD5BF7570EC2DDCF376E5CF5FDCCCFB20F,
	ServiceLocator__cctor_m254F5AFCA5E373D7D4153A68B0C7F666748C8A9B,
	CoinUpdater_Awake_mE7089F2891F96B758573F85BF6EC6D7FFFF845B3,
	CoinUpdater_UpdateText_m8DF4FFB491D78ABDE000CB6275722F9929AD5187,
	CoinUpdater__ctor_m48F598783DD1BDA49B2C7C98914ACA40E1919344,
	EndGameScreen_SetupMessage_m61C4686076064FCCBB75C68791D8B66E29CB65E8,
	EndGameScreen_RestartClick_m3890EED9564BCED4E01909F4374F78344280227E,
	EndGameScreen_QuitGameClick_m490CFC4C51CA4AD45F506BC2AE9523EF4DD3BCAA,
	EndGameScreen__ctor_mCEB2EC5114040D599657F1A4A797E31C4EED37AE,
	InteractionUpdater_Awake_mA1D50B5102722D48C848F708E39245C1E0A0FB03,
	InteractionUpdater_UpdateText_m1951509D4BFC59D3D8A5D7D8188E42E209E181DC,
	InteractionUpdater__ctor_m61755C53BB9A82B9F2565E9BF0BE4738C2406441,
	UpdaterInteractionHUD_OnLevelWasLoaded_m914D96A8782A9FA7E66F7FFF3F6E608E114E82EA,
	UpdaterInteractionHUD_OnTriggerEnter_m926433D69449BFBB1EEB7657C530B94142755170,
	UpdaterInteractionHUD_OnTriggerExit_m9288C8C5F76278733F280923208D51CA63AFE109,
	UpdaterInteractionHUD__ctor_mCB5E3FE40CD2EB38E18164063F1C045BA37BBDD3,
	WinScreen_Start_mA6E89311D95BA4DEB6B152D9830AC0DB9B945D67,
	WinScreen_Restart_m82E25FB1BB5ABBCC32ACF446B73D118789591896,
	WinScreen_QuitGame_mA54BF67872A939862CBA5D65D0953CBAD4539B8E,
	WinScreen__ctor_m88AE52FC406FC9C6234A7C3479197BCE7803A7DF,
	NULL,
	NULL,
	NULL,
	CollectableConfiguration__ctor_mC1E29BADD2CCE630092A94D4A3155BAA663224EC,
	CollectableConfiguration__ctor_m0A9C39CA0B536728CBF88164C0C8FADEA2C8BDA5,
	CollectableData_GetCollectableInformations_m5502E57A58711D935DE8B42BB4EDED57C322DCB2,
	CollectableData_GetRandomColor_mCDC9701B55C8184BDF6837C4DA78B63B5E312E70,
	CollectableData__ctor_m7B6E1E2EC5B389AE1D0DA890A04E2CDD2FDE5B9B,
	NULL,
	NULL,
	SaveSystem_HasSavedData_m36D56885F8F131591154A1B14B5A69BF699C0C6E,
	SaveSystem_DeleteAllSaveData_m0E2DEFDCE70B176D64C68D0E40D9F08564086BDC,
	MovimentActions__ctor_mA00A699511FDFAAB4112FE38A3DD675A54EAF4BF,
	MovimentActions_get_HorizontalMoviment_mA4A14EBD845780B4C881B26EDC6D1C77395A4881,
	MovimentActions_get_Vertical_mB5E4EEFC172506E4C6A511855A6C15418A591B05,
	MovimentActions_get_Jump_m4A3488F82C1C446D1AD974E3E8F7A3F12DB086F0,
	MovimentActions_get_Crounch_m5A8A27A24DEC577D902D0A26CCBBAE8E230F9E6F,
	MovimentActions_Get_m115F22C88007DF96139C4AEE6764B395C488A0A4,
	MovimentActions_Enable_mF6EFD45144C17319EF6FCDC251F6148D5C675C6C,
	MovimentActions_Disable_m2691A7EB2354BE8C94449395FDBE8A742222BD54,
	MovimentActions_get_enabled_m33F86FAB507A0813DAA692DD3FE500A92CFE8CE5,
	MovimentActions_op_Implicit_m5ACB08638CD7D16A9B73959F93576E4FF2C5E5C7,
	MovimentActions_SetCallbacks_mDB25872EEC3182FF53432D04101D3DA5DE50209F,
	InteractionActions__ctor_mE807502D1FF73EB6DAB84B09AB5AC6736AA9C384,
	InteractionActions_get_BasicInteraction_m1D9656BBCB97AC2CE86F5099F03DFAEC8AC3B5BA,
	InteractionActions_Get_m41BFA5428E3E17049BE5FD619A1D18C2622C7457,
	InteractionActions_Enable_mC5D13278342F5D1608912C07619DB934A5F5DCDA,
	InteractionActions_Disable_mEAD4A94D8A6BF4F9D76E3519C0F7048F474A7241,
	InteractionActions_get_enabled_mA5C574CAE52E796135AB8F63C4D4A6ACE1C930FF,
	InteractionActions_op_Implicit_m401093D72F3B8DF6681AB16D2B734342EB4E0AD5,
	InteractionActions_SetCallbacks_m156BF1D58260DDED6D0F48E064146EDEBE1B3D93,
	PauseActions__ctor_m8B81C5DECB939C6B51EF23DFDEA326D4A6766A94,
	PauseActions_get_PauseTrigger_m3E2FE52839399C76FF9A172480F5FDE6A8DBBB37,
	PauseActions_Get_mA0F9B80D0B295918E7EB20743825B60C9E149CBE,
	PauseActions_Enable_m481E3346D4A1D15697F6AB78ED77A9D3440406D7,
	PauseActions_Disable_mB7095F13D8652252984507B6A9084B14CB82E97A,
	PauseActions_get_enabled_m1B202081F66FA94889A3E4B6386B600640D7AB3B,
	PauseActions_op_Implicit_m88013223AADEB38B19BBB8421F7FBF1F0DF96783,
	PauseActions_SetCallbacks_m528C82A200766B6A393648409FB0273875F457E8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CSetNextTargetU3Ed__8_MoveNext_mD2B11DFB0DFD8F0E370C1E51FDBBF1589FB280C6,
	U3CSetNextTargetU3Ed__8_SetStateMachine_m1040D950004C15E24BFBC9391631E00CA2D2C134,
	U3CLoadSceneU3Ed__16__ctor_mE1A9BA58509B7FB702E403623EBCD8A9F05B5924,
	U3CLoadSceneU3Ed__16_System_IDisposable_Dispose_m2BFC079564895AC8D202C826F9B9B1F565484601,
	U3CLoadSceneU3Ed__16_MoveNext_m185DE2998A2CCFB89307EDF93671D8CBD24D72BA,
	U3CLoadSceneU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE4A74973C0667F8ACE296AAC36BA50CABCF0C9D6,
	U3CLoadSceneU3Ed__16_System_Collections_IEnumerator_Reset_mDF942E7CB9F2BCBAB50C5920B2F70392C20A0D9F,
	U3CLoadSceneU3Ed__16_System_Collections_IEnumerator_get_Current_m612A9E4B9816F8FF5D36C2D744435BAEBDAB0393,
};
extern void MovimentActions__ctor_mA00A699511FDFAAB4112FE38A3DD675A54EAF4BF_AdjustorThunk (void);
extern void MovimentActions_get_HorizontalMoviment_mA4A14EBD845780B4C881B26EDC6D1C77395A4881_AdjustorThunk (void);
extern void MovimentActions_get_Vertical_mB5E4EEFC172506E4C6A511855A6C15418A591B05_AdjustorThunk (void);
extern void MovimentActions_get_Jump_m4A3488F82C1C446D1AD974E3E8F7A3F12DB086F0_AdjustorThunk (void);
extern void MovimentActions_get_Crounch_m5A8A27A24DEC577D902D0A26CCBBAE8E230F9E6F_AdjustorThunk (void);
extern void MovimentActions_Get_m115F22C88007DF96139C4AEE6764B395C488A0A4_AdjustorThunk (void);
extern void MovimentActions_Enable_mF6EFD45144C17319EF6FCDC251F6148D5C675C6C_AdjustorThunk (void);
extern void MovimentActions_Disable_m2691A7EB2354BE8C94449395FDBE8A742222BD54_AdjustorThunk (void);
extern void MovimentActions_get_enabled_m33F86FAB507A0813DAA692DD3FE500A92CFE8CE5_AdjustorThunk (void);
extern void MovimentActions_SetCallbacks_mDB25872EEC3182FF53432D04101D3DA5DE50209F_AdjustorThunk (void);
extern void InteractionActions__ctor_mE807502D1FF73EB6DAB84B09AB5AC6736AA9C384_AdjustorThunk (void);
extern void InteractionActions_get_BasicInteraction_m1D9656BBCB97AC2CE86F5099F03DFAEC8AC3B5BA_AdjustorThunk (void);
extern void InteractionActions_Get_m41BFA5428E3E17049BE5FD619A1D18C2622C7457_AdjustorThunk (void);
extern void InteractionActions_Enable_mC5D13278342F5D1608912C07619DB934A5F5DCDA_AdjustorThunk (void);
extern void InteractionActions_Disable_mEAD4A94D8A6BF4F9D76E3519C0F7048F474A7241_AdjustorThunk (void);
extern void InteractionActions_get_enabled_mA5C574CAE52E796135AB8F63C4D4A6ACE1C930FF_AdjustorThunk (void);
extern void InteractionActions_SetCallbacks_m156BF1D58260DDED6D0F48E064146EDEBE1B3D93_AdjustorThunk (void);
extern void PauseActions__ctor_m8B81C5DECB939C6B51EF23DFDEA326D4A6766A94_AdjustorThunk (void);
extern void PauseActions_get_PauseTrigger_m3E2FE52839399C76FF9A172480F5FDE6A8DBBB37_AdjustorThunk (void);
extern void PauseActions_Get_mA0F9B80D0B295918E7EB20743825B60C9E149CBE_AdjustorThunk (void);
extern void PauseActions_Enable_m481E3346D4A1D15697F6AB78ED77A9D3440406D7_AdjustorThunk (void);
extern void PauseActions_Disable_mB7095F13D8652252984507B6A9084B14CB82E97A_AdjustorThunk (void);
extern void PauseActions_get_enabled_m1B202081F66FA94889A3E4B6386B600640D7AB3B_AdjustorThunk (void);
extern void PauseActions_SetCallbacks_m528C82A200766B6A393648409FB0273875F457E8_AdjustorThunk (void);
extern void U3CSetNextTargetU3Ed__8_MoveNext_mD2B11DFB0DFD8F0E370C1E51FDBBF1589FB280C6_AdjustorThunk (void);
extern void U3CSetNextTargetU3Ed__8_SetStateMachine_m1040D950004C15E24BFBC9391631E00CA2D2C134_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[26] = 
{
	{ 0x060000CB, MovimentActions__ctor_mA00A699511FDFAAB4112FE38A3DD675A54EAF4BF_AdjustorThunk },
	{ 0x060000CC, MovimentActions_get_HorizontalMoviment_mA4A14EBD845780B4C881B26EDC6D1C77395A4881_AdjustorThunk },
	{ 0x060000CD, MovimentActions_get_Vertical_mB5E4EEFC172506E4C6A511855A6C15418A591B05_AdjustorThunk },
	{ 0x060000CE, MovimentActions_get_Jump_m4A3488F82C1C446D1AD974E3E8F7A3F12DB086F0_AdjustorThunk },
	{ 0x060000CF, MovimentActions_get_Crounch_m5A8A27A24DEC577D902D0A26CCBBAE8E230F9E6F_AdjustorThunk },
	{ 0x060000D0, MovimentActions_Get_m115F22C88007DF96139C4AEE6764B395C488A0A4_AdjustorThunk },
	{ 0x060000D1, MovimentActions_Enable_mF6EFD45144C17319EF6FCDC251F6148D5C675C6C_AdjustorThunk },
	{ 0x060000D2, MovimentActions_Disable_m2691A7EB2354BE8C94449395FDBE8A742222BD54_AdjustorThunk },
	{ 0x060000D3, MovimentActions_get_enabled_m33F86FAB507A0813DAA692DD3FE500A92CFE8CE5_AdjustorThunk },
	{ 0x060000D5, MovimentActions_SetCallbacks_mDB25872EEC3182FF53432D04101D3DA5DE50209F_AdjustorThunk },
	{ 0x060000D6, InteractionActions__ctor_mE807502D1FF73EB6DAB84B09AB5AC6736AA9C384_AdjustorThunk },
	{ 0x060000D7, InteractionActions_get_BasicInteraction_m1D9656BBCB97AC2CE86F5099F03DFAEC8AC3B5BA_AdjustorThunk },
	{ 0x060000D8, InteractionActions_Get_m41BFA5428E3E17049BE5FD619A1D18C2622C7457_AdjustorThunk },
	{ 0x060000D9, InteractionActions_Enable_mC5D13278342F5D1608912C07619DB934A5F5DCDA_AdjustorThunk },
	{ 0x060000DA, InteractionActions_Disable_mEAD4A94D8A6BF4F9D76E3519C0F7048F474A7241_AdjustorThunk },
	{ 0x060000DB, InteractionActions_get_enabled_mA5C574CAE52E796135AB8F63C4D4A6ACE1C930FF_AdjustorThunk },
	{ 0x060000DD, InteractionActions_SetCallbacks_m156BF1D58260DDED6D0F48E064146EDEBE1B3D93_AdjustorThunk },
	{ 0x060000DE, PauseActions__ctor_m8B81C5DECB939C6B51EF23DFDEA326D4A6766A94_AdjustorThunk },
	{ 0x060000DF, PauseActions_get_PauseTrigger_m3E2FE52839399C76FF9A172480F5FDE6A8DBBB37_AdjustorThunk },
	{ 0x060000E0, PauseActions_Get_mA0F9B80D0B295918E7EB20743825B60C9E149CBE_AdjustorThunk },
	{ 0x060000E1, PauseActions_Enable_m481E3346D4A1D15697F6AB78ED77A9D3440406D7_AdjustorThunk },
	{ 0x060000E2, PauseActions_Disable_mB7095F13D8652252984507B6A9084B14CB82E97A_AdjustorThunk },
	{ 0x060000E3, PauseActions_get_enabled_m1B202081F66FA94889A3E4B6386B600640D7AB3B_AdjustorThunk },
	{ 0x060000E5, PauseActions_SetCallbacks_m528C82A200766B6A393648409FB0273875F457E8_AdjustorThunk },
	{ 0x060000EC, U3CSetNextTargetU3Ed__8_MoveNext_mD2B11DFB0DFD8F0E370C1E51FDBBF1589FB280C6_AdjustorThunk },
	{ 0x060000ED, U3CSetNextTargetU3Ed__8_SetStateMachine_m1040D950004C15E24BFBC9391631E00CA2D2C134_AdjustorThunk },
};
static const int32_t s_InvokerIndices[243] = 
{
	14,
	23,
	23,
	1705,
	1706,
	1707,
	1708,
	1709,
	9,
	14,
	14,
	23,
	23,
	2552,
	2553,
	2554,
	1755,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	9,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	14,
	114,
	31,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	1866,
	1866,
	114,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	1866,
	1866,
	1866,
	1866,
	1866,
	23,
	23,
	23,
	23,
	1866,
	1866,
	26,
	23,
	23,
	23,
	23,
	1866,
	114,
	114,
	23,
	23,
	114,
	23,
	23,
	23,
	23,
	23,
	114,
	23,
	23,
	14,
	23,
	23,
	23,
	1866,
	31,
	23,
	23,
	23,
	114,
	23,
	23,
	23,
	1866,
	114,
	1866,
	114,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	404,
	1866,
	31,
	23,
	23,
	23,
	114,
	28,
	23,
	26,
	23,
	34,
	26,
	34,
	32,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	3,
	3,
	3,
	23,
	26,
	23,
	-1,
	-1,
	-1,
	3,
	3,
	23,
	3,
	23,
	404,
	23,
	23,
	23,
	23,
	797,
	23,
	32,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	23,
	26,
	160,
	1108,
	23,
	-1,
	-1,
	49,
	3,
	26,
	14,
	14,
	14,
	14,
	14,
	23,
	23,
	114,
	2555,
	26,
	26,
	14,
	14,
	23,
	23,
	114,
	2556,
	26,
	26,
	14,
	14,
	23,
	23,
	114,
	2557,
	26,
	1866,
	1866,
	1866,
	1866,
	1866,
	1866,
	23,
	26,
	32,
	23,
	114,
	14,
	23,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[6] = 
{
	{ 0x02000025, { 4, 1 } },
	{ 0x060000A8, { 0, 1 } },
	{ 0x060000A9, { 1, 1 } },
	{ 0x060000AA, { 2, 2 } },
	{ 0x060000C7, { 5, 1 } },
	{ 0x060000C8, { 6, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[7] = 
{
	{ (Il2CppRGCTXDataType)1, 25032 },
	{ (Il2CppRGCTXDataType)1, 25421 },
	{ (Il2CppRGCTXDataType)1, 25033 },
	{ (Il2CppRGCTXDataType)2, 25033 },
	{ (Il2CppRGCTXDataType)2, 25422 },
	{ (Il2CppRGCTXDataType)2, 25059 },
	{ (Il2CppRGCTXDataType)3, 23465 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	243,
	s_methodPointers,
	26,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	6,
	s_rgctxIndices,
	7,
	s_rgctxValues,
	NULL,
};
