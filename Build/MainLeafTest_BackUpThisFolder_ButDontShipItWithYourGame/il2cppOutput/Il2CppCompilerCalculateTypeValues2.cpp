﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Cinemachine.CinemachineBrain/VcamActivatedEvent
struct VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B;
// Cinemachine.CinemachineCore/Stage[]
struct StageU5BU5D_tC107E846F8ED5DBF4CEF77B4AFCC26D9DBE723A2;
// Cinemachine.CinemachineImpulseDefinition
struct CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF;
// Cinemachine.CinemachineTriggerAction/ActionSettings/TriggerEvent
struct TriggerEvent_t4D34422A1066EEC819DFFEA8DD88FA76A44A9CA7;
// Cinemachine.CinemachineVirtualCameraBase
struct CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD;
// Cinemachine.ISignalSource6D
struct ISignalSource6D_t25FC13DF22002AA6533EA3CC5AAF841610674CBA;
// Cinemachine.NoiseSettings/TransformNoiseParams[]
struct TransformNoiseParamsU5BU5D_tA03937735BC5D12BD3AF6572C92C7AE69F011E9A;
// Cinemachine.SignalSourceAsset
struct SignalSourceAsset_t543AC1ED7A03A17D509FDBD53782B72CC58F1140;
// Cinemachine.Utility.CinemachineDebug/OnGUIDelegate
struct OnGUIDelegate_tF9317001AB4E703C8439EFD7987552E90C476F72;
// Cinemachine.Utility.GaussianWindow1D_Vector3
struct GaussianWindow1D_Vector3_t3D2CA98CAA335A6CA30F2F3E51CEC6FA69C1EC48;
// Cinemachine.Utility.HeadingTracker/Item[]
struct ItemU5BU5D_t7D008EEEF404065A770D7C42085C86F254078B46;
// CollectableConfiguration
struct CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210;
// CollectableConfiguration[]
struct CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A;
// CollectableData
struct CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7;
// EndGameScreen
struct EndGameScreen_tA1957EC7E20256B5515F2ABF8D752AD582C297FF;
// EnemyBehaviour
struct EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552;
// GameManager
struct GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89;
// Grabbable
struct Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3;
// IPlayerComponent[]
struct IPlayerComponentU5BU5D_t17127376879D8403CFB92FCDF78A0AA888B39222;
// Informations
struct Informations_tADA074FFFA53270CEFD43C54FEE73B957720E1C2;
// InputSettings
struct InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE;
// InputSettings/IInteractionActions
struct IInteractionActions_t7B47ABFE586B9717513257116DE204844B92769C;
// InputSettings/IMovimentActions
struct IMovimentActions_t0358792E255C5175235EB33094386AD100846E88;
// InputSettings/IPauseActions
struct IPauseActions_tE1311BD44A22C707BC351D42084CCDD6CE4F03CD;
// Player
struct Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873;
// PlayerInput
struct PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB;
// PlayerInteraction
struct PlayerInteraction_t7BE361C67931CA1C21B195DD20AEBAC3EE777A16;
// PoolSystem
struct PoolSystem_t1805A8BEF518DCC1DA9D16E7932112C3692507A4;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>
struct Action_1_tFCECCF366288B4AB33F727560383950494F520DE;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Cinemachine.ICinemachineCamera,System.Object>
struct Dictionary_2_t2DA729FA5DE6343A62FDA39E2B5A6EA153BB03A8;
// System.Collections.Generic.Dictionary`2<CollectableType,System.Collections.Generic.Queue`1<Collectable>>
struct Dictionary_2_t5D531F2F867CC41CF8C228C66EDA4313D16C738A;
// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
struct Dictionary_2_t17BAC429FFB732E40F76FF33EF5FB320E0DAB8AB;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean>
struct Dictionary_2_tDB6E99559F505A01720155F4596CB8DC52E0E786;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera>
struct Dictionary_2_tA412557CE01C74AD294901BEFE89672A83C176C7;
// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,Cinemachine.UpdateTracker/UpdateStatus>
struct Dictionary_2_t206ABE2C488C2CFD8F3B87CAA95C0BF491D6D82F;
// System.Collections.Generic.HashSet`1<UnityEngine.GameObject>
struct HashSet_1_tE5720030A8BA061DA65ABBF4C5150D8D84BD3169;
// System.Collections.Generic.HashSet`1<UnityEngine.Object>
struct HashSet_1_tADB7C44C9B6A85B59697F67CB1D4281425E2EB15;
// System.Collections.Generic.List`1<Cinemachine.CinemachineExtension>
struct List_1_tB06968E265CE1689803547CD89D453ACFB3509D5;
// System.Collections.Generic.List`1<Cinemachine.CinemachineImpulseManager/ImpulseEvent>
struct List_1_t349AA6402EA18E7FC0C3088824AA2AE1F37149C5;
// System.Collections.Generic.List`1<System.Text.StringBuilder>
struct List_1_t0FD2D255A85E18EA46EBFF5ACE19F56970882812;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t4DBFD85DCFB946888856DBE52AC08C2AF69C4DBE;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_tEFDFBE18E061A6065AB2FF735F1425FB59F919BC;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7;
// System.Threading.Tasks.Task
struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.InputSystem.InputAction
struct InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23;
// UnityEngine.InputSystem.InputActionAsset
struct InputActionAsset_tF7B0F55148C83D9F1A6427BF0159DB809816E3F8;
// UnityEngine.InputSystem.InputActionMap
struct InputActionMap_tB7AD91E2F755C55799B0249B601BE7113FB44CA3;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t26EB10EEE89DD3EF913D52E8797FAB841F6F2AA3;
// UnityEngine.RenderTexture
struct RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Transform[]
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityStandardAssets.Water.PlanarReflection
struct PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B;
// UnityStandardAssets.Water.WaterBase
struct WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565;

struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_com;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};


// <Module>
struct U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4 
{
public:

public:
};


// <Module>
struct U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC 
{
public:

public:
};


// System.Object


// Cinemachine.CinemachineImpulseManager
struct CinemachineImpulseManager_tC1F7AA07F19AE36A04DB0AD50E19B7E256BEEF67  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Cinemachine.CinemachineImpulseManager/ImpulseEvent> Cinemachine.CinemachineImpulseManager::m_ExpiredEvents
	List_1_t349AA6402EA18E7FC0C3088824AA2AE1F37149C5 * ___m_ExpiredEvents_2;
	// System.Collections.Generic.List`1<Cinemachine.CinemachineImpulseManager/ImpulseEvent> Cinemachine.CinemachineImpulseManager::m_ActiveEvents
	List_1_t349AA6402EA18E7FC0C3088824AA2AE1F37149C5 * ___m_ActiveEvents_3;

public:
	inline static int32_t get_offset_of_m_ExpiredEvents_2() { return static_cast<int32_t>(offsetof(CinemachineImpulseManager_tC1F7AA07F19AE36A04DB0AD50E19B7E256BEEF67, ___m_ExpiredEvents_2)); }
	inline List_1_t349AA6402EA18E7FC0C3088824AA2AE1F37149C5 * get_m_ExpiredEvents_2() const { return ___m_ExpiredEvents_2; }
	inline List_1_t349AA6402EA18E7FC0C3088824AA2AE1F37149C5 ** get_address_of_m_ExpiredEvents_2() { return &___m_ExpiredEvents_2; }
	inline void set_m_ExpiredEvents_2(List_1_t349AA6402EA18E7FC0C3088824AA2AE1F37149C5 * value)
	{
		___m_ExpiredEvents_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ExpiredEvents_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActiveEvents_3() { return static_cast<int32_t>(offsetof(CinemachineImpulseManager_tC1F7AA07F19AE36A04DB0AD50E19B7E256BEEF67, ___m_ActiveEvents_3)); }
	inline List_1_t349AA6402EA18E7FC0C3088824AA2AE1F37149C5 * get_m_ActiveEvents_3() const { return ___m_ActiveEvents_3; }
	inline List_1_t349AA6402EA18E7FC0C3088824AA2AE1F37149C5 ** get_address_of_m_ActiveEvents_3() { return &___m_ActiveEvents_3; }
	inline void set_m_ActiveEvents_3(List_1_t349AA6402EA18E7FC0C3088824AA2AE1F37149C5 * value)
	{
		___m_ActiveEvents_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActiveEvents_3), (void*)value);
	}
};

struct CinemachineImpulseManager_tC1F7AA07F19AE36A04DB0AD50E19B7E256BEEF67_StaticFields
{
public:
	// Cinemachine.CinemachineImpulseManager Cinemachine.CinemachineImpulseManager::sInstance
	CinemachineImpulseManager_tC1F7AA07F19AE36A04DB0AD50E19B7E256BEEF67 * ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(CinemachineImpulseManager_tC1F7AA07F19AE36A04DB0AD50E19B7E256BEEF67_StaticFields, ___sInstance_0)); }
	inline CinemachineImpulseManager_tC1F7AA07F19AE36A04DB0AD50E19B7E256BEEF67 * get_sInstance_0() const { return ___sInstance_0; }
	inline CinemachineImpulseManager_tC1F7AA07F19AE36A04DB0AD50E19B7E256BEEF67 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(CinemachineImpulseManager_tC1F7AA07F19AE36A04DB0AD50E19B7E256BEEF67 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sInstance_0), (void*)value);
	}
};


// Cinemachine.RuntimeUtility
struct RuntimeUtility_t3A27FF884D04BFB4D5001F66D89928026AFED18F  : public RuntimeObject
{
public:

public:
};


// Cinemachine.UpdateTracker
struct UpdateTracker_tD9C9B2CB1F30AB0EAF2E743C107291FBBDACE8FE  : public RuntimeObject
{
public:

public:
};

struct UpdateTracker_tD9C9B2CB1F30AB0EAF2E743C107291FBBDACE8FE_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,Cinemachine.UpdateTracker/UpdateStatus> Cinemachine.UpdateTracker::mUpdateStatus
	Dictionary_2_t206ABE2C488C2CFD8F3B87CAA95C0BF491D6D82F * ___mUpdateStatus_0;
	// System.Collections.Generic.List`1<UnityEngine.Transform> Cinemachine.UpdateTracker::sToDelete
	List_1_t4DBFD85DCFB946888856DBE52AC08C2AF69C4DBE * ___sToDelete_1;
	// System.Single Cinemachine.UpdateTracker::mLastUpdateTime
	float ___mLastUpdateTime_2;

public:
	inline static int32_t get_offset_of_mUpdateStatus_0() { return static_cast<int32_t>(offsetof(UpdateTracker_tD9C9B2CB1F30AB0EAF2E743C107291FBBDACE8FE_StaticFields, ___mUpdateStatus_0)); }
	inline Dictionary_2_t206ABE2C488C2CFD8F3B87CAA95C0BF491D6D82F * get_mUpdateStatus_0() const { return ___mUpdateStatus_0; }
	inline Dictionary_2_t206ABE2C488C2CFD8F3B87CAA95C0BF491D6D82F ** get_address_of_mUpdateStatus_0() { return &___mUpdateStatus_0; }
	inline void set_mUpdateStatus_0(Dictionary_2_t206ABE2C488C2CFD8F3B87CAA95C0BF491D6D82F * value)
	{
		___mUpdateStatus_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mUpdateStatus_0), (void*)value);
	}

	inline static int32_t get_offset_of_sToDelete_1() { return static_cast<int32_t>(offsetof(UpdateTracker_tD9C9B2CB1F30AB0EAF2E743C107291FBBDACE8FE_StaticFields, ___sToDelete_1)); }
	inline List_1_t4DBFD85DCFB946888856DBE52AC08C2AF69C4DBE * get_sToDelete_1() const { return ___sToDelete_1; }
	inline List_1_t4DBFD85DCFB946888856DBE52AC08C2AF69C4DBE ** get_address_of_sToDelete_1() { return &___sToDelete_1; }
	inline void set_sToDelete_1(List_1_t4DBFD85DCFB946888856DBE52AC08C2AF69C4DBE * value)
	{
		___sToDelete_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sToDelete_1), (void*)value);
	}

	inline static int32_t get_offset_of_mLastUpdateTime_2() { return static_cast<int32_t>(offsetof(UpdateTracker_tD9C9B2CB1F30AB0EAF2E743C107291FBBDACE8FE_StaticFields, ___mLastUpdateTime_2)); }
	inline float get_mLastUpdateTime_2() const { return ___mLastUpdateTime_2; }
	inline float* get_address_of_mLastUpdateTime_2() { return &___mLastUpdateTime_2; }
	inline void set_mLastUpdateTime_2(float value)
	{
		___mLastUpdateTime_2 = value;
	}
};


// Cinemachine.Utility.CinemachineDebug
struct CinemachineDebug_t0D9E17BEA16D2F423F05274824D6BBB6A2B4993E  : public RuntimeObject
{
public:

public:
};

struct CinemachineDebug_t0D9E17BEA16D2F423F05274824D6BBB6A2B4993E_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<UnityEngine.Object> Cinemachine.Utility.CinemachineDebug::mClients
	HashSet_1_tADB7C44C9B6A85B59697F67CB1D4281425E2EB15 * ___mClients_0;
	// Cinemachine.Utility.CinemachineDebug/OnGUIDelegate Cinemachine.Utility.CinemachineDebug::OnGUIHandlers
	OnGUIDelegate_tF9317001AB4E703C8439EFD7987552E90C476F72 * ___OnGUIHandlers_1;
	// System.Collections.Generic.List`1<System.Text.StringBuilder> Cinemachine.Utility.CinemachineDebug::mAvailableStringBuilders
	List_1_t0FD2D255A85E18EA46EBFF5ACE19F56970882812 * ___mAvailableStringBuilders_2;

public:
	inline static int32_t get_offset_of_mClients_0() { return static_cast<int32_t>(offsetof(CinemachineDebug_t0D9E17BEA16D2F423F05274824D6BBB6A2B4993E_StaticFields, ___mClients_0)); }
	inline HashSet_1_tADB7C44C9B6A85B59697F67CB1D4281425E2EB15 * get_mClients_0() const { return ___mClients_0; }
	inline HashSet_1_tADB7C44C9B6A85B59697F67CB1D4281425E2EB15 ** get_address_of_mClients_0() { return &___mClients_0; }
	inline void set_mClients_0(HashSet_1_tADB7C44C9B6A85B59697F67CB1D4281425E2EB15 * value)
	{
		___mClients_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mClients_0), (void*)value);
	}

	inline static int32_t get_offset_of_OnGUIHandlers_1() { return static_cast<int32_t>(offsetof(CinemachineDebug_t0D9E17BEA16D2F423F05274824D6BBB6A2B4993E_StaticFields, ___OnGUIHandlers_1)); }
	inline OnGUIDelegate_tF9317001AB4E703C8439EFD7987552E90C476F72 * get_OnGUIHandlers_1() const { return ___OnGUIHandlers_1; }
	inline OnGUIDelegate_tF9317001AB4E703C8439EFD7987552E90C476F72 ** get_address_of_OnGUIHandlers_1() { return &___OnGUIHandlers_1; }
	inline void set_OnGUIHandlers_1(OnGUIDelegate_tF9317001AB4E703C8439EFD7987552E90C476F72 * value)
	{
		___OnGUIHandlers_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnGUIHandlers_1), (void*)value);
	}

	inline static int32_t get_offset_of_mAvailableStringBuilders_2() { return static_cast<int32_t>(offsetof(CinemachineDebug_t0D9E17BEA16D2F423F05274824D6BBB6A2B4993E_StaticFields, ___mAvailableStringBuilders_2)); }
	inline List_1_t0FD2D255A85E18EA46EBFF5ACE19F56970882812 * get_mAvailableStringBuilders_2() const { return ___mAvailableStringBuilders_2; }
	inline List_1_t0FD2D255A85E18EA46EBFF5ACE19F56970882812 ** get_address_of_mAvailableStringBuilders_2() { return &___mAvailableStringBuilders_2; }
	inline void set_mAvailableStringBuilders_2(List_1_t0FD2D255A85E18EA46EBFF5ACE19F56970882812 * value)
	{
		___mAvailableStringBuilders_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mAvailableStringBuilders_2), (void*)value);
	}
};


// Cinemachine.Utility.Damper
struct Damper_t9BB3B28320C2E100B659D1237F6064551C846A91  : public RuntimeObject
{
public:

public:
};


// Cinemachine.Utility.GaussianWindow1d`1<UnityEngine.Quaternion>
struct GaussianWindow1d_1_tD306E7D9C22D8A3423192901A6231996D605EAFC  : public RuntimeObject
{
public:
	// T[] Cinemachine.Utility.GaussianWindow1d`1::mData
	QuaternionU5BU5D_t26EB10EEE89DD3EF913D52E8797FAB841F6F2AA3* ___mData_0;
	// System.Single[] Cinemachine.Utility.GaussianWindow1d`1::mKernel
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___mKernel_1;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::mKernelSum
	float ___mKernelSum_2;
	// System.Int32 Cinemachine.Utility.GaussianWindow1d`1::mCurrentPos
	int32_t ___mCurrentPos_3;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::<Sigma>k__BackingField
	float ___U3CSigmaU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_mData_0() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_tD306E7D9C22D8A3423192901A6231996D605EAFC, ___mData_0)); }
	inline QuaternionU5BU5D_t26EB10EEE89DD3EF913D52E8797FAB841F6F2AA3* get_mData_0() const { return ___mData_0; }
	inline QuaternionU5BU5D_t26EB10EEE89DD3EF913D52E8797FAB841F6F2AA3** get_address_of_mData_0() { return &___mData_0; }
	inline void set_mData_0(QuaternionU5BU5D_t26EB10EEE89DD3EF913D52E8797FAB841F6F2AA3* value)
	{
		___mData_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mData_0), (void*)value);
	}

	inline static int32_t get_offset_of_mKernel_1() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_tD306E7D9C22D8A3423192901A6231996D605EAFC, ___mKernel_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_mKernel_1() const { return ___mKernel_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_mKernel_1() { return &___mKernel_1; }
	inline void set_mKernel_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___mKernel_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mKernel_1), (void*)value);
	}

	inline static int32_t get_offset_of_mKernelSum_2() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_tD306E7D9C22D8A3423192901A6231996D605EAFC, ___mKernelSum_2)); }
	inline float get_mKernelSum_2() const { return ___mKernelSum_2; }
	inline float* get_address_of_mKernelSum_2() { return &___mKernelSum_2; }
	inline void set_mKernelSum_2(float value)
	{
		___mKernelSum_2 = value;
	}

	inline static int32_t get_offset_of_mCurrentPos_3() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_tD306E7D9C22D8A3423192901A6231996D605EAFC, ___mCurrentPos_3)); }
	inline int32_t get_mCurrentPos_3() const { return ___mCurrentPos_3; }
	inline int32_t* get_address_of_mCurrentPos_3() { return &___mCurrentPos_3; }
	inline void set_mCurrentPos_3(int32_t value)
	{
		___mCurrentPos_3 = value;
	}

	inline static int32_t get_offset_of_U3CSigmaU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_tD306E7D9C22D8A3423192901A6231996D605EAFC, ___U3CSigmaU3Ek__BackingField_4)); }
	inline float get_U3CSigmaU3Ek__BackingField_4() const { return ___U3CSigmaU3Ek__BackingField_4; }
	inline float* get_address_of_U3CSigmaU3Ek__BackingField_4() { return &___U3CSigmaU3Ek__BackingField_4; }
	inline void set_U3CSigmaU3Ek__BackingField_4(float value)
	{
		___U3CSigmaU3Ek__BackingField_4 = value;
	}
};


// Cinemachine.Utility.GaussianWindow1d`1<UnityEngine.Vector2>
struct GaussianWindow1d_1_t10F20E064ADE0A33D663FD5C4A4BA8B9BDC981AD  : public RuntimeObject
{
public:
	// T[] Cinemachine.Utility.GaussianWindow1d`1::mData
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___mData_0;
	// System.Single[] Cinemachine.Utility.GaussianWindow1d`1::mKernel
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___mKernel_1;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::mKernelSum
	float ___mKernelSum_2;
	// System.Int32 Cinemachine.Utility.GaussianWindow1d`1::mCurrentPos
	int32_t ___mCurrentPos_3;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::<Sigma>k__BackingField
	float ___U3CSigmaU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_mData_0() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t10F20E064ADE0A33D663FD5C4A4BA8B9BDC981AD, ___mData_0)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_mData_0() const { return ___mData_0; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_mData_0() { return &___mData_0; }
	inline void set_mData_0(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___mData_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mData_0), (void*)value);
	}

	inline static int32_t get_offset_of_mKernel_1() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t10F20E064ADE0A33D663FD5C4A4BA8B9BDC981AD, ___mKernel_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_mKernel_1() const { return ___mKernel_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_mKernel_1() { return &___mKernel_1; }
	inline void set_mKernel_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___mKernel_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mKernel_1), (void*)value);
	}

	inline static int32_t get_offset_of_mKernelSum_2() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t10F20E064ADE0A33D663FD5C4A4BA8B9BDC981AD, ___mKernelSum_2)); }
	inline float get_mKernelSum_2() const { return ___mKernelSum_2; }
	inline float* get_address_of_mKernelSum_2() { return &___mKernelSum_2; }
	inline void set_mKernelSum_2(float value)
	{
		___mKernelSum_2 = value;
	}

	inline static int32_t get_offset_of_mCurrentPos_3() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t10F20E064ADE0A33D663FD5C4A4BA8B9BDC981AD, ___mCurrentPos_3)); }
	inline int32_t get_mCurrentPos_3() const { return ___mCurrentPos_3; }
	inline int32_t* get_address_of_mCurrentPos_3() { return &___mCurrentPos_3; }
	inline void set_mCurrentPos_3(int32_t value)
	{
		___mCurrentPos_3 = value;
	}

	inline static int32_t get_offset_of_U3CSigmaU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_t10F20E064ADE0A33D663FD5C4A4BA8B9BDC981AD, ___U3CSigmaU3Ek__BackingField_4)); }
	inline float get_U3CSigmaU3Ek__BackingField_4() const { return ___U3CSigmaU3Ek__BackingField_4; }
	inline float* get_address_of_U3CSigmaU3Ek__BackingField_4() { return &___U3CSigmaU3Ek__BackingField_4; }
	inline void set_U3CSigmaU3Ek__BackingField_4(float value)
	{
		___U3CSigmaU3Ek__BackingField_4 = value;
	}
};


// Cinemachine.Utility.GaussianWindow1d`1<UnityEngine.Vector3>
struct GaussianWindow1d_1_tB879E544550B44574C9A5A21528AB3884F3580DD  : public RuntimeObject
{
public:
	// T[] Cinemachine.Utility.GaussianWindow1d`1::mData
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___mData_0;
	// System.Single[] Cinemachine.Utility.GaussianWindow1d`1::mKernel
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___mKernel_1;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::mKernelSum
	float ___mKernelSum_2;
	// System.Int32 Cinemachine.Utility.GaussianWindow1d`1::mCurrentPos
	int32_t ___mCurrentPos_3;
	// System.Single Cinemachine.Utility.GaussianWindow1d`1::<Sigma>k__BackingField
	float ___U3CSigmaU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_mData_0() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_tB879E544550B44574C9A5A21528AB3884F3580DD, ___mData_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_mData_0() const { return ___mData_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_mData_0() { return &___mData_0; }
	inline void set_mData_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___mData_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mData_0), (void*)value);
	}

	inline static int32_t get_offset_of_mKernel_1() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_tB879E544550B44574C9A5A21528AB3884F3580DD, ___mKernel_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_mKernel_1() const { return ___mKernel_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_mKernel_1() { return &___mKernel_1; }
	inline void set_mKernel_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___mKernel_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mKernel_1), (void*)value);
	}

	inline static int32_t get_offset_of_mKernelSum_2() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_tB879E544550B44574C9A5A21528AB3884F3580DD, ___mKernelSum_2)); }
	inline float get_mKernelSum_2() const { return ___mKernelSum_2; }
	inline float* get_address_of_mKernelSum_2() { return &___mKernelSum_2; }
	inline void set_mKernelSum_2(float value)
	{
		___mKernelSum_2 = value;
	}

	inline static int32_t get_offset_of_mCurrentPos_3() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_tB879E544550B44574C9A5A21528AB3884F3580DD, ___mCurrentPos_3)); }
	inline int32_t get_mCurrentPos_3() const { return ___mCurrentPos_3; }
	inline int32_t* get_address_of_mCurrentPos_3() { return &___mCurrentPos_3; }
	inline void set_mCurrentPos_3(int32_t value)
	{
		___mCurrentPos_3 = value;
	}

	inline static int32_t get_offset_of_U3CSigmaU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GaussianWindow1d_1_tB879E544550B44574C9A5A21528AB3884F3580DD, ___U3CSigmaU3Ek__BackingField_4)); }
	inline float get_U3CSigmaU3Ek__BackingField_4() const { return ___U3CSigmaU3Ek__BackingField_4; }
	inline float* get_address_of_U3CSigmaU3Ek__BackingField_4() { return &___U3CSigmaU3Ek__BackingField_4; }
	inline void set_U3CSigmaU3Ek__BackingField_4(float value)
	{
		___U3CSigmaU3Ek__BackingField_4 = value;
	}
};


// Cinemachine.Utility.SplineHelpers
struct SplineHelpers_tC88AED5435AA4C67E6039B93997091214A2E0858  : public RuntimeObject
{
public:

public:
};


// Cinemachine.Utility.UnityQuaternionExtensions
struct UnityQuaternionExtensions_tCED4E5F8D5E7C212E7CE30310E4123D534075ABB  : public RuntimeObject
{
public:

public:
};


// Cinemachine.Utility.UnityRectExtensions
struct UnityRectExtensions_tD2D2C115AA54BE180CE78EF2BB37D1EC18A36847  : public RuntimeObject
{
public:

public:
};


// Cinemachine.Utility.UnityVectorExtensions
struct UnityVectorExtensions_t8B7D79BD392965E48AEA9B91EE25FD09244E69B3  : public RuntimeObject
{
public:

public:
};


// GameManager/<LoadScene>d__16
struct U3CLoadSceneU3Ed__16_t72200372913778B5D70C4802329F3D4396CC3A32  : public RuntimeObject
{
public:
	// System.Int32 GameManager/<LoadScene>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GameManager/<LoadScene>d__16::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GameManager GameManager/<LoadScene>d__16::<>4__this
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___U3CU3E4__this_2;
	// System.String GameManager/<LoadScene>d__16::nextMap
	String_t* ___nextMap_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__16_t72200372913778B5D70C4802329F3D4396CC3A32, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__16_t72200372913778B5D70C4802329F3D4396CC3A32, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__16_t72200372913778B5D70C4802329F3D4396CC3A32, ___U3CU3E4__this_2)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_nextMap_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneU3Ed__16_t72200372913778B5D70C4802329F3D4396CC3A32, ___nextMap_3)); }
	inline String_t* get_nextMap_3() const { return ___nextMap_3; }
	inline String_t** get_address_of_nextMap_3() { return &___nextMap_3; }
	inline void set_nextMap_3(String_t* value)
	{
		___nextMap_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nextMap_3), (void*)value);
	}
};


// Informations
struct Informations_tADA074FFFA53270CEFD43C54FEE73B957720E1C2  : public RuntimeObject
{
public:
	// System.Int32 Informations::coins
	int32_t ___coins_0;

public:
	inline static int32_t get_offset_of_coins_0() { return static_cast<int32_t>(offsetof(Informations_tADA074FFFA53270CEFD43C54FEE73B957720E1C2, ___coins_0)); }
	inline int32_t get_coins_0() const { return ___coins_0; }
	inline int32_t* get_address_of_coins_0() { return &___coins_0; }
	inline void set_coins_0(int32_t value)
	{
		___coins_0 = value;
	}
};


// InputSettings
struct InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE  : public RuntimeObject
{
public:
	// UnityEngine.InputSystem.InputActionAsset InputSettings::<asset>k__BackingField
	InputActionAsset_tF7B0F55148C83D9F1A6427BF0159DB809816E3F8 * ___U3CassetU3Ek__BackingField_0;
	// UnityEngine.InputSystem.InputActionMap InputSettings::m_Moviment
	InputActionMap_tB7AD91E2F755C55799B0249B601BE7113FB44CA3 * ___m_Moviment_1;
	// InputSettings/IMovimentActions InputSettings::m_MovimentActionsCallbackInterface
	RuntimeObject* ___m_MovimentActionsCallbackInterface_2;
	// UnityEngine.InputSystem.InputAction InputSettings::m_Moviment_HorizontalMoviment
	InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * ___m_Moviment_HorizontalMoviment_3;
	// UnityEngine.InputSystem.InputAction InputSettings::m_Moviment_Vertical
	InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * ___m_Moviment_Vertical_4;
	// UnityEngine.InputSystem.InputAction InputSettings::m_Moviment_Jump
	InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * ___m_Moviment_Jump_5;
	// UnityEngine.InputSystem.InputAction InputSettings::m_Moviment_Crounch
	InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * ___m_Moviment_Crounch_6;
	// UnityEngine.InputSystem.InputActionMap InputSettings::m_Interaction
	InputActionMap_tB7AD91E2F755C55799B0249B601BE7113FB44CA3 * ___m_Interaction_7;
	// InputSettings/IInteractionActions InputSettings::m_InteractionActionsCallbackInterface
	RuntimeObject* ___m_InteractionActionsCallbackInterface_8;
	// UnityEngine.InputSystem.InputAction InputSettings::m_Interaction_BasicInteraction
	InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * ___m_Interaction_BasicInteraction_9;
	// UnityEngine.InputSystem.InputActionMap InputSettings::m_Pause
	InputActionMap_tB7AD91E2F755C55799B0249B601BE7113FB44CA3 * ___m_Pause_10;
	// InputSettings/IPauseActions InputSettings::m_PauseActionsCallbackInterface
	RuntimeObject* ___m_PauseActionsCallbackInterface_11;
	// UnityEngine.InputSystem.InputAction InputSettings::m_Pause_PauseTrigger
	InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * ___m_Pause_PauseTrigger_12;
	// System.Int32 InputSettings::m_PCSchemeIndex
	int32_t ___m_PCSchemeIndex_13;

public:
	inline static int32_t get_offset_of_U3CassetU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE, ___U3CassetU3Ek__BackingField_0)); }
	inline InputActionAsset_tF7B0F55148C83D9F1A6427BF0159DB809816E3F8 * get_U3CassetU3Ek__BackingField_0() const { return ___U3CassetU3Ek__BackingField_0; }
	inline InputActionAsset_tF7B0F55148C83D9F1A6427BF0159DB809816E3F8 ** get_address_of_U3CassetU3Ek__BackingField_0() { return &___U3CassetU3Ek__BackingField_0; }
	inline void set_U3CassetU3Ek__BackingField_0(InputActionAsset_tF7B0F55148C83D9F1A6427BF0159DB809816E3F8 * value)
	{
		___U3CassetU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CassetU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Moviment_1() { return static_cast<int32_t>(offsetof(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE, ___m_Moviment_1)); }
	inline InputActionMap_tB7AD91E2F755C55799B0249B601BE7113FB44CA3 * get_m_Moviment_1() const { return ___m_Moviment_1; }
	inline InputActionMap_tB7AD91E2F755C55799B0249B601BE7113FB44CA3 ** get_address_of_m_Moviment_1() { return &___m_Moviment_1; }
	inline void set_m_Moviment_1(InputActionMap_tB7AD91E2F755C55799B0249B601BE7113FB44CA3 * value)
	{
		___m_Moviment_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Moviment_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_MovimentActionsCallbackInterface_2() { return static_cast<int32_t>(offsetof(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE, ___m_MovimentActionsCallbackInterface_2)); }
	inline RuntimeObject* get_m_MovimentActionsCallbackInterface_2() const { return ___m_MovimentActionsCallbackInterface_2; }
	inline RuntimeObject** get_address_of_m_MovimentActionsCallbackInterface_2() { return &___m_MovimentActionsCallbackInterface_2; }
	inline void set_m_MovimentActionsCallbackInterface_2(RuntimeObject* value)
	{
		___m_MovimentActionsCallbackInterface_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MovimentActionsCallbackInterface_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Moviment_HorizontalMoviment_3() { return static_cast<int32_t>(offsetof(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE, ___m_Moviment_HorizontalMoviment_3)); }
	inline InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * get_m_Moviment_HorizontalMoviment_3() const { return ___m_Moviment_HorizontalMoviment_3; }
	inline InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 ** get_address_of_m_Moviment_HorizontalMoviment_3() { return &___m_Moviment_HorizontalMoviment_3; }
	inline void set_m_Moviment_HorizontalMoviment_3(InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * value)
	{
		___m_Moviment_HorizontalMoviment_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Moviment_HorizontalMoviment_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Moviment_Vertical_4() { return static_cast<int32_t>(offsetof(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE, ___m_Moviment_Vertical_4)); }
	inline InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * get_m_Moviment_Vertical_4() const { return ___m_Moviment_Vertical_4; }
	inline InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 ** get_address_of_m_Moviment_Vertical_4() { return &___m_Moviment_Vertical_4; }
	inline void set_m_Moviment_Vertical_4(InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * value)
	{
		___m_Moviment_Vertical_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Moviment_Vertical_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Moviment_Jump_5() { return static_cast<int32_t>(offsetof(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE, ___m_Moviment_Jump_5)); }
	inline InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * get_m_Moviment_Jump_5() const { return ___m_Moviment_Jump_5; }
	inline InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 ** get_address_of_m_Moviment_Jump_5() { return &___m_Moviment_Jump_5; }
	inline void set_m_Moviment_Jump_5(InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * value)
	{
		___m_Moviment_Jump_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Moviment_Jump_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Moviment_Crounch_6() { return static_cast<int32_t>(offsetof(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE, ___m_Moviment_Crounch_6)); }
	inline InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * get_m_Moviment_Crounch_6() const { return ___m_Moviment_Crounch_6; }
	inline InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 ** get_address_of_m_Moviment_Crounch_6() { return &___m_Moviment_Crounch_6; }
	inline void set_m_Moviment_Crounch_6(InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * value)
	{
		___m_Moviment_Crounch_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Moviment_Crounch_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interaction_7() { return static_cast<int32_t>(offsetof(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE, ___m_Interaction_7)); }
	inline InputActionMap_tB7AD91E2F755C55799B0249B601BE7113FB44CA3 * get_m_Interaction_7() const { return ___m_Interaction_7; }
	inline InputActionMap_tB7AD91E2F755C55799B0249B601BE7113FB44CA3 ** get_address_of_m_Interaction_7() { return &___m_Interaction_7; }
	inline void set_m_Interaction_7(InputActionMap_tB7AD91E2F755C55799B0249B601BE7113FB44CA3 * value)
	{
		___m_Interaction_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Interaction_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_InteractionActionsCallbackInterface_8() { return static_cast<int32_t>(offsetof(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE, ___m_InteractionActionsCallbackInterface_8)); }
	inline RuntimeObject* get_m_InteractionActionsCallbackInterface_8() const { return ___m_InteractionActionsCallbackInterface_8; }
	inline RuntimeObject** get_address_of_m_InteractionActionsCallbackInterface_8() { return &___m_InteractionActionsCallbackInterface_8; }
	inline void set_m_InteractionActionsCallbackInterface_8(RuntimeObject* value)
	{
		___m_InteractionActionsCallbackInterface_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InteractionActionsCallbackInterface_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interaction_BasicInteraction_9() { return static_cast<int32_t>(offsetof(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE, ___m_Interaction_BasicInteraction_9)); }
	inline InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * get_m_Interaction_BasicInteraction_9() const { return ___m_Interaction_BasicInteraction_9; }
	inline InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 ** get_address_of_m_Interaction_BasicInteraction_9() { return &___m_Interaction_BasicInteraction_9; }
	inline void set_m_Interaction_BasicInteraction_9(InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * value)
	{
		___m_Interaction_BasicInteraction_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Interaction_BasicInteraction_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_Pause_10() { return static_cast<int32_t>(offsetof(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE, ___m_Pause_10)); }
	inline InputActionMap_tB7AD91E2F755C55799B0249B601BE7113FB44CA3 * get_m_Pause_10() const { return ___m_Pause_10; }
	inline InputActionMap_tB7AD91E2F755C55799B0249B601BE7113FB44CA3 ** get_address_of_m_Pause_10() { return &___m_Pause_10; }
	inline void set_m_Pause_10(InputActionMap_tB7AD91E2F755C55799B0249B601BE7113FB44CA3 * value)
	{
		___m_Pause_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Pause_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_PauseActionsCallbackInterface_11() { return static_cast<int32_t>(offsetof(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE, ___m_PauseActionsCallbackInterface_11)); }
	inline RuntimeObject* get_m_PauseActionsCallbackInterface_11() const { return ___m_PauseActionsCallbackInterface_11; }
	inline RuntimeObject** get_address_of_m_PauseActionsCallbackInterface_11() { return &___m_PauseActionsCallbackInterface_11; }
	inline void set_m_PauseActionsCallbackInterface_11(RuntimeObject* value)
	{
		___m_PauseActionsCallbackInterface_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PauseActionsCallbackInterface_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Pause_PauseTrigger_12() { return static_cast<int32_t>(offsetof(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE, ___m_Pause_PauseTrigger_12)); }
	inline InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * get_m_Pause_PauseTrigger_12() const { return ___m_Pause_PauseTrigger_12; }
	inline InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 ** get_address_of_m_Pause_PauseTrigger_12() { return &___m_Pause_PauseTrigger_12; }
	inline void set_m_Pause_PauseTrigger_12(InputAction_t41650906F73B93B30F7721E1DF695ADDD9466D23 * value)
	{
		___m_Pause_PauseTrigger_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Pause_PauseTrigger_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_PCSchemeIndex_13() { return static_cast<int32_t>(offsetof(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE, ___m_PCSchemeIndex_13)); }
	inline int32_t get_m_PCSchemeIndex_13() const { return ___m_PCSchemeIndex_13; }
	inline int32_t* get_address_of_m_PCSchemeIndex_13() { return &___m_PCSchemeIndex_13; }
	inline void set_m_PCSchemeIndex_13(int32_t value)
	{
		___m_PCSchemeIndex_13 = value;
	}
};


// MainLeaf.SaveSystem.SaveSystem
struct SaveSystem_t3C4D994BCE04F61BE8CAD1966E2A334BBCF5DFCF  : public RuntimeObject
{
public:

public:
};


// PlayerInformations
struct PlayerInformations_t0E38B6119ADAB2C0A72D9F803B7555346F386895  : public RuntimeObject
{
public:

public:
};

struct PlayerInformations_t0E38B6119ADAB2C0A72D9F803B7555346F386895_StaticFields
{
public:
	// Informations PlayerInformations::informations
	Informations_tADA074FFFA53270CEFD43C54FEE73B957720E1C2 * ___informations_0;
	// System.Boolean PlayerInformations::completedFirstPuzzle
	bool ___completedFirstPuzzle_1;

public:
	inline static int32_t get_offset_of_informations_0() { return static_cast<int32_t>(offsetof(PlayerInformations_t0E38B6119ADAB2C0A72D9F803B7555346F386895_StaticFields, ___informations_0)); }
	inline Informations_tADA074FFFA53270CEFD43C54FEE73B957720E1C2 * get_informations_0() const { return ___informations_0; }
	inline Informations_tADA074FFFA53270CEFD43C54FEE73B957720E1C2 ** get_address_of_informations_0() { return &___informations_0; }
	inline void set_informations_0(Informations_tADA074FFFA53270CEFD43C54FEE73B957720E1C2 * value)
	{
		___informations_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___informations_0), (void*)value);
	}

	inline static int32_t get_offset_of_completedFirstPuzzle_1() { return static_cast<int32_t>(offsetof(PlayerInformations_t0E38B6119ADAB2C0A72D9F803B7555346F386895_StaticFields, ___completedFirstPuzzle_1)); }
	inline bool get_completedFirstPuzzle_1() const { return ___completedFirstPuzzle_1; }
	inline bool* get_address_of_completedFirstPuzzle_1() { return &___completedFirstPuzzle_1; }
	inline void set_completedFirstPuzzle_1(bool value)
	{
		___completedFirstPuzzle_1 = value;
	}
};


// PoolSystem
struct PoolSystem_t1805A8BEF518DCC1DA9D16E7932112C3692507A4  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<CollectableType,System.Collections.Generic.Queue`1<Collectable>> PoolSystem::collectablePoolList
	Dictionary_2_t5D531F2F867CC41CF8C228C66EDA4313D16C738A * ___collectablePoolList_0;

public:
	inline static int32_t get_offset_of_collectablePoolList_0() { return static_cast<int32_t>(offsetof(PoolSystem_t1805A8BEF518DCC1DA9D16E7932112C3692507A4, ___collectablePoolList_0)); }
	inline Dictionary_2_t5D531F2F867CC41CF8C228C66EDA4313D16C738A * get_collectablePoolList_0() const { return ___collectablePoolList_0; }
	inline Dictionary_2_t5D531F2F867CC41CF8C228C66EDA4313D16C738A ** get_address_of_collectablePoolList_0() { return &___collectablePoolList_0; }
	inline void set_collectablePoolList_0(Dictionary_2_t5D531F2F867CC41CF8C228C66EDA4313D16C738A * value)
	{
		___collectablePoolList_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collectablePoolList_0), (void*)value);
	}
};


// ServiceLocator
struct ServiceLocator_t40441F4445217D8FAEE52BA1EA2E598D93FE8E5A  : public RuntimeObject
{
public:

public:
};

struct ServiceLocator_t40441F4445217D8FAEE52BA1EA2E598D93FE8E5A_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Object> ServiceLocator::Services
	Dictionary_2_t17BAC429FFB732E40F76FF33EF5FB320E0DAB8AB * ___Services_0;

public:
	inline static int32_t get_offset_of_Services_0() { return static_cast<int32_t>(offsetof(ServiceLocator_t40441F4445217D8FAEE52BA1EA2E598D93FE8E5A_StaticFields, ___Services_0)); }
	inline Dictionary_2_t17BAC429FFB732E40F76FF33EF5FB320E0DAB8AB * get_Services_0() const { return ___Services_0; }
	inline Dictionary_2_t17BAC429FFB732E40F76FF33EF5FB320E0DAB8AB ** get_address_of_Services_0() { return &___Services_0; }
	inline void set_Services_0(Dictionary_2_t17BAC429FFB732E40F76FF33EF5FB320E0DAB8AB * value)
	{
		___Services_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Services_0), (void*)value);
	}
};


// System.Attribute
struct Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// UnityStandardAssets.Water.MeshContainer
struct MeshContainer_tAFAB4888889EABF26999DFC66A4122B9C5A084EE  : public RuntimeObject
{
public:
	// UnityEngine.Mesh UnityStandardAssets.Water.MeshContainer::mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh_0;
	// UnityEngine.Vector3[] UnityStandardAssets.Water.MeshContainer::vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___vertices_1;
	// UnityEngine.Vector3[] UnityStandardAssets.Water.MeshContainer::normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___normals_2;

public:
	inline static int32_t get_offset_of_mesh_0() { return static_cast<int32_t>(offsetof(MeshContainer_tAFAB4888889EABF26999DFC66A4122B9C5A084EE, ___mesh_0)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_mesh_0() const { return ___mesh_0; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_mesh_0() { return &___mesh_0; }
	inline void set_mesh_0(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___mesh_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mesh_0), (void*)value);
	}

	inline static int32_t get_offset_of_vertices_1() { return static_cast<int32_t>(offsetof(MeshContainer_tAFAB4888889EABF26999DFC66A4122B9C5A084EE, ___vertices_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_vertices_1() const { return ___vertices_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_vertices_1() { return &___vertices_1; }
	inline void set_vertices_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___vertices_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vertices_1), (void*)value);
	}

	inline static int32_t get_offset_of_normals_2() { return static_cast<int32_t>(offsetof(MeshContainer_tAFAB4888889EABF26999DFC66A4122B9C5A084EE, ___normals_2)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_normals_2() const { return ___normals_2; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_normals_2() { return &___normals_2; }
	inline void set_normals_2(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___normals_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___normals_2), (void*)value);
	}
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct __StaticArrayInitTypeSizeU3D12_t424CC67AAA71D9952D514D413907E974A287EC65 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t424CC67AAA71D9952D514D413907E974A287EC65__padding[12];
	};

public:
};


// Cinemachine.NoiseSettings/NoiseParams
struct NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229 
{
public:
	// System.Single Cinemachine.NoiseSettings/NoiseParams::Frequency
	float ___Frequency_0;
	// System.Single Cinemachine.NoiseSettings/NoiseParams::Amplitude
	float ___Amplitude_1;
	// System.Boolean Cinemachine.NoiseSettings/NoiseParams::Constant
	bool ___Constant_2;

public:
	inline static int32_t get_offset_of_Frequency_0() { return static_cast<int32_t>(offsetof(NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229, ___Frequency_0)); }
	inline float get_Frequency_0() const { return ___Frequency_0; }
	inline float* get_address_of_Frequency_0() { return &___Frequency_0; }
	inline void set_Frequency_0(float value)
	{
		___Frequency_0 = value;
	}

	inline static int32_t get_offset_of_Amplitude_1() { return static_cast<int32_t>(offsetof(NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229, ___Amplitude_1)); }
	inline float get_Amplitude_1() const { return ___Amplitude_1; }
	inline float* get_address_of_Amplitude_1() { return &___Amplitude_1; }
	inline void set_Amplitude_1(float value)
	{
		___Amplitude_1 = value;
	}

	inline static int32_t get_offset_of_Constant_2() { return static_cast<int32_t>(offsetof(NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229, ___Constant_2)); }
	inline bool get_Constant_2() const { return ___Constant_2; }
	inline bool* get_address_of_Constant_2() { return &___Constant_2; }
	inline void set_Constant_2(bool value)
	{
		___Constant_2 = value;
	}
};

// Native definition for P/Invoke marshalling of Cinemachine.NoiseSettings/NoiseParams
struct NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229_marshaled_pinvoke
{
	float ___Frequency_0;
	float ___Amplitude_1;
	int32_t ___Constant_2;
};
// Native definition for COM marshalling of Cinemachine.NoiseSettings/NoiseParams
struct NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229_marshaled_com
{
	float ___Frequency_0;
	float ___Amplitude_1;
	int32_t ___Constant_2;
};

// Cinemachine.Utility.GaussianWindow1D_CameraRotation
struct GaussianWindow1D_CameraRotation_t88B75023ED9125C4BE06144C20215CD10666641D  : public GaussianWindow1d_1_t10F20E064ADE0A33D663FD5C4A4BA8B9BDC981AD
{
public:

public:
};


// Cinemachine.Utility.GaussianWindow1D_Quaternion
struct GaussianWindow1D_Quaternion_t7B6418660E5E1E7E1CDC2E2AD08FADD17C4EECC3  : public GaussianWindow1d_1_tD306E7D9C22D8A3423192901A6231996D605EAFC
{
public:

public:
};


// Cinemachine.Utility.GaussianWindow1D_Vector3
struct GaussianWindow1D_Vector3_t3D2CA98CAA335A6CA30F2F3E51CEC6FA69C1EC48  : public GaussianWindow1d_1_tB879E544550B44574C9A5A21528AB3884F3580DD
{
public:

public:
};


// InputSettings/InteractionActions
struct InteractionActions_tA55A474A3250F62C19F1B26508F484D589100F02 
{
public:
	// InputSettings InputSettings/InteractionActions::m_Wrapper
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * ___m_Wrapper_0;

public:
	inline static int32_t get_offset_of_m_Wrapper_0() { return static_cast<int32_t>(offsetof(InteractionActions_tA55A474A3250F62C19F1B26508F484D589100F02, ___m_Wrapper_0)); }
	inline InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * get_m_Wrapper_0() const { return ___m_Wrapper_0; }
	inline InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE ** get_address_of_m_Wrapper_0() { return &___m_Wrapper_0; }
	inline void set_m_Wrapper_0(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * value)
	{
		___m_Wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Wrapper_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of InputSettings/InteractionActions
struct InteractionActions_tA55A474A3250F62C19F1B26508F484D589100F02_marshaled_pinvoke
{
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * ___m_Wrapper_0;
};
// Native definition for COM marshalling of InputSettings/InteractionActions
struct InteractionActions_tA55A474A3250F62C19F1B26508F484D589100F02_marshaled_com
{
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * ___m_Wrapper_0;
};

// InputSettings/MovimentActions
struct MovimentActions_t043B55B8C65750A7ED088DFE82D773D6F757DDFA 
{
public:
	// InputSettings InputSettings/MovimentActions::m_Wrapper
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * ___m_Wrapper_0;

public:
	inline static int32_t get_offset_of_m_Wrapper_0() { return static_cast<int32_t>(offsetof(MovimentActions_t043B55B8C65750A7ED088DFE82D773D6F757DDFA, ___m_Wrapper_0)); }
	inline InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * get_m_Wrapper_0() const { return ___m_Wrapper_0; }
	inline InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE ** get_address_of_m_Wrapper_0() { return &___m_Wrapper_0; }
	inline void set_m_Wrapper_0(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * value)
	{
		___m_Wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Wrapper_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of InputSettings/MovimentActions
struct MovimentActions_t043B55B8C65750A7ED088DFE82D773D6F757DDFA_marshaled_pinvoke
{
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * ___m_Wrapper_0;
};
// Native definition for COM marshalling of InputSettings/MovimentActions
struct MovimentActions_t043B55B8C65750A7ED088DFE82D773D6F757DDFA_marshaled_com
{
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * ___m_Wrapper_0;
};

// InputSettings/PauseActions
struct PauseActions_tC24EA586A943A15DF870FD096027ED46912BC0AE 
{
public:
	// InputSettings InputSettings/PauseActions::m_Wrapper
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * ___m_Wrapper_0;

public:
	inline static int32_t get_offset_of_m_Wrapper_0() { return static_cast<int32_t>(offsetof(PauseActions_tC24EA586A943A15DF870FD096027ED46912BC0AE, ___m_Wrapper_0)); }
	inline InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * get_m_Wrapper_0() const { return ___m_Wrapper_0; }
	inline InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE ** get_address_of_m_Wrapper_0() { return &___m_Wrapper_0; }
	inline void set_m_Wrapper_0(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * value)
	{
		___m_Wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Wrapper_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of InputSettings/PauseActions
struct PauseActions_tC24EA586A943A15DF870FD096027ED46912BC0AE_marshaled_pinvoke
{
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * ___m_Wrapper_0;
};
// Native definition for COM marshalling of InputSettings/PauseActions
struct PauseActions_tC24EA586A943A15DF870FD096027ED46912BC0AE_marshaled_com
{
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * ___m_Wrapper_0;
};

// System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateMachine_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_defaultContextAction_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_defaultContextAction_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};

// System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F 
{
public:
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.TaskAwaiter::m_task
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F, ___m_task_0)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_m_task_0() const { return ___m_task_0; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_marshaled_pinvoke
{
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_marshaled_com
{
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
};

// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Color
struct Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.LayerMask
struct LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Matrix4x4
struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// UnityEngine.Vector2
struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_tF5473A948C7ED035EB072C0E8916F6AC30F9643E  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tF5473A948C7ED035EB072C0E8916F6AC30F9643E_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::78517443912BB49729313EC23065D9970ABC80E3
	__StaticArrayInitTypeSizeU3D12_t424CC67AAA71D9952D514D413907E974A287EC65  ___78517443912BB49729313EC23065D9970ABC80E3_0;

public:
	inline static int32_t get_offset_of_U378517443912BB49729313EC23065D9970ABC80E3_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tF5473A948C7ED035EB072C0E8916F6AC30F9643E_StaticFields, ___78517443912BB49729313EC23065D9970ABC80E3_0)); }
	inline __StaticArrayInitTypeSizeU3D12_t424CC67AAA71D9952D514D413907E974A287EC65  get_U378517443912BB49729313EC23065D9970ABC80E3_0() const { return ___78517443912BB49729313EC23065D9970ABC80E3_0; }
	inline __StaticArrayInitTypeSizeU3D12_t424CC67AAA71D9952D514D413907E974A287EC65 * get_address_of_U378517443912BB49729313EC23065D9970ABC80E3_0() { return &___78517443912BB49729313EC23065D9970ABC80E3_0; }
	inline void set_U378517443912BB49729313EC23065D9970ABC80E3_0(__StaticArrayInitTypeSizeU3D12_t424CC67AAA71D9952D514D413907E974A287EC65  value)
	{
		___78517443912BB49729313EC23065D9970ABC80E3_0 = value;
	}
};


// Cinemachine.CinemachineEmbeddedAssetPropertyAttribute
struct CinemachineEmbeddedAssetPropertyAttribute_t6A5ABA333EA100382FB10CCDA663908F397DCC14  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:
	// System.Boolean Cinemachine.CinemachineEmbeddedAssetPropertyAttribute::WarnIfNull
	bool ___WarnIfNull_0;

public:
	inline static int32_t get_offset_of_WarnIfNull_0() { return static_cast<int32_t>(offsetof(CinemachineEmbeddedAssetPropertyAttribute_t6A5ABA333EA100382FB10CCDA663908F397DCC14, ___WarnIfNull_0)); }
	inline bool get_WarnIfNull_0() const { return ___WarnIfNull_0; }
	inline bool* get_address_of_WarnIfNull_0() { return &___WarnIfNull_0; }
	inline void set_WarnIfNull_0(bool value)
	{
		___WarnIfNull_0 = value;
	}
};


// Cinemachine.CinemachineImpulseChannelPropertyAttribute
struct CinemachineImpulseChannelPropertyAttribute_t6BF9C93F9B8D1B758BABD2BDE3DD84D5D6368C0A  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:

public:
};


// Cinemachine.CinemachineImpulseDefinition/RepeatMode
struct RepeatMode_t3EE130ABD9F284BF06DE68C86E74709D724803FD 
{
public:
	// System.Int32 Cinemachine.CinemachineImpulseDefinition/RepeatMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RepeatMode_t3EE130ABD9F284BF06DE68C86E74709D724803FD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineImpulseDefinition/SignalSource
struct SignalSource_t9CDE1AB5C86593ACD3888A3C580CE5F19358125E  : public RuntimeObject
{
public:
	// Cinemachine.CinemachineImpulseDefinition Cinemachine.CinemachineImpulseDefinition/SignalSource::m_Def
	CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF * ___m_Def_0;
	// UnityEngine.Vector3 Cinemachine.CinemachineImpulseDefinition/SignalSource::m_Velocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Velocity_1;
	// System.Single Cinemachine.CinemachineImpulseDefinition/SignalSource::m_StartTimeOffset
	float ___m_StartTimeOffset_2;

public:
	inline static int32_t get_offset_of_m_Def_0() { return static_cast<int32_t>(offsetof(SignalSource_t9CDE1AB5C86593ACD3888A3C580CE5F19358125E, ___m_Def_0)); }
	inline CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF * get_m_Def_0() const { return ___m_Def_0; }
	inline CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF ** get_address_of_m_Def_0() { return &___m_Def_0; }
	inline void set_m_Def_0(CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF * value)
	{
		___m_Def_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Def_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Velocity_1() { return static_cast<int32_t>(offsetof(SignalSource_t9CDE1AB5C86593ACD3888A3C580CE5F19358125E, ___m_Velocity_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Velocity_1() const { return ___m_Velocity_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Velocity_1() { return &___m_Velocity_1; }
	inline void set_m_Velocity_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Velocity_1 = value;
	}

	inline static int32_t get_offset_of_m_StartTimeOffset_2() { return static_cast<int32_t>(offsetof(SignalSource_t9CDE1AB5C86593ACD3888A3C580CE5F19358125E, ___m_StartTimeOffset_2)); }
	inline float get_m_StartTimeOffset_2() const { return ___m_StartTimeOffset_2; }
	inline float* get_address_of_m_StartTimeOffset_2() { return &___m_StartTimeOffset_2; }
	inline void set_m_StartTimeOffset_2(float value)
	{
		___m_StartTimeOffset_2 = value;
	}
};


// Cinemachine.CinemachineImpulseDefinitionPropertyAttribute
struct CinemachineImpulseDefinitionPropertyAttribute_tE39EAFF04AFBA44050B1F0B5EF3CED4653C67D24  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:

public:
};


// Cinemachine.CinemachineImpulseEnvelopePropertyAttribute
struct CinemachineImpulseEnvelopePropertyAttribute_tC41425006E83643EA2F259F979894D82B6FC0A0E  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:

public:
};


// Cinemachine.CinemachineImpulseManager/ImpulseEvent/DirectionMode
struct DirectionMode_t1C0606ECAAD200158217A6BE23476C6FDF0178F6 
{
public:
	// System.Int32 Cinemachine.CinemachineImpulseManager/ImpulseEvent/DirectionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DirectionMode_t1C0606ECAAD200158217A6BE23476C6FDF0178F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineImpulseManager/ImpulseEvent/DissipationMode
struct DissipationMode_t768C05D9CFC3FE4D58AEE5AAC147A88E30DC160A 
{
public:
	// System.Int32 Cinemachine.CinemachineImpulseManager/ImpulseEvent/DissipationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DissipationMode_t768C05D9CFC3FE4D58AEE5AAC147A88E30DC160A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineTriggerAction/ActionSettings/Mode
struct Mode_t4B3F51421E88722B3CBE2723C07642D5CFD4B637 
{
public:
	// System.Int32 Cinemachine.CinemachineTriggerAction/ActionSettings/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t4B3F51421E88722B3CBE2723C07642D5CFD4B637, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineTriggerAction/ActionSettings/TimeMode
struct TimeMode_t45986AB4C1F762E22CAFC62432578E5343596E08 
{
public:
	// System.Int32 Cinemachine.CinemachineTriggerAction/ActionSettings/TimeMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TimeMode_t45986AB4C1F762E22CAFC62432578E5343596E08, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineTriggerAction/ActionSettings/TriggerEvent
struct TriggerEvent_t4D34422A1066EEC819DFFEA8DD88FA76A44A9CA7  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};


// Cinemachine.CinemachineVirtualCameraBase/BlendHint
struct BlendHint_t7D5604B2298076677193EF9C12950B17EA52ACA5 
{
public:
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase/BlendHint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlendHint_t7D5604B2298076677193EF9C12950B17EA52ACA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineVirtualCameraBase/StandbyUpdateMode
struct StandbyUpdateMode_tDD387031AFF8EB8516338BC71517029CB6F1BE18 
{
public:
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase/StandbyUpdateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StandbyUpdateMode_tDD387031AFF8EB8516338BC71517029CB6F1BE18, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.DocumentationSortingAttribute/Level
struct Level_tFCA1BDCF2D24439BC272A8BEE1A8EBBABA21F29A 
{
public:
	// System.Int32 Cinemachine.DocumentationSortingAttribute/Level::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Level_tFCA1BDCF2D24439BC272A8BEE1A8EBBABA21F29A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.LensSettings
struct LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC 
{
public:
	// System.Single Cinemachine.LensSettings::FieldOfView
	float ___FieldOfView_1;
	// System.Single Cinemachine.LensSettings::OrthographicSize
	float ___OrthographicSize_2;
	// System.Single Cinemachine.LensSettings::NearClipPlane
	float ___NearClipPlane_3;
	// System.Single Cinemachine.LensSettings::FarClipPlane
	float ___FarClipPlane_4;
	// System.Single Cinemachine.LensSettings::Dutch
	float ___Dutch_5;
	// System.Boolean Cinemachine.LensSettings::<Orthographic>k__BackingField
	bool ___U3COrthographicU3Ek__BackingField_6;
	// System.Boolean Cinemachine.LensSettings::<IsPhysicalCamera>k__BackingField
	bool ___U3CIsPhysicalCameraU3Ek__BackingField_7;
	// UnityEngine.Vector2 Cinemachine.LensSettings::<SensorSize>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CSensorSizeU3Ek__BackingField_8;
	// UnityEngine.Vector2 Cinemachine.LensSettings::LensShift
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___LensShift_9;

public:
	inline static int32_t get_offset_of_FieldOfView_1() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___FieldOfView_1)); }
	inline float get_FieldOfView_1() const { return ___FieldOfView_1; }
	inline float* get_address_of_FieldOfView_1() { return &___FieldOfView_1; }
	inline void set_FieldOfView_1(float value)
	{
		___FieldOfView_1 = value;
	}

	inline static int32_t get_offset_of_OrthographicSize_2() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___OrthographicSize_2)); }
	inline float get_OrthographicSize_2() const { return ___OrthographicSize_2; }
	inline float* get_address_of_OrthographicSize_2() { return &___OrthographicSize_2; }
	inline void set_OrthographicSize_2(float value)
	{
		___OrthographicSize_2 = value;
	}

	inline static int32_t get_offset_of_NearClipPlane_3() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___NearClipPlane_3)); }
	inline float get_NearClipPlane_3() const { return ___NearClipPlane_3; }
	inline float* get_address_of_NearClipPlane_3() { return &___NearClipPlane_3; }
	inline void set_NearClipPlane_3(float value)
	{
		___NearClipPlane_3 = value;
	}

	inline static int32_t get_offset_of_FarClipPlane_4() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___FarClipPlane_4)); }
	inline float get_FarClipPlane_4() const { return ___FarClipPlane_4; }
	inline float* get_address_of_FarClipPlane_4() { return &___FarClipPlane_4; }
	inline void set_FarClipPlane_4(float value)
	{
		___FarClipPlane_4 = value;
	}

	inline static int32_t get_offset_of_Dutch_5() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___Dutch_5)); }
	inline float get_Dutch_5() const { return ___Dutch_5; }
	inline float* get_address_of_Dutch_5() { return &___Dutch_5; }
	inline void set_Dutch_5(float value)
	{
		___Dutch_5 = value;
	}

	inline static int32_t get_offset_of_U3COrthographicU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___U3COrthographicU3Ek__BackingField_6)); }
	inline bool get_U3COrthographicU3Ek__BackingField_6() const { return ___U3COrthographicU3Ek__BackingField_6; }
	inline bool* get_address_of_U3COrthographicU3Ek__BackingField_6() { return &___U3COrthographicU3Ek__BackingField_6; }
	inline void set_U3COrthographicU3Ek__BackingField_6(bool value)
	{
		___U3COrthographicU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CIsPhysicalCameraU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___U3CIsPhysicalCameraU3Ek__BackingField_7)); }
	inline bool get_U3CIsPhysicalCameraU3Ek__BackingField_7() const { return ___U3CIsPhysicalCameraU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsPhysicalCameraU3Ek__BackingField_7() { return &___U3CIsPhysicalCameraU3Ek__BackingField_7; }
	inline void set_U3CIsPhysicalCameraU3Ek__BackingField_7(bool value)
	{
		___U3CIsPhysicalCameraU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CSensorSizeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___U3CSensorSizeU3Ek__BackingField_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CSensorSizeU3Ek__BackingField_8() const { return ___U3CSensorSizeU3Ek__BackingField_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CSensorSizeU3Ek__BackingField_8() { return &___U3CSensorSizeU3Ek__BackingField_8; }
	inline void set_U3CSensorSizeU3Ek__BackingField_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CSensorSizeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_LensShift_9() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC, ___LensShift_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_LensShift_9() const { return ___LensShift_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_LensShift_9() { return &___LensShift_9; }
	inline void set_LensShift_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___LensShift_9 = value;
	}
};

struct LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC_StaticFields
{
public:
	// Cinemachine.LensSettings Cinemachine.LensSettings::Default
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC_StaticFields, ___Default_0)); }
	inline LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  get_Default_0() const { return ___Default_0; }
	inline LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC * get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC  value)
	{
		___Default_0 = value;
	}
};

// Native definition for P/Invoke marshalling of Cinemachine.LensSettings
struct LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC_marshaled_pinvoke
{
	float ___FieldOfView_1;
	float ___OrthographicSize_2;
	float ___NearClipPlane_3;
	float ___FarClipPlane_4;
	float ___Dutch_5;
	int32_t ___U3COrthographicU3Ek__BackingField_6;
	int32_t ___U3CIsPhysicalCameraU3Ek__BackingField_7;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CSensorSizeU3Ek__BackingField_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___LensShift_9;
};
// Native definition for COM marshalling of Cinemachine.LensSettings
struct LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC_marshaled_com
{
	float ___FieldOfView_1;
	float ___OrthographicSize_2;
	float ___NearClipPlane_3;
	float ___FarClipPlane_4;
	float ___Dutch_5;
	int32_t ___U3COrthographicU3Ek__BackingField_6;
	int32_t ___U3CIsPhysicalCameraU3Ek__BackingField_7;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CSensorSizeU3Ek__BackingField_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___LensShift_9;
};

// Cinemachine.NoiseSettings/TransformNoiseParams
struct TransformNoiseParams_t426C9F41209D45543A8DCACA9C6194ECE590C27D 
{
public:
	// Cinemachine.NoiseSettings/NoiseParams Cinemachine.NoiseSettings/TransformNoiseParams::X
	NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229  ___X_0;
	// Cinemachine.NoiseSettings/NoiseParams Cinemachine.NoiseSettings/TransformNoiseParams::Y
	NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229  ___Y_1;
	// Cinemachine.NoiseSettings/NoiseParams Cinemachine.NoiseSettings/TransformNoiseParams::Z
	NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229  ___Z_2;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(TransformNoiseParams_t426C9F41209D45543A8DCACA9C6194ECE590C27D, ___X_0)); }
	inline NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229  get_X_0() const { return ___X_0; }
	inline NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229 * get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229  value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(TransformNoiseParams_t426C9F41209D45543A8DCACA9C6194ECE590C27D, ___Y_1)); }
	inline NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229  get_Y_1() const { return ___Y_1; }
	inline NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229 * get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229  value)
	{
		___Y_1 = value;
	}

	inline static int32_t get_offset_of_Z_2() { return static_cast<int32_t>(offsetof(TransformNoiseParams_t426C9F41209D45543A8DCACA9C6194ECE590C27D, ___Z_2)); }
	inline NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229  get_Z_2() const { return ___Z_2; }
	inline NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229 * get_address_of_Z_2() { return &___Z_2; }
	inline void set_Z_2(NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229  value)
	{
		___Z_2 = value;
	}
};

// Native definition for P/Invoke marshalling of Cinemachine.NoiseSettings/TransformNoiseParams
struct TransformNoiseParams_t426C9F41209D45543A8DCACA9C6194ECE590C27D_marshaled_pinvoke
{
	NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229_marshaled_pinvoke ___X_0;
	NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229_marshaled_pinvoke ___Y_1;
	NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229_marshaled_pinvoke ___Z_2;
};
// Native definition for COM marshalling of Cinemachine.NoiseSettings/TransformNoiseParams
struct TransformNoiseParams_t426C9F41209D45543A8DCACA9C6194ECE590C27D_marshaled_com
{
	NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229_marshaled_com ___X_0;
	NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229_marshaled_com ___Y_1;
	NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229_marshaled_com ___Z_2;
};

// Cinemachine.NoiseSettingsPropertyAttribute
struct NoiseSettingsPropertyAttribute_tBCFE5EE5B22D5A7D8D44BC1F4D329B7A65F7B503  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:

public:
};


// Cinemachine.UpdateTracker/UpdateClock
struct UpdateClock_tC6995F64381C6E9302A218387A9805FA1680EBED 
{
public:
	// System.Int32 Cinemachine.UpdateTracker/UpdateClock::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateClock_tC6995F64381C6E9302A218387A9805FA1680EBED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.Utility.HeadingTracker
struct HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4  : public RuntimeObject
{
public:
	// Cinemachine.Utility.HeadingTracker/Item[] Cinemachine.Utility.HeadingTracker::mHistory
	ItemU5BU5D_t7D008EEEF404065A770D7C42085C86F254078B46* ___mHistory_0;
	// System.Int32 Cinemachine.Utility.HeadingTracker::mTop
	int32_t ___mTop_1;
	// System.Int32 Cinemachine.Utility.HeadingTracker::mBottom
	int32_t ___mBottom_2;
	// System.Int32 Cinemachine.Utility.HeadingTracker::mCount
	int32_t ___mCount_3;
	// UnityEngine.Vector3 Cinemachine.Utility.HeadingTracker::mHeadingSum
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mHeadingSum_4;
	// System.Single Cinemachine.Utility.HeadingTracker::mWeightSum
	float ___mWeightSum_5;
	// System.Single Cinemachine.Utility.HeadingTracker::mWeightTime
	float ___mWeightTime_6;
	// UnityEngine.Vector3 Cinemachine.Utility.HeadingTracker::mLastGoodHeading
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mLastGoodHeading_7;

public:
	inline static int32_t get_offset_of_mHistory_0() { return static_cast<int32_t>(offsetof(HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4, ___mHistory_0)); }
	inline ItemU5BU5D_t7D008EEEF404065A770D7C42085C86F254078B46* get_mHistory_0() const { return ___mHistory_0; }
	inline ItemU5BU5D_t7D008EEEF404065A770D7C42085C86F254078B46** get_address_of_mHistory_0() { return &___mHistory_0; }
	inline void set_mHistory_0(ItemU5BU5D_t7D008EEEF404065A770D7C42085C86F254078B46* value)
	{
		___mHistory_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mHistory_0), (void*)value);
	}

	inline static int32_t get_offset_of_mTop_1() { return static_cast<int32_t>(offsetof(HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4, ___mTop_1)); }
	inline int32_t get_mTop_1() const { return ___mTop_1; }
	inline int32_t* get_address_of_mTop_1() { return &___mTop_1; }
	inline void set_mTop_1(int32_t value)
	{
		___mTop_1 = value;
	}

	inline static int32_t get_offset_of_mBottom_2() { return static_cast<int32_t>(offsetof(HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4, ___mBottom_2)); }
	inline int32_t get_mBottom_2() const { return ___mBottom_2; }
	inline int32_t* get_address_of_mBottom_2() { return &___mBottom_2; }
	inline void set_mBottom_2(int32_t value)
	{
		___mBottom_2 = value;
	}

	inline static int32_t get_offset_of_mCount_3() { return static_cast<int32_t>(offsetof(HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4, ___mCount_3)); }
	inline int32_t get_mCount_3() const { return ___mCount_3; }
	inline int32_t* get_address_of_mCount_3() { return &___mCount_3; }
	inline void set_mCount_3(int32_t value)
	{
		___mCount_3 = value;
	}

	inline static int32_t get_offset_of_mHeadingSum_4() { return static_cast<int32_t>(offsetof(HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4, ___mHeadingSum_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mHeadingSum_4() const { return ___mHeadingSum_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mHeadingSum_4() { return &___mHeadingSum_4; }
	inline void set_mHeadingSum_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mHeadingSum_4 = value;
	}

	inline static int32_t get_offset_of_mWeightSum_5() { return static_cast<int32_t>(offsetof(HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4, ___mWeightSum_5)); }
	inline float get_mWeightSum_5() const { return ___mWeightSum_5; }
	inline float* get_address_of_mWeightSum_5() { return &___mWeightSum_5; }
	inline void set_mWeightSum_5(float value)
	{
		___mWeightSum_5 = value;
	}

	inline static int32_t get_offset_of_mWeightTime_6() { return static_cast<int32_t>(offsetof(HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4, ___mWeightTime_6)); }
	inline float get_mWeightTime_6() const { return ___mWeightTime_6; }
	inline float* get_address_of_mWeightTime_6() { return &___mWeightTime_6; }
	inline void set_mWeightTime_6(float value)
	{
		___mWeightTime_6 = value;
	}

	inline static int32_t get_offset_of_mLastGoodHeading_7() { return static_cast<int32_t>(offsetof(HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4, ___mLastGoodHeading_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mLastGoodHeading_7() const { return ___mLastGoodHeading_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mLastGoodHeading_7() { return &___mLastGoodHeading_7; }
	inline void set_mLastGoodHeading_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mLastGoodHeading_7 = value;
	}
};

struct HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4_StaticFields
{
public:
	// System.Single Cinemachine.Utility.HeadingTracker::mDecayExponent
	float ___mDecayExponent_8;

public:
	inline static int32_t get_offset_of_mDecayExponent_8() { return static_cast<int32_t>(offsetof(HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4_StaticFields, ___mDecayExponent_8)); }
	inline float get_mDecayExponent_8() const { return ___mDecayExponent_8; }
	inline float* get_address_of_mDecayExponent_8() { return &___mDecayExponent_8; }
	inline void set_mDecayExponent_8(float value)
	{
		___mDecayExponent_8 = value;
	}
};


// Cinemachine.Utility.HeadingTracker/Item
struct Item_t3B6B467A1D39665462BC0A703E543496169303B4 
{
public:
	// UnityEngine.Vector3 Cinemachine.Utility.HeadingTracker/Item::velocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___velocity_0;
	// System.Single Cinemachine.Utility.HeadingTracker/Item::weight
	float ___weight_1;
	// System.Single Cinemachine.Utility.HeadingTracker/Item::time
	float ___time_2;

public:
	inline static int32_t get_offset_of_velocity_0() { return static_cast<int32_t>(offsetof(Item_t3B6B467A1D39665462BC0A703E543496169303B4, ___velocity_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_velocity_0() const { return ___velocity_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_velocity_0() { return &___velocity_0; }
	inline void set_velocity_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___velocity_0 = value;
	}

	inline static int32_t get_offset_of_weight_1() { return static_cast<int32_t>(offsetof(Item_t3B6B467A1D39665462BC0A703E543496169303B4, ___weight_1)); }
	inline float get_weight_1() const { return ___weight_1; }
	inline float* get_address_of_weight_1() { return &___weight_1; }
	inline void set_weight_1(float value)
	{
		___weight_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(Item_t3B6B467A1D39665462BC0A703E543496169303B4, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}
};


// Cinemachine.Utility.PositionPredictor
struct PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Cinemachine.Utility.PositionPredictor::m_Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Position_0;
	// Cinemachine.Utility.GaussianWindow1D_Vector3 Cinemachine.Utility.PositionPredictor::m_Velocity
	GaussianWindow1D_Vector3_t3D2CA98CAA335A6CA30F2F3E51CEC6FA69C1EC48 * ___m_Velocity_1;
	// Cinemachine.Utility.GaussianWindow1D_Vector3 Cinemachine.Utility.PositionPredictor::m_Accel
	GaussianWindow1D_Vector3_t3D2CA98CAA335A6CA30F2F3E51CEC6FA69C1EC48 * ___m_Accel_2;
	// System.Single Cinemachine.Utility.PositionPredictor::mLastVelAddedTime
	float ___mLastVelAddedTime_3;
	// System.Single Cinemachine.Utility.PositionPredictor::mSmoothing
	float ___mSmoothing_5;

public:
	inline static int32_t get_offset_of_m_Position_0() { return static_cast<int32_t>(offsetof(PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223, ___m_Position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Position_0() const { return ___m_Position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Position_0() { return &___m_Position_0; }
	inline void set_m_Position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Position_0 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_1() { return static_cast<int32_t>(offsetof(PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223, ___m_Velocity_1)); }
	inline GaussianWindow1D_Vector3_t3D2CA98CAA335A6CA30F2F3E51CEC6FA69C1EC48 * get_m_Velocity_1() const { return ___m_Velocity_1; }
	inline GaussianWindow1D_Vector3_t3D2CA98CAA335A6CA30F2F3E51CEC6FA69C1EC48 ** get_address_of_m_Velocity_1() { return &___m_Velocity_1; }
	inline void set_m_Velocity_1(GaussianWindow1D_Vector3_t3D2CA98CAA335A6CA30F2F3E51CEC6FA69C1EC48 * value)
	{
		___m_Velocity_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Velocity_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Accel_2() { return static_cast<int32_t>(offsetof(PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223, ___m_Accel_2)); }
	inline GaussianWindow1D_Vector3_t3D2CA98CAA335A6CA30F2F3E51CEC6FA69C1EC48 * get_m_Accel_2() const { return ___m_Accel_2; }
	inline GaussianWindow1D_Vector3_t3D2CA98CAA335A6CA30F2F3E51CEC6FA69C1EC48 ** get_address_of_m_Accel_2() { return &___m_Accel_2; }
	inline void set_m_Accel_2(GaussianWindow1D_Vector3_t3D2CA98CAA335A6CA30F2F3E51CEC6FA69C1EC48 * value)
	{
		___m_Accel_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Accel_2), (void*)value);
	}

	inline static int32_t get_offset_of_mLastVelAddedTime_3() { return static_cast<int32_t>(offsetof(PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223, ___mLastVelAddedTime_3)); }
	inline float get_mLastVelAddedTime_3() const { return ___mLastVelAddedTime_3; }
	inline float* get_address_of_mLastVelAddedTime_3() { return &___mLastVelAddedTime_3; }
	inline void set_mLastVelAddedTime_3(float value)
	{
		___mLastVelAddedTime_3 = value;
	}

	inline static int32_t get_offset_of_mSmoothing_5() { return static_cast<int32_t>(offsetof(PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223, ___mSmoothing_5)); }
	inline float get_mSmoothing_5() const { return ___mSmoothing_5; }
	inline float* get_address_of_mSmoothing_5() { return &___mSmoothing_5; }
	inline void set_mSmoothing_5(float value)
	{
		___mSmoothing_5 = value;
	}
};


// CollectableType
struct CollectableType_t0EDB217F53835832065358E8C732FE7953BF4294 
{
public:
	// System.Int32 CollectableType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CollectableType_t0EDB217F53835832065358E8C732FE7953BF4294, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF 
{
public:
	// System.Threading.SynchronizationContext System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_synchronizationContext
	SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * ___m_synchronizationContext_0;
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_coreState
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  ___m_coreState_1;
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_task
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_synchronizationContext_0() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF, ___m_synchronizationContext_0)); }
	inline SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * get_m_synchronizationContext_0() const { return ___m_synchronizationContext_0; }
	inline SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 ** get_address_of_m_synchronizationContext_0() { return &___m_synchronizationContext_0; }
	inline void set_m_synchronizationContext_0(SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * value)
	{
		___m_synchronizationContext_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_synchronizationContext_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  value)
	{
		___m_coreState_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF, ___m_task_2)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_m_task_2() const { return ___m_task_2; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF_marshaled_pinvoke
{
	SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_pinvoke ___m_coreState_1;
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_2;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF_marshaled_com
{
	SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_com ___m_coreState_1;
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_2;
};

// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityStandardAssets.Water.Water/WaterMode
struct WaterMode_t1799A7D5CF5C687BBCB577D86D980CE1E42AE102 
{
public:
	// System.Int32 UnityStandardAssets.Water.Water/WaterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WaterMode_t1799A7D5CF5C687BBCB577D86D980CE1E42AE102, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityStandardAssets.Water.WaterQuality
struct WaterQuality_t06C96FA87F57ACDA33F4DE38CADF3BB167ED3FAF 
{
public:
	// System.Int32 UnityStandardAssets.Water.WaterQuality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WaterQuality_t06C96FA87F57ACDA33F4DE38CADF3BB167ED3FAF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineImpulseManager/EnvelopeDefinition
struct EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E 
{
public:
	// UnityEngine.AnimationCurve Cinemachine.CinemachineImpulseManager/EnvelopeDefinition::m_AttackShape
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_AttackShape_0;
	// UnityEngine.AnimationCurve Cinemachine.CinemachineImpulseManager/EnvelopeDefinition::m_DecayShape
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_DecayShape_1;
	// System.Single Cinemachine.CinemachineImpulseManager/EnvelopeDefinition::m_AttackTime
	float ___m_AttackTime_2;
	// System.Single Cinemachine.CinemachineImpulseManager/EnvelopeDefinition::m_SustainTime
	float ___m_SustainTime_3;
	// System.Single Cinemachine.CinemachineImpulseManager/EnvelopeDefinition::m_DecayTime
	float ___m_DecayTime_4;
	// System.Boolean Cinemachine.CinemachineImpulseManager/EnvelopeDefinition::m_ScaleWithImpact
	bool ___m_ScaleWithImpact_5;
	// System.Boolean Cinemachine.CinemachineImpulseManager/EnvelopeDefinition::m_HoldForever
	bool ___m_HoldForever_6;

public:
	inline static int32_t get_offset_of_m_AttackShape_0() { return static_cast<int32_t>(offsetof(EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E, ___m_AttackShape_0)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_AttackShape_0() const { return ___m_AttackShape_0; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_AttackShape_0() { return &___m_AttackShape_0; }
	inline void set_m_AttackShape_0(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_AttackShape_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AttackShape_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_DecayShape_1() { return static_cast<int32_t>(offsetof(EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E, ___m_DecayShape_1)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_DecayShape_1() const { return ___m_DecayShape_1; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_DecayShape_1() { return &___m_DecayShape_1; }
	inline void set_m_DecayShape_1(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_DecayShape_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DecayShape_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_AttackTime_2() { return static_cast<int32_t>(offsetof(EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E, ___m_AttackTime_2)); }
	inline float get_m_AttackTime_2() const { return ___m_AttackTime_2; }
	inline float* get_address_of_m_AttackTime_2() { return &___m_AttackTime_2; }
	inline void set_m_AttackTime_2(float value)
	{
		___m_AttackTime_2 = value;
	}

	inline static int32_t get_offset_of_m_SustainTime_3() { return static_cast<int32_t>(offsetof(EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E, ___m_SustainTime_3)); }
	inline float get_m_SustainTime_3() const { return ___m_SustainTime_3; }
	inline float* get_address_of_m_SustainTime_3() { return &___m_SustainTime_3; }
	inline void set_m_SustainTime_3(float value)
	{
		___m_SustainTime_3 = value;
	}

	inline static int32_t get_offset_of_m_DecayTime_4() { return static_cast<int32_t>(offsetof(EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E, ___m_DecayTime_4)); }
	inline float get_m_DecayTime_4() const { return ___m_DecayTime_4; }
	inline float* get_address_of_m_DecayTime_4() { return &___m_DecayTime_4; }
	inline void set_m_DecayTime_4(float value)
	{
		___m_DecayTime_4 = value;
	}

	inline static int32_t get_offset_of_m_ScaleWithImpact_5() { return static_cast<int32_t>(offsetof(EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E, ___m_ScaleWithImpact_5)); }
	inline bool get_m_ScaleWithImpact_5() const { return ___m_ScaleWithImpact_5; }
	inline bool* get_address_of_m_ScaleWithImpact_5() { return &___m_ScaleWithImpact_5; }
	inline void set_m_ScaleWithImpact_5(bool value)
	{
		___m_ScaleWithImpact_5 = value;
	}

	inline static int32_t get_offset_of_m_HoldForever_6() { return static_cast<int32_t>(offsetof(EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E, ___m_HoldForever_6)); }
	inline bool get_m_HoldForever_6() const { return ___m_HoldForever_6; }
	inline bool* get_address_of_m_HoldForever_6() { return &___m_HoldForever_6; }
	inline void set_m_HoldForever_6(bool value)
	{
		___m_HoldForever_6 = value;
	}
};

// Native definition for P/Invoke marshalling of Cinemachine.CinemachineImpulseManager/EnvelopeDefinition
struct EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E_marshaled_pinvoke
{
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_pinvoke ___m_AttackShape_0;
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_pinvoke ___m_DecayShape_1;
	float ___m_AttackTime_2;
	float ___m_SustainTime_3;
	float ___m_DecayTime_4;
	int32_t ___m_ScaleWithImpact_5;
	int32_t ___m_HoldForever_6;
};
// Native definition for COM marshalling of Cinemachine.CinemachineImpulseManager/EnvelopeDefinition
struct EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E_marshaled_com
{
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_com* ___m_AttackShape_0;
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C_marshaled_com* ___m_DecayShape_1;
	float ___m_AttackTime_2;
	float ___m_SustainTime_3;
	float ___m_DecayTime_4;
	int32_t ___m_ScaleWithImpact_5;
	int32_t ___m_HoldForever_6;
};

// Cinemachine.CinemachineTriggerAction/ActionSettings
struct ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2 
{
public:
	// Cinemachine.CinemachineTriggerAction/ActionSettings/Mode Cinemachine.CinemachineTriggerAction/ActionSettings::m_Action
	int32_t ___m_Action_0;
	// UnityEngine.Object Cinemachine.CinemachineTriggerAction/ActionSettings::m_Target
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___m_Target_1;
	// System.Int32 Cinemachine.CinemachineTriggerAction/ActionSettings::m_BoostAmount
	int32_t ___m_BoostAmount_2;
	// System.Single Cinemachine.CinemachineTriggerAction/ActionSettings::m_StartTime
	float ___m_StartTime_3;
	// Cinemachine.CinemachineTriggerAction/ActionSettings/TimeMode Cinemachine.CinemachineTriggerAction/ActionSettings::m_Mode
	int32_t ___m_Mode_4;
	// Cinemachine.CinemachineTriggerAction/ActionSettings/TriggerEvent Cinemachine.CinemachineTriggerAction/ActionSettings::m_Event
	TriggerEvent_t4D34422A1066EEC819DFFEA8DD88FA76A44A9CA7 * ___m_Event_5;

public:
	inline static int32_t get_offset_of_m_Action_0() { return static_cast<int32_t>(offsetof(ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2, ___m_Action_0)); }
	inline int32_t get_m_Action_0() const { return ___m_Action_0; }
	inline int32_t* get_address_of_m_Action_0() { return &___m_Action_0; }
	inline void set_m_Action_0(int32_t value)
	{
		___m_Action_0 = value;
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2, ___m_Target_1)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Target_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_BoostAmount_2() { return static_cast<int32_t>(offsetof(ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2, ___m_BoostAmount_2)); }
	inline int32_t get_m_BoostAmount_2() const { return ___m_BoostAmount_2; }
	inline int32_t* get_address_of_m_BoostAmount_2() { return &___m_BoostAmount_2; }
	inline void set_m_BoostAmount_2(int32_t value)
	{
		___m_BoostAmount_2 = value;
	}

	inline static int32_t get_offset_of_m_StartTime_3() { return static_cast<int32_t>(offsetof(ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2, ___m_StartTime_3)); }
	inline float get_m_StartTime_3() const { return ___m_StartTime_3; }
	inline float* get_address_of_m_StartTime_3() { return &___m_StartTime_3; }
	inline void set_m_StartTime_3(float value)
	{
		___m_StartTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Mode_4() { return static_cast<int32_t>(offsetof(ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2, ___m_Mode_4)); }
	inline int32_t get_m_Mode_4() const { return ___m_Mode_4; }
	inline int32_t* get_address_of_m_Mode_4() { return &___m_Mode_4; }
	inline void set_m_Mode_4(int32_t value)
	{
		___m_Mode_4 = value;
	}

	inline static int32_t get_offset_of_m_Event_5() { return static_cast<int32_t>(offsetof(ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2, ___m_Event_5)); }
	inline TriggerEvent_t4D34422A1066EEC819DFFEA8DD88FA76A44A9CA7 * get_m_Event_5() const { return ___m_Event_5; }
	inline TriggerEvent_t4D34422A1066EEC819DFFEA8DD88FA76A44A9CA7 ** get_address_of_m_Event_5() { return &___m_Event_5; }
	inline void set_m_Event_5(TriggerEvent_t4D34422A1066EEC819DFFEA8DD88FA76A44A9CA7 * value)
	{
		___m_Event_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Event_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Cinemachine.CinemachineTriggerAction/ActionSettings
struct ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2_marshaled_pinvoke
{
	int32_t ___m_Action_0;
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke ___m_Target_1;
	int32_t ___m_BoostAmount_2;
	float ___m_StartTime_3;
	int32_t ___m_Mode_4;
	TriggerEvent_t4D34422A1066EEC819DFFEA8DD88FA76A44A9CA7 * ___m_Event_5;
};
// Native definition for COM marshalling of Cinemachine.CinemachineTriggerAction/ActionSettings
struct ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2_marshaled_com
{
	int32_t ___m_Action_0;
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com* ___m_Target_1;
	int32_t ___m_BoostAmount_2;
	float ___m_StartTime_3;
	int32_t ___m_Mode_4;
	TriggerEvent_t4D34422A1066EEC819DFFEA8DD88FA76A44A9CA7 * ___m_Event_5;
};

// Cinemachine.CinemachineVirtualCameraBase/TransitionParams
struct TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8 
{
public:
	// Cinemachine.CinemachineVirtualCameraBase/BlendHint Cinemachine.CinemachineVirtualCameraBase/TransitionParams::m_BlendHint
	int32_t ___m_BlendHint_0;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase/TransitionParams::m_InheritPosition
	bool ___m_InheritPosition_1;
	// Cinemachine.CinemachineBrain/VcamActivatedEvent Cinemachine.CinemachineVirtualCameraBase/TransitionParams::m_OnCameraLive
	VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B * ___m_OnCameraLive_2;

public:
	inline static int32_t get_offset_of_m_BlendHint_0() { return static_cast<int32_t>(offsetof(TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8, ___m_BlendHint_0)); }
	inline int32_t get_m_BlendHint_0() const { return ___m_BlendHint_0; }
	inline int32_t* get_address_of_m_BlendHint_0() { return &___m_BlendHint_0; }
	inline void set_m_BlendHint_0(int32_t value)
	{
		___m_BlendHint_0 = value;
	}

	inline static int32_t get_offset_of_m_InheritPosition_1() { return static_cast<int32_t>(offsetof(TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8, ___m_InheritPosition_1)); }
	inline bool get_m_InheritPosition_1() const { return ___m_InheritPosition_1; }
	inline bool* get_address_of_m_InheritPosition_1() { return &___m_InheritPosition_1; }
	inline void set_m_InheritPosition_1(bool value)
	{
		___m_InheritPosition_1 = value;
	}

	inline static int32_t get_offset_of_m_OnCameraLive_2() { return static_cast<int32_t>(offsetof(TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8, ___m_OnCameraLive_2)); }
	inline VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B * get_m_OnCameraLive_2() const { return ___m_OnCameraLive_2; }
	inline VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B ** get_address_of_m_OnCameraLive_2() { return &___m_OnCameraLive_2; }
	inline void set_m_OnCameraLive_2(VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B * value)
	{
		___m_OnCameraLive_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCameraLive_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Cinemachine.CinemachineVirtualCameraBase/TransitionParams
struct TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8_marshaled_pinvoke
{
	int32_t ___m_BlendHint_0;
	int32_t ___m_InheritPosition_1;
	VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B * ___m_OnCameraLive_2;
};
// Native definition for COM marshalling of Cinemachine.CinemachineVirtualCameraBase/TransitionParams
struct TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8_marshaled_com
{
	int32_t ___m_BlendHint_0;
	int32_t ___m_InheritPosition_1;
	VcamActivatedEvent_tD6FFF8FCC883ABD7CC27619CD451E6FE56DB4A3B * ___m_OnCameraLive_2;
};

// Cinemachine.DocumentationSortingAttribute
struct DocumentationSortingAttribute_tA3DD9DD04F77DEE0CF0FB7409F17C2A5A9920A87  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// Cinemachine.DocumentationSortingAttribute/Level Cinemachine.DocumentationSortingAttribute::<Category>k__BackingField
	int32_t ___U3CCategoryU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCategoryU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DocumentationSortingAttribute_tA3DD9DD04F77DEE0CF0FB7409F17C2A5A9920A87, ___U3CCategoryU3Ek__BackingField_0)); }
	inline int32_t get_U3CCategoryU3Ek__BackingField_0() const { return ___U3CCategoryU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CCategoryU3Ek__BackingField_0() { return &___U3CCategoryU3Ek__BackingField_0; }
	inline void set_U3CCategoryU3Ek__BackingField_0(int32_t value)
	{
		___U3CCategoryU3Ek__BackingField_0 = value;
	}
};


// Cinemachine.UpdateTracker/UpdateStatus
struct UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5  : public RuntimeObject
{
public:
	// System.Int32 Cinemachine.UpdateTracker/UpdateStatus::windowStart
	int32_t ___windowStart_1;
	// System.Int32 Cinemachine.UpdateTracker/UpdateStatus::numWindowLateUpdateMoves
	int32_t ___numWindowLateUpdateMoves_2;
	// System.Int32 Cinemachine.UpdateTracker/UpdateStatus::numWindowFixedUpdateMoves
	int32_t ___numWindowFixedUpdateMoves_3;
	// System.Int32 Cinemachine.UpdateTracker/UpdateStatus::numWindows
	int32_t ___numWindows_4;
	// System.Int32 Cinemachine.UpdateTracker/UpdateStatus::lastFrameUpdated
	int32_t ___lastFrameUpdated_5;
	// UnityEngine.Matrix4x4 Cinemachine.UpdateTracker/UpdateStatus::lastPos
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___lastPos_6;
	// Cinemachine.UpdateTracker/UpdateClock Cinemachine.UpdateTracker/UpdateStatus::<PreferredUpdate>k__BackingField
	int32_t ___U3CPreferredUpdateU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_windowStart_1() { return static_cast<int32_t>(offsetof(UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5, ___windowStart_1)); }
	inline int32_t get_windowStart_1() const { return ___windowStart_1; }
	inline int32_t* get_address_of_windowStart_1() { return &___windowStart_1; }
	inline void set_windowStart_1(int32_t value)
	{
		___windowStart_1 = value;
	}

	inline static int32_t get_offset_of_numWindowLateUpdateMoves_2() { return static_cast<int32_t>(offsetof(UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5, ___numWindowLateUpdateMoves_2)); }
	inline int32_t get_numWindowLateUpdateMoves_2() const { return ___numWindowLateUpdateMoves_2; }
	inline int32_t* get_address_of_numWindowLateUpdateMoves_2() { return &___numWindowLateUpdateMoves_2; }
	inline void set_numWindowLateUpdateMoves_2(int32_t value)
	{
		___numWindowLateUpdateMoves_2 = value;
	}

	inline static int32_t get_offset_of_numWindowFixedUpdateMoves_3() { return static_cast<int32_t>(offsetof(UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5, ___numWindowFixedUpdateMoves_3)); }
	inline int32_t get_numWindowFixedUpdateMoves_3() const { return ___numWindowFixedUpdateMoves_3; }
	inline int32_t* get_address_of_numWindowFixedUpdateMoves_3() { return &___numWindowFixedUpdateMoves_3; }
	inline void set_numWindowFixedUpdateMoves_3(int32_t value)
	{
		___numWindowFixedUpdateMoves_3 = value;
	}

	inline static int32_t get_offset_of_numWindows_4() { return static_cast<int32_t>(offsetof(UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5, ___numWindows_4)); }
	inline int32_t get_numWindows_4() const { return ___numWindows_4; }
	inline int32_t* get_address_of_numWindows_4() { return &___numWindows_4; }
	inline void set_numWindows_4(int32_t value)
	{
		___numWindows_4 = value;
	}

	inline static int32_t get_offset_of_lastFrameUpdated_5() { return static_cast<int32_t>(offsetof(UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5, ___lastFrameUpdated_5)); }
	inline int32_t get_lastFrameUpdated_5() const { return ___lastFrameUpdated_5; }
	inline int32_t* get_address_of_lastFrameUpdated_5() { return &___lastFrameUpdated_5; }
	inline void set_lastFrameUpdated_5(int32_t value)
	{
		___lastFrameUpdated_5 = value;
	}

	inline static int32_t get_offset_of_lastPos_6() { return static_cast<int32_t>(offsetof(UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5, ___lastPos_6)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_lastPos_6() const { return ___lastPos_6; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_lastPos_6() { return &___lastPos_6; }
	inline void set_lastPos_6(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___lastPos_6 = value;
	}

	inline static int32_t get_offset_of_U3CPreferredUpdateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5, ___U3CPreferredUpdateU3Ek__BackingField_7)); }
	inline int32_t get_U3CPreferredUpdateU3Ek__BackingField_7() const { return ___U3CPreferredUpdateU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CPreferredUpdateU3Ek__BackingField_7() { return &___U3CPreferredUpdateU3Ek__BackingField_7; }
	inline void set_U3CPreferredUpdateU3Ek__BackingField_7(int32_t value)
	{
		___U3CPreferredUpdateU3Ek__BackingField_7 = value;
	}
};


// CollectableConfiguration
struct CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210  : public RuntimeObject
{
public:
	// System.Boolean CollectableConfiguration::useFixedColor
	bool ___useFixedColor_0;
	// UnityEngine.Color CollectableConfiguration::fixedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___fixedColor_1;
	// CollectableType CollectableConfiguration::type
	int32_t ___type_2;
	// System.Int32 CollectableConfiguration::value
	int32_t ___value_3;

public:
	inline static int32_t get_offset_of_useFixedColor_0() { return static_cast<int32_t>(offsetof(CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210, ___useFixedColor_0)); }
	inline bool get_useFixedColor_0() const { return ___useFixedColor_0; }
	inline bool* get_address_of_useFixedColor_0() { return &___useFixedColor_0; }
	inline void set_useFixedColor_0(bool value)
	{
		___useFixedColor_0 = value;
	}

	inline static int32_t get_offset_of_fixedColor_1() { return static_cast<int32_t>(offsetof(CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210, ___fixedColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_fixedColor_1() const { return ___fixedColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_fixedColor_1() { return &___fixedColor_1; }
	inline void set_fixedColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___fixedColor_1 = value;
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210, ___value_3)); }
	inline int32_t get_value_3() const { return ___value_3; }
	inline int32_t* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(int32_t value)
	{
		___value_3 = value;
	}
};


// EnemyBehaviour/<SetNextTarget>d__8
struct U3CSetNextTargetU3Ed__8_tA8539BE570080CC8BD09B106B403201FF4864AC9 
{
public:
	// System.Int32 EnemyBehaviour/<SetNextTarget>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncVoidMethodBuilder EnemyBehaviour/<SetNextTarget>d__8::<>t__builder
	AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  ___U3CU3Et__builder_1;
	// EnemyBehaviour EnemyBehaviour/<SetNextTarget>d__8::<>4__this
	EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552 * ___U3CU3E4__this_2;
	// System.Runtime.CompilerServices.TaskAwaiter EnemyBehaviour/<SetNextTarget>d__8::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSetNextTargetU3Ed__8_tA8539BE570080CC8BD09B106B403201FF4864AC9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CSetNextTargetU3Ed__8_tA8539BE570080CC8BD09B106B403201FF4864AC9, ___U3CU3Et__builder_1)); }
	inline AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncVoidMethodBuilder_t44E3C9B52B019BB5BDCC0E1BB83188B536161CFF  value)
	{
		___U3CU3Et__builder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Et__builder_1))->___m_synchronizationContext_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Et__builder_1))->___m_task_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSetNextTargetU3Ed__8_tA8539BE570080CC8BD09B106B403201FF4864AC9, ___U3CU3E4__this_2)); }
	inline EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_3() { return static_cast<int32_t>(offsetof(U3CSetNextTargetU3Ed__8_tA8539BE570080CC8BD09B106B403201FF4864AC9, ___U3CU3Eu__1_3)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_3() const { return ___U3CU3Eu__1_3; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_3() { return &___U3CU3Eu__1_3; }
	inline void set_U3CU3Eu__1_3(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Eu__1_3))->___m_task_0), (void*)NULL);
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};

// Cinemachine.CinemachineImpulseDefinition
struct CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF  : public RuntimeObject
{
public:
	// System.Int32 Cinemachine.CinemachineImpulseDefinition::m_ImpulseChannel
	int32_t ___m_ImpulseChannel_0;
	// Cinemachine.SignalSourceAsset Cinemachine.CinemachineImpulseDefinition::m_RawSignal
	SignalSourceAsset_t543AC1ED7A03A17D509FDBD53782B72CC58F1140 * ___m_RawSignal_1;
	// System.Single Cinemachine.CinemachineImpulseDefinition::m_AmplitudeGain
	float ___m_AmplitudeGain_2;
	// System.Single Cinemachine.CinemachineImpulseDefinition::m_FrequencyGain
	float ___m_FrequencyGain_3;
	// Cinemachine.CinemachineImpulseDefinition/RepeatMode Cinemachine.CinemachineImpulseDefinition::m_RepeatMode
	int32_t ___m_RepeatMode_4;
	// System.Boolean Cinemachine.CinemachineImpulseDefinition::m_Randomize
	bool ___m_Randomize_5;
	// Cinemachine.CinemachineImpulseManager/EnvelopeDefinition Cinemachine.CinemachineImpulseDefinition::m_TimeEnvelope
	EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E  ___m_TimeEnvelope_6;
	// System.Single Cinemachine.CinemachineImpulseDefinition::m_ImpactRadius
	float ___m_ImpactRadius_7;
	// Cinemachine.CinemachineImpulseManager/ImpulseEvent/DirectionMode Cinemachine.CinemachineImpulseDefinition::m_DirectionMode
	int32_t ___m_DirectionMode_8;
	// Cinemachine.CinemachineImpulseManager/ImpulseEvent/DissipationMode Cinemachine.CinemachineImpulseDefinition::m_DissipationMode
	int32_t ___m_DissipationMode_9;
	// System.Single Cinemachine.CinemachineImpulseDefinition::m_DissipationDistance
	float ___m_DissipationDistance_10;

public:
	inline static int32_t get_offset_of_m_ImpulseChannel_0() { return static_cast<int32_t>(offsetof(CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF, ___m_ImpulseChannel_0)); }
	inline int32_t get_m_ImpulseChannel_0() const { return ___m_ImpulseChannel_0; }
	inline int32_t* get_address_of_m_ImpulseChannel_0() { return &___m_ImpulseChannel_0; }
	inline void set_m_ImpulseChannel_0(int32_t value)
	{
		___m_ImpulseChannel_0 = value;
	}

	inline static int32_t get_offset_of_m_RawSignal_1() { return static_cast<int32_t>(offsetof(CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF, ___m_RawSignal_1)); }
	inline SignalSourceAsset_t543AC1ED7A03A17D509FDBD53782B72CC58F1140 * get_m_RawSignal_1() const { return ___m_RawSignal_1; }
	inline SignalSourceAsset_t543AC1ED7A03A17D509FDBD53782B72CC58F1140 ** get_address_of_m_RawSignal_1() { return &___m_RawSignal_1; }
	inline void set_m_RawSignal_1(SignalSourceAsset_t543AC1ED7A03A17D509FDBD53782B72CC58F1140 * value)
	{
		___m_RawSignal_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RawSignal_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_AmplitudeGain_2() { return static_cast<int32_t>(offsetof(CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF, ___m_AmplitudeGain_2)); }
	inline float get_m_AmplitudeGain_2() const { return ___m_AmplitudeGain_2; }
	inline float* get_address_of_m_AmplitudeGain_2() { return &___m_AmplitudeGain_2; }
	inline void set_m_AmplitudeGain_2(float value)
	{
		___m_AmplitudeGain_2 = value;
	}

	inline static int32_t get_offset_of_m_FrequencyGain_3() { return static_cast<int32_t>(offsetof(CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF, ___m_FrequencyGain_3)); }
	inline float get_m_FrequencyGain_3() const { return ___m_FrequencyGain_3; }
	inline float* get_address_of_m_FrequencyGain_3() { return &___m_FrequencyGain_3; }
	inline void set_m_FrequencyGain_3(float value)
	{
		___m_FrequencyGain_3 = value;
	}

	inline static int32_t get_offset_of_m_RepeatMode_4() { return static_cast<int32_t>(offsetof(CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF, ___m_RepeatMode_4)); }
	inline int32_t get_m_RepeatMode_4() const { return ___m_RepeatMode_4; }
	inline int32_t* get_address_of_m_RepeatMode_4() { return &___m_RepeatMode_4; }
	inline void set_m_RepeatMode_4(int32_t value)
	{
		___m_RepeatMode_4 = value;
	}

	inline static int32_t get_offset_of_m_Randomize_5() { return static_cast<int32_t>(offsetof(CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF, ___m_Randomize_5)); }
	inline bool get_m_Randomize_5() const { return ___m_Randomize_5; }
	inline bool* get_address_of_m_Randomize_5() { return &___m_Randomize_5; }
	inline void set_m_Randomize_5(bool value)
	{
		___m_Randomize_5 = value;
	}

	inline static int32_t get_offset_of_m_TimeEnvelope_6() { return static_cast<int32_t>(offsetof(CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF, ___m_TimeEnvelope_6)); }
	inline EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E  get_m_TimeEnvelope_6() const { return ___m_TimeEnvelope_6; }
	inline EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E * get_address_of_m_TimeEnvelope_6() { return &___m_TimeEnvelope_6; }
	inline void set_m_TimeEnvelope_6(EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E  value)
	{
		___m_TimeEnvelope_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_TimeEnvelope_6))->___m_AttackShape_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_TimeEnvelope_6))->___m_DecayShape_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_ImpactRadius_7() { return static_cast<int32_t>(offsetof(CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF, ___m_ImpactRadius_7)); }
	inline float get_m_ImpactRadius_7() const { return ___m_ImpactRadius_7; }
	inline float* get_address_of_m_ImpactRadius_7() { return &___m_ImpactRadius_7; }
	inline void set_m_ImpactRadius_7(float value)
	{
		___m_ImpactRadius_7 = value;
	}

	inline static int32_t get_offset_of_m_DirectionMode_8() { return static_cast<int32_t>(offsetof(CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF, ___m_DirectionMode_8)); }
	inline int32_t get_m_DirectionMode_8() const { return ___m_DirectionMode_8; }
	inline int32_t* get_address_of_m_DirectionMode_8() { return &___m_DirectionMode_8; }
	inline void set_m_DirectionMode_8(int32_t value)
	{
		___m_DirectionMode_8 = value;
	}

	inline static int32_t get_offset_of_m_DissipationMode_9() { return static_cast<int32_t>(offsetof(CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF, ___m_DissipationMode_9)); }
	inline int32_t get_m_DissipationMode_9() const { return ___m_DissipationMode_9; }
	inline int32_t* get_address_of_m_DissipationMode_9() { return &___m_DissipationMode_9; }
	inline void set_m_DissipationMode_9(int32_t value)
	{
		___m_DissipationMode_9 = value;
	}

	inline static int32_t get_offset_of_m_DissipationDistance_10() { return static_cast<int32_t>(offsetof(CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF, ___m_DissipationDistance_10)); }
	inline float get_m_DissipationDistance_10() const { return ___m_DissipationDistance_10; }
	inline float* get_address_of_m_DissipationDistance_10() { return &___m_DissipationDistance_10; }
	inline void set_m_DissipationDistance_10(float value)
	{
		___m_DissipationDistance_10 = value;
	}
};


// Cinemachine.CinemachineImpulseManager/ImpulseEvent
struct ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C  : public RuntimeObject
{
public:
	// System.Single Cinemachine.CinemachineImpulseManager/ImpulseEvent::m_StartTime
	float ___m_StartTime_0;
	// Cinemachine.CinemachineImpulseManager/EnvelopeDefinition Cinemachine.CinemachineImpulseManager/ImpulseEvent::m_Envelope
	EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E  ___m_Envelope_1;
	// Cinemachine.ISignalSource6D Cinemachine.CinemachineImpulseManager/ImpulseEvent::m_SignalSource
	RuntimeObject* ___m_SignalSource_2;
	// UnityEngine.Vector3 Cinemachine.CinemachineImpulseManager/ImpulseEvent::m_Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Position_3;
	// System.Single Cinemachine.CinemachineImpulseManager/ImpulseEvent::m_Radius
	float ___m_Radius_4;
	// Cinemachine.CinemachineImpulseManager/ImpulseEvent/DirectionMode Cinemachine.CinemachineImpulseManager/ImpulseEvent::m_DirectionMode
	int32_t ___m_DirectionMode_5;
	// System.Int32 Cinemachine.CinemachineImpulseManager/ImpulseEvent::m_Channel
	int32_t ___m_Channel_6;
	// Cinemachine.CinemachineImpulseManager/ImpulseEvent/DissipationMode Cinemachine.CinemachineImpulseManager/ImpulseEvent::m_DissipationMode
	int32_t ___m_DissipationMode_7;
	// System.Single Cinemachine.CinemachineImpulseManager/ImpulseEvent::m_DissipationDistance
	float ___m_DissipationDistance_8;

public:
	inline static int32_t get_offset_of_m_StartTime_0() { return static_cast<int32_t>(offsetof(ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C, ___m_StartTime_0)); }
	inline float get_m_StartTime_0() const { return ___m_StartTime_0; }
	inline float* get_address_of_m_StartTime_0() { return &___m_StartTime_0; }
	inline void set_m_StartTime_0(float value)
	{
		___m_StartTime_0 = value;
	}

	inline static int32_t get_offset_of_m_Envelope_1() { return static_cast<int32_t>(offsetof(ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C, ___m_Envelope_1)); }
	inline EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E  get_m_Envelope_1() const { return ___m_Envelope_1; }
	inline EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E * get_address_of_m_Envelope_1() { return &___m_Envelope_1; }
	inline void set_m_Envelope_1(EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E  value)
	{
		___m_Envelope_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Envelope_1))->___m_AttackShape_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Envelope_1))->___m_DecayShape_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_SignalSource_2() { return static_cast<int32_t>(offsetof(ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C, ___m_SignalSource_2)); }
	inline RuntimeObject* get_m_SignalSource_2() const { return ___m_SignalSource_2; }
	inline RuntimeObject** get_address_of_m_SignalSource_2() { return &___m_SignalSource_2; }
	inline void set_m_SignalSource_2(RuntimeObject* value)
	{
		___m_SignalSource_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SignalSource_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Position_3() { return static_cast<int32_t>(offsetof(ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C, ___m_Position_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Position_3() const { return ___m_Position_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Position_3() { return &___m_Position_3; }
	inline void set_m_Position_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Position_3 = value;
	}

	inline static int32_t get_offset_of_m_Radius_4() { return static_cast<int32_t>(offsetof(ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C, ___m_Radius_4)); }
	inline float get_m_Radius_4() const { return ___m_Radius_4; }
	inline float* get_address_of_m_Radius_4() { return &___m_Radius_4; }
	inline void set_m_Radius_4(float value)
	{
		___m_Radius_4 = value;
	}

	inline static int32_t get_offset_of_m_DirectionMode_5() { return static_cast<int32_t>(offsetof(ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C, ___m_DirectionMode_5)); }
	inline int32_t get_m_DirectionMode_5() const { return ___m_DirectionMode_5; }
	inline int32_t* get_address_of_m_DirectionMode_5() { return &___m_DirectionMode_5; }
	inline void set_m_DirectionMode_5(int32_t value)
	{
		___m_DirectionMode_5 = value;
	}

	inline static int32_t get_offset_of_m_Channel_6() { return static_cast<int32_t>(offsetof(ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C, ___m_Channel_6)); }
	inline int32_t get_m_Channel_6() const { return ___m_Channel_6; }
	inline int32_t* get_address_of_m_Channel_6() { return &___m_Channel_6; }
	inline void set_m_Channel_6(int32_t value)
	{
		___m_Channel_6 = value;
	}

	inline static int32_t get_offset_of_m_DissipationMode_7() { return static_cast<int32_t>(offsetof(ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C, ___m_DissipationMode_7)); }
	inline int32_t get_m_DissipationMode_7() const { return ___m_DissipationMode_7; }
	inline int32_t* get_address_of_m_DissipationMode_7() { return &___m_DissipationMode_7; }
	inline void set_m_DissipationMode_7(int32_t value)
	{
		___m_DissipationMode_7 = value;
	}

	inline static int32_t get_offset_of_m_DissipationDistance_8() { return static_cast<int32_t>(offsetof(ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C, ___m_DissipationDistance_8)); }
	inline float get_m_DissipationDistance_8() const { return ___m_DissipationDistance_8; }
	inline float* get_address_of_m_DissipationDistance_8() { return &___m_DissipationDistance_8; }
	inline void set_m_DissipationDistance_8(float value)
	{
		___m_DissipationDistance_8 = value;
	}
};


// Cinemachine.SignalSourceAsset
struct SignalSourceAsset_t543AC1ED7A03A17D509FDBD53782B72CC58F1140  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:

public:
};


// Cinemachine.Utility.CinemachineDebug/OnGUIDelegate
struct OnGUIDelegate_tF9317001AB4E703C8439EFD7987552E90C476F72  : public MulticastDelegate_t
{
public:

public:
};


// CollectableData
struct CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// CollectableConfiguration[] CollectableData::collectableCoin
	CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A* ___collectableCoin_4;
	// CollectableConfiguration[] CollectableData::collectablePowerUp
	CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A* ___collectablePowerUp_5;
	// CollectableConfiguration[] CollectableData::collectableHealth
	CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A* ___collectableHealth_6;
	// CollectableConfiguration[] CollectableData::collectableMunition
	CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A* ___collectableMunition_7;
	// UnityEngine.Color[] CollectableData::colorPossibilities
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___colorPossibilities_8;

public:
	inline static int32_t get_offset_of_collectableCoin_4() { return static_cast<int32_t>(offsetof(CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7, ___collectableCoin_4)); }
	inline CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A* get_collectableCoin_4() const { return ___collectableCoin_4; }
	inline CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A** get_address_of_collectableCoin_4() { return &___collectableCoin_4; }
	inline void set_collectableCoin_4(CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A* value)
	{
		___collectableCoin_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collectableCoin_4), (void*)value);
	}

	inline static int32_t get_offset_of_collectablePowerUp_5() { return static_cast<int32_t>(offsetof(CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7, ___collectablePowerUp_5)); }
	inline CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A* get_collectablePowerUp_5() const { return ___collectablePowerUp_5; }
	inline CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A** get_address_of_collectablePowerUp_5() { return &___collectablePowerUp_5; }
	inline void set_collectablePowerUp_5(CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A* value)
	{
		___collectablePowerUp_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collectablePowerUp_5), (void*)value);
	}

	inline static int32_t get_offset_of_collectableHealth_6() { return static_cast<int32_t>(offsetof(CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7, ___collectableHealth_6)); }
	inline CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A* get_collectableHealth_6() const { return ___collectableHealth_6; }
	inline CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A** get_address_of_collectableHealth_6() { return &___collectableHealth_6; }
	inline void set_collectableHealth_6(CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A* value)
	{
		___collectableHealth_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collectableHealth_6), (void*)value);
	}

	inline static int32_t get_offset_of_collectableMunition_7() { return static_cast<int32_t>(offsetof(CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7, ___collectableMunition_7)); }
	inline CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A* get_collectableMunition_7() const { return ___collectableMunition_7; }
	inline CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A** get_address_of_collectableMunition_7() { return &___collectableMunition_7; }
	inline void set_collectableMunition_7(CollectableConfigurationU5BU5D_t6AF5D28CD3553EB174399776803C1C48B458931A* value)
	{
		___collectableMunition_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collectableMunition_7), (void*)value);
	}

	inline static int32_t get_offset_of_colorPossibilities_8() { return static_cast<int32_t>(offsetof(CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7, ___colorPossibilities_8)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_colorPossibilities_8() const { return ___colorPossibilities_8; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_colorPossibilities_8() { return &___colorPossibilities_8; }
	inline void set_colorPossibilities_8(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___colorPossibilities_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorPossibilities_8), (void*)value);
	}
};


// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// Cinemachine.CinemachineFixedSignal
struct CinemachineFixedSignal_t74D185C6DC7771E44E97DB9EB33FB2914659E0B3  : public SignalSourceAsset_t543AC1ED7A03A17D509FDBD53782B72CC58F1140
{
public:
	// UnityEngine.AnimationCurve Cinemachine.CinemachineFixedSignal::m_XCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_XCurve_4;
	// UnityEngine.AnimationCurve Cinemachine.CinemachineFixedSignal::m_YCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_YCurve_5;
	// UnityEngine.AnimationCurve Cinemachine.CinemachineFixedSignal::m_ZCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___m_ZCurve_6;

public:
	inline static int32_t get_offset_of_m_XCurve_4() { return static_cast<int32_t>(offsetof(CinemachineFixedSignal_t74D185C6DC7771E44E97DB9EB33FB2914659E0B3, ___m_XCurve_4)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_XCurve_4() const { return ___m_XCurve_4; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_XCurve_4() { return &___m_XCurve_4; }
	inline void set_m_XCurve_4(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_XCurve_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_XCurve_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_YCurve_5() { return static_cast<int32_t>(offsetof(CinemachineFixedSignal_t74D185C6DC7771E44E97DB9EB33FB2914659E0B3, ___m_YCurve_5)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_YCurve_5() const { return ___m_YCurve_5; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_YCurve_5() { return &___m_YCurve_5; }
	inline void set_m_YCurve_5(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_YCurve_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_YCurve_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_ZCurve_6() { return static_cast<int32_t>(offsetof(CinemachineFixedSignal_t74D185C6DC7771E44E97DB9EB33FB2914659E0B3, ___m_ZCurve_6)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_m_ZCurve_6() const { return ___m_ZCurve_6; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_m_ZCurve_6() { return &___m_ZCurve_6; }
	inline void set_m_ZCurve_6(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___m_ZCurve_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ZCurve_6), (void*)value);
	}
};


// Cinemachine.NoiseSettings
struct NoiseSettings_t2F4032D427ECE5DF595F6E6BFC3881368E798EC5  : public SignalSourceAsset_t543AC1ED7A03A17D509FDBD53782B72CC58F1140
{
public:
	// Cinemachine.NoiseSettings/TransformNoiseParams[] Cinemachine.NoiseSettings::PositionNoise
	TransformNoiseParamsU5BU5D_tA03937735BC5D12BD3AF6572C92C7AE69F011E9A* ___PositionNoise_4;
	// Cinemachine.NoiseSettings/TransformNoiseParams[] Cinemachine.NoiseSettings::OrientationNoise
	TransformNoiseParamsU5BU5D_tA03937735BC5D12BD3AF6572C92C7AE69F011E9A* ___OrientationNoise_5;

public:
	inline static int32_t get_offset_of_PositionNoise_4() { return static_cast<int32_t>(offsetof(NoiseSettings_t2F4032D427ECE5DF595F6E6BFC3881368E798EC5, ___PositionNoise_4)); }
	inline TransformNoiseParamsU5BU5D_tA03937735BC5D12BD3AF6572C92C7AE69F011E9A* get_PositionNoise_4() const { return ___PositionNoise_4; }
	inline TransformNoiseParamsU5BU5D_tA03937735BC5D12BD3AF6572C92C7AE69F011E9A** get_address_of_PositionNoise_4() { return &___PositionNoise_4; }
	inline void set_PositionNoise_4(TransformNoiseParamsU5BU5D_tA03937735BC5D12BD3AF6572C92C7AE69F011E9A* value)
	{
		___PositionNoise_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PositionNoise_4), (void*)value);
	}

	inline static int32_t get_offset_of_OrientationNoise_5() { return static_cast<int32_t>(offsetof(NoiseSettings_t2F4032D427ECE5DF595F6E6BFC3881368E798EC5, ___OrientationNoise_5)); }
	inline TransformNoiseParamsU5BU5D_tA03937735BC5D12BD3AF6572C92C7AE69F011E9A* get_OrientationNoise_5() const { return ___OrientationNoise_5; }
	inline TransformNoiseParamsU5BU5D_tA03937735BC5D12BD3AF6572C92C7AE69F011E9A** get_address_of_OrientationNoise_5() { return &___OrientationNoise_5; }
	inline void set_OrientationNoise_5(TransformNoiseParamsU5BU5D_tA03937735BC5D12BD3AF6572C92C7AE69F011E9A* value)
	{
		___OrientationNoise_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OrientationNoise_5), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// CameraFollow
struct CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform[] CameraFollow::points
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ___points_4;
	// System.Int32 CameraFollow::cameraVelocity
	int32_t ___cameraVelocity_5;
	// System.Int32 CameraFollow::current
	int32_t ___current_6;

public:
	inline static int32_t get_offset_of_points_4() { return static_cast<int32_t>(offsetof(CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142, ___points_4)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get_points_4() const { return ___points_4; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of_points_4() { return &___points_4; }
	inline void set_points_4(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		___points_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___points_4), (void*)value);
	}

	inline static int32_t get_offset_of_cameraVelocity_5() { return static_cast<int32_t>(offsetof(CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142, ___cameraVelocity_5)); }
	inline int32_t get_cameraVelocity_5() const { return ___cameraVelocity_5; }
	inline int32_t* get_address_of_cameraVelocity_5() { return &___cameraVelocity_5; }
	inline void set_cameraVelocity_5(int32_t value)
	{
		___cameraVelocity_5 = value;
	}

	inline static int32_t get_offset_of_current_6() { return static_cast<int32_t>(offsetof(CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142, ___current_6)); }
	inline int32_t get_current_6() const { return ___current_6; }
	inline int32_t* get_address_of_current_6() { return &___current_6; }
	inline void set_current_6(int32_t value)
	{
		___current_6 = value;
	}
};


// CharacterBase
struct CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Rigidbody CharacterBase::characterRB
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___characterRB_4;
	// UnityEngine.Renderer CharacterBase::characterRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___characterRenderer_5;
	// UnityEngine.Animator CharacterBase::characterAnimator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___characterAnimator_6;
	// System.Single CharacterBase::velocity
	float ___velocity_7;
	// System.Single CharacterBase::grabVelocity
	float ___grabVelocity_8;
	// System.Boolean CharacterBase::duringMoviment
	bool ___duringMoviment_9;

public:
	inline static int32_t get_offset_of_characterRB_4() { return static_cast<int32_t>(offsetof(CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039, ___characterRB_4)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_characterRB_4() const { return ___characterRB_4; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_characterRB_4() { return &___characterRB_4; }
	inline void set_characterRB_4(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___characterRB_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___characterRB_4), (void*)value);
	}

	inline static int32_t get_offset_of_characterRenderer_5() { return static_cast<int32_t>(offsetof(CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039, ___characterRenderer_5)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_characterRenderer_5() const { return ___characterRenderer_5; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_characterRenderer_5() { return &___characterRenderer_5; }
	inline void set_characterRenderer_5(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___characterRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___characterRenderer_5), (void*)value);
	}

	inline static int32_t get_offset_of_characterAnimator_6() { return static_cast<int32_t>(offsetof(CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039, ___characterAnimator_6)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_characterAnimator_6() const { return ___characterAnimator_6; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_characterAnimator_6() { return &___characterAnimator_6; }
	inline void set_characterAnimator_6(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___characterAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___characterAnimator_6), (void*)value);
	}

	inline static int32_t get_offset_of_velocity_7() { return static_cast<int32_t>(offsetof(CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039, ___velocity_7)); }
	inline float get_velocity_7() const { return ___velocity_7; }
	inline float* get_address_of_velocity_7() { return &___velocity_7; }
	inline void set_velocity_7(float value)
	{
		___velocity_7 = value;
	}

	inline static int32_t get_offset_of_grabVelocity_8() { return static_cast<int32_t>(offsetof(CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039, ___grabVelocity_8)); }
	inline float get_grabVelocity_8() const { return ___grabVelocity_8; }
	inline float* get_address_of_grabVelocity_8() { return &___grabVelocity_8; }
	inline void set_grabVelocity_8(float value)
	{
		___grabVelocity_8 = value;
	}

	inline static int32_t get_offset_of_duringMoviment_9() { return static_cast<int32_t>(offsetof(CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039, ___duringMoviment_9)); }
	inline bool get_duringMoviment_9() const { return ___duringMoviment_9; }
	inline bool* get_address_of_duringMoviment_9() { return &___duringMoviment_9; }
	inline void set_duringMoviment_9(bool value)
	{
		___duringMoviment_9 = value;
	}
};


// Cinemachine.CinemachineExtension
struct CinemachineExtension_t397048E40D5EB90A0A06031DCF9E1B52306D06AE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineExtension::m_vcamOwner
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * ___m_vcamOwner_5;
	// System.Collections.Generic.Dictionary`2<Cinemachine.ICinemachineCamera,System.Object> Cinemachine.CinemachineExtension::mExtraState
	Dictionary_2_t2DA729FA5DE6343A62FDA39E2B5A6EA153BB03A8 * ___mExtraState_6;

public:
	inline static int32_t get_offset_of_m_vcamOwner_5() { return static_cast<int32_t>(offsetof(CinemachineExtension_t397048E40D5EB90A0A06031DCF9E1B52306D06AE, ___m_vcamOwner_5)); }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * get_m_vcamOwner_5() const { return ___m_vcamOwner_5; }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD ** get_address_of_m_vcamOwner_5() { return &___m_vcamOwner_5; }
	inline void set_m_vcamOwner_5(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * value)
	{
		___m_vcamOwner_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_vcamOwner_5), (void*)value);
	}

	inline static int32_t get_offset_of_mExtraState_6() { return static_cast<int32_t>(offsetof(CinemachineExtension_t397048E40D5EB90A0A06031DCF9E1B52306D06AE, ___mExtraState_6)); }
	inline Dictionary_2_t2DA729FA5DE6343A62FDA39E2B5A6EA153BB03A8 * get_mExtraState_6() const { return ___mExtraState_6; }
	inline Dictionary_2_t2DA729FA5DE6343A62FDA39E2B5A6EA153BB03A8 ** get_address_of_mExtraState_6() { return &___mExtraState_6; }
	inline void set_mExtraState_6(Dictionary_2_t2DA729FA5DE6343A62FDA39E2B5A6EA153BB03A8 * value)
	{
		___mExtraState_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mExtraState_6), (void*)value);
	}
};


// Cinemachine.CinemachineImpulseSource
struct CinemachineImpulseSource_t0939CD50852A448FDE8E55DDB3050358180B10ED  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Cinemachine.CinemachineImpulseDefinition Cinemachine.CinemachineImpulseSource::m_ImpulseDefinition
	CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF * ___m_ImpulseDefinition_4;

public:
	inline static int32_t get_offset_of_m_ImpulseDefinition_4() { return static_cast<int32_t>(offsetof(CinemachineImpulseSource_t0939CD50852A448FDE8E55DDB3050358180B10ED, ___m_ImpulseDefinition_4)); }
	inline CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF * get_m_ImpulseDefinition_4() const { return ___m_ImpulseDefinition_4; }
	inline CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF ** get_address_of_m_ImpulseDefinition_4() { return &___m_ImpulseDefinition_4; }
	inline void set_m_ImpulseDefinition_4(CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF * value)
	{
		___m_ImpulseDefinition_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ImpulseDefinition_4), (void*)value);
	}
};


// Cinemachine.CinemachineTriggerAction
struct CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.LayerMask Cinemachine.CinemachineTriggerAction::m_LayerMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___m_LayerMask_4;
	// System.String Cinemachine.CinemachineTriggerAction::m_WithTag
	String_t* ___m_WithTag_5;
	// System.String Cinemachine.CinemachineTriggerAction::m_WithoutTag
	String_t* ___m_WithoutTag_6;
	// System.Int32 Cinemachine.CinemachineTriggerAction::m_SkipFirst
	int32_t ___m_SkipFirst_7;
	// System.Boolean Cinemachine.CinemachineTriggerAction::m_Repeating
	bool ___m_Repeating_8;
	// Cinemachine.CinemachineTriggerAction/ActionSettings Cinemachine.CinemachineTriggerAction::m_OnObjectEnter
	ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2  ___m_OnObjectEnter_9;
	// Cinemachine.CinemachineTriggerAction/ActionSettings Cinemachine.CinemachineTriggerAction::m_OnObjectExit
	ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2  ___m_OnObjectExit_10;
	// System.Collections.Generic.HashSet`1<UnityEngine.GameObject> Cinemachine.CinemachineTriggerAction::m_ActiveTriggerObjects
	HashSet_1_tE5720030A8BA061DA65ABBF4C5150D8D84BD3169 * ___m_ActiveTriggerObjects_11;

public:
	inline static int32_t get_offset_of_m_LayerMask_4() { return static_cast<int32_t>(offsetof(CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071, ___m_LayerMask_4)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_m_LayerMask_4() const { return ___m_LayerMask_4; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_m_LayerMask_4() { return &___m_LayerMask_4; }
	inline void set_m_LayerMask_4(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___m_LayerMask_4 = value;
	}

	inline static int32_t get_offset_of_m_WithTag_5() { return static_cast<int32_t>(offsetof(CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071, ___m_WithTag_5)); }
	inline String_t* get_m_WithTag_5() const { return ___m_WithTag_5; }
	inline String_t** get_address_of_m_WithTag_5() { return &___m_WithTag_5; }
	inline void set_m_WithTag_5(String_t* value)
	{
		___m_WithTag_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_WithTag_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_WithoutTag_6() { return static_cast<int32_t>(offsetof(CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071, ___m_WithoutTag_6)); }
	inline String_t* get_m_WithoutTag_6() const { return ___m_WithoutTag_6; }
	inline String_t** get_address_of_m_WithoutTag_6() { return &___m_WithoutTag_6; }
	inline void set_m_WithoutTag_6(String_t* value)
	{
		___m_WithoutTag_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_WithoutTag_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_SkipFirst_7() { return static_cast<int32_t>(offsetof(CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071, ___m_SkipFirst_7)); }
	inline int32_t get_m_SkipFirst_7() const { return ___m_SkipFirst_7; }
	inline int32_t* get_address_of_m_SkipFirst_7() { return &___m_SkipFirst_7; }
	inline void set_m_SkipFirst_7(int32_t value)
	{
		___m_SkipFirst_7 = value;
	}

	inline static int32_t get_offset_of_m_Repeating_8() { return static_cast<int32_t>(offsetof(CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071, ___m_Repeating_8)); }
	inline bool get_m_Repeating_8() const { return ___m_Repeating_8; }
	inline bool* get_address_of_m_Repeating_8() { return &___m_Repeating_8; }
	inline void set_m_Repeating_8(bool value)
	{
		___m_Repeating_8 = value;
	}

	inline static int32_t get_offset_of_m_OnObjectEnter_9() { return static_cast<int32_t>(offsetof(CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071, ___m_OnObjectEnter_9)); }
	inline ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2  get_m_OnObjectEnter_9() const { return ___m_OnObjectEnter_9; }
	inline ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2 * get_address_of_m_OnObjectEnter_9() { return &___m_OnObjectEnter_9; }
	inline void set_m_OnObjectEnter_9(ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2  value)
	{
		___m_OnObjectEnter_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_OnObjectEnter_9))->___m_Target_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_OnObjectEnter_9))->___m_Event_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_OnObjectExit_10() { return static_cast<int32_t>(offsetof(CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071, ___m_OnObjectExit_10)); }
	inline ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2  get_m_OnObjectExit_10() const { return ___m_OnObjectExit_10; }
	inline ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2 * get_address_of_m_OnObjectExit_10() { return &___m_OnObjectExit_10; }
	inline void set_m_OnObjectExit_10(ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2  value)
	{
		___m_OnObjectExit_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_OnObjectExit_10))->___m_Target_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_OnObjectExit_10))->___m_Event_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_ActiveTriggerObjects_11() { return static_cast<int32_t>(offsetof(CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071, ___m_ActiveTriggerObjects_11)); }
	inline HashSet_1_tE5720030A8BA061DA65ABBF4C5150D8D84BD3169 * get_m_ActiveTriggerObjects_11() const { return ___m_ActiveTriggerObjects_11; }
	inline HashSet_1_tE5720030A8BA061DA65ABBF4C5150D8D84BD3169 ** get_address_of_m_ActiveTriggerObjects_11() { return &___m_ActiveTriggerObjects_11; }
	inline void set_m_ActiveTriggerObjects_11(HashSet_1_tE5720030A8BA061DA65ABBF4C5150D8D84BD3169 * value)
	{
		___m_ActiveTriggerObjects_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActiveTriggerObjects_11), (void*)value);
	}
};


// Cinemachine.CinemachineVirtualCameraBase
struct CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String[] Cinemachine.CinemachineVirtualCameraBase::m_ExcludedPropertiesInInspector
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_ExcludedPropertiesInInspector_4;
	// Cinemachine.CinemachineCore/Stage[] Cinemachine.CinemachineVirtualCameraBase::m_LockStageInInspector
	StageU5BU5D_tC107E846F8ED5DBF4CEF77B4AFCC26D9DBE723A2* ___m_LockStageInInspector_5;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_ValidatingStreamVersion
	int32_t ___m_ValidatingStreamVersion_6;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::m_OnValidateCalled
	bool ___m_OnValidateCalled_7;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_StreamingVersion
	int32_t ___m_StreamingVersion_8;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_Priority
	int32_t ___m_Priority_9;
	// Cinemachine.CinemachineVirtualCameraBase/StandbyUpdateMode Cinemachine.CinemachineVirtualCameraBase::m_StandbyUpdate
	int32_t ___m_StandbyUpdate_10;
	// System.Collections.Generic.List`1<Cinemachine.CinemachineExtension> Cinemachine.CinemachineVirtualCameraBase::mExtensions
	List_1_tB06968E265CE1689803547CD89D453ACFB3509D5 * ___mExtensions_11;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::m_previousStateIsValid
	bool ___m_previousStateIsValid_12;
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCameraBase::m_previousLookAtTarget
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_previousLookAtTarget_13;
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCameraBase::m_previousFollowTarget
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_previousFollowTarget_14;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::mSlaveStatusUpdated
	bool ___mSlaveStatusUpdated_15;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineVirtualCameraBase::m_parentVcam
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * ___m_parentVcam_16;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_QueuePriority
	int32_t ___m_QueuePriority_17;

public:
	inline static int32_t get_offset_of_m_ExcludedPropertiesInInspector_4() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_ExcludedPropertiesInInspector_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_ExcludedPropertiesInInspector_4() const { return ___m_ExcludedPropertiesInInspector_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_ExcludedPropertiesInInspector_4() { return &___m_ExcludedPropertiesInInspector_4; }
	inline void set_m_ExcludedPropertiesInInspector_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_ExcludedPropertiesInInspector_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ExcludedPropertiesInInspector_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_LockStageInInspector_5() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_LockStageInInspector_5)); }
	inline StageU5BU5D_tC107E846F8ED5DBF4CEF77B4AFCC26D9DBE723A2* get_m_LockStageInInspector_5() const { return ___m_LockStageInInspector_5; }
	inline StageU5BU5D_tC107E846F8ED5DBF4CEF77B4AFCC26D9DBE723A2** get_address_of_m_LockStageInInspector_5() { return &___m_LockStageInInspector_5; }
	inline void set_m_LockStageInInspector_5(StageU5BU5D_tC107E846F8ED5DBF4CEF77B4AFCC26D9DBE723A2* value)
	{
		___m_LockStageInInspector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LockStageInInspector_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_ValidatingStreamVersion_6() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_ValidatingStreamVersion_6)); }
	inline int32_t get_m_ValidatingStreamVersion_6() const { return ___m_ValidatingStreamVersion_6; }
	inline int32_t* get_address_of_m_ValidatingStreamVersion_6() { return &___m_ValidatingStreamVersion_6; }
	inline void set_m_ValidatingStreamVersion_6(int32_t value)
	{
		___m_ValidatingStreamVersion_6 = value;
	}

	inline static int32_t get_offset_of_m_OnValidateCalled_7() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_OnValidateCalled_7)); }
	inline bool get_m_OnValidateCalled_7() const { return ___m_OnValidateCalled_7; }
	inline bool* get_address_of_m_OnValidateCalled_7() { return &___m_OnValidateCalled_7; }
	inline void set_m_OnValidateCalled_7(bool value)
	{
		___m_OnValidateCalled_7 = value;
	}

	inline static int32_t get_offset_of_m_StreamingVersion_8() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_StreamingVersion_8)); }
	inline int32_t get_m_StreamingVersion_8() const { return ___m_StreamingVersion_8; }
	inline int32_t* get_address_of_m_StreamingVersion_8() { return &___m_StreamingVersion_8; }
	inline void set_m_StreamingVersion_8(int32_t value)
	{
		___m_StreamingVersion_8 = value;
	}

	inline static int32_t get_offset_of_m_Priority_9() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_Priority_9)); }
	inline int32_t get_m_Priority_9() const { return ___m_Priority_9; }
	inline int32_t* get_address_of_m_Priority_9() { return &___m_Priority_9; }
	inline void set_m_Priority_9(int32_t value)
	{
		___m_Priority_9 = value;
	}

	inline static int32_t get_offset_of_m_StandbyUpdate_10() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_StandbyUpdate_10)); }
	inline int32_t get_m_StandbyUpdate_10() const { return ___m_StandbyUpdate_10; }
	inline int32_t* get_address_of_m_StandbyUpdate_10() { return &___m_StandbyUpdate_10; }
	inline void set_m_StandbyUpdate_10(int32_t value)
	{
		___m_StandbyUpdate_10 = value;
	}

	inline static int32_t get_offset_of_mExtensions_11() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___mExtensions_11)); }
	inline List_1_tB06968E265CE1689803547CD89D453ACFB3509D5 * get_mExtensions_11() const { return ___mExtensions_11; }
	inline List_1_tB06968E265CE1689803547CD89D453ACFB3509D5 ** get_address_of_mExtensions_11() { return &___mExtensions_11; }
	inline void set_mExtensions_11(List_1_tB06968E265CE1689803547CD89D453ACFB3509D5 * value)
	{
		___mExtensions_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mExtensions_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_previousStateIsValid_12() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_previousStateIsValid_12)); }
	inline bool get_m_previousStateIsValid_12() const { return ___m_previousStateIsValid_12; }
	inline bool* get_address_of_m_previousStateIsValid_12() { return &___m_previousStateIsValid_12; }
	inline void set_m_previousStateIsValid_12(bool value)
	{
		___m_previousStateIsValid_12 = value;
	}

	inline static int32_t get_offset_of_m_previousLookAtTarget_13() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_previousLookAtTarget_13)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_previousLookAtTarget_13() const { return ___m_previousLookAtTarget_13; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_previousLookAtTarget_13() { return &___m_previousLookAtTarget_13; }
	inline void set_m_previousLookAtTarget_13(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_previousLookAtTarget_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_previousLookAtTarget_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_previousFollowTarget_14() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_previousFollowTarget_14)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_previousFollowTarget_14() const { return ___m_previousFollowTarget_14; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_previousFollowTarget_14() { return &___m_previousFollowTarget_14; }
	inline void set_m_previousFollowTarget_14(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_previousFollowTarget_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_previousFollowTarget_14), (void*)value);
	}

	inline static int32_t get_offset_of_mSlaveStatusUpdated_15() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___mSlaveStatusUpdated_15)); }
	inline bool get_mSlaveStatusUpdated_15() const { return ___mSlaveStatusUpdated_15; }
	inline bool* get_address_of_mSlaveStatusUpdated_15() { return &___mSlaveStatusUpdated_15; }
	inline void set_mSlaveStatusUpdated_15(bool value)
	{
		___mSlaveStatusUpdated_15 = value;
	}

	inline static int32_t get_offset_of_m_parentVcam_16() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_parentVcam_16)); }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * get_m_parentVcam_16() const { return ___m_parentVcam_16; }
	inline CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD ** get_address_of_m_parentVcam_16() { return &___m_parentVcam_16; }
	inline void set_m_parentVcam_16(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD * value)
	{
		___m_parentVcam_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_parentVcam_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_QueuePriority_17() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD, ___m_QueuePriority_17)); }
	inline int32_t get_m_QueuePriority_17() const { return ___m_QueuePriority_17; }
	inline int32_t* get_address_of_m_QueuePriority_17() { return &___m_QueuePriority_17; }
	inline void set_m_QueuePriority_17(int32_t value)
	{
		___m_QueuePriority_17 = value;
	}
};


// CoinUpdater
struct CoinUpdater_t8B38B35C3D29397BA3F025A713AB6D953621A7ED  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct CoinUpdater_t8B38B35C3D29397BA3F025A713AB6D953621A7ED_StaticFields
{
public:
	// TMPro.TextMeshProUGUI CoinUpdater::coinTMP
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___coinTMP_4;

public:
	inline static int32_t get_offset_of_coinTMP_4() { return static_cast<int32_t>(offsetof(CoinUpdater_t8B38B35C3D29397BA3F025A713AB6D953621A7ED_StaticFields, ___coinTMP_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_coinTMP_4() const { return ___coinTMP_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_coinTMP_4() { return &___coinTMP_4; }
	inline void set_coinTMP_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___coinTMP_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coinTMP_4), (void*)value);
	}
};


// Collectable
struct Collectable_tB9D507F84CB6880BA5EE1A6A2589203264F29651  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 Collectable::id
	int32_t ___id_4;
	// CollectableType Collectable::type
	int32_t ___type_5;
	// UnityEngine.Renderer Collectable::collectableRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___collectableRenderer_6;
	// CollectableConfiguration Collectable::information
	CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210 * ___information_7;
	// UnityEngine.MaterialPropertyBlock Collectable::propertyBlock
	MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * ___propertyBlock_8;

public:
	inline static int32_t get_offset_of_id_4() { return static_cast<int32_t>(offsetof(Collectable_tB9D507F84CB6880BA5EE1A6A2589203264F29651, ___id_4)); }
	inline int32_t get_id_4() const { return ___id_4; }
	inline int32_t* get_address_of_id_4() { return &___id_4; }
	inline void set_id_4(int32_t value)
	{
		___id_4 = value;
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(Collectable_tB9D507F84CB6880BA5EE1A6A2589203264F29651, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}

	inline static int32_t get_offset_of_collectableRenderer_6() { return static_cast<int32_t>(offsetof(Collectable_tB9D507F84CB6880BA5EE1A6A2589203264F29651, ___collectableRenderer_6)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_collectableRenderer_6() const { return ___collectableRenderer_6; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_collectableRenderer_6() { return &___collectableRenderer_6; }
	inline void set_collectableRenderer_6(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___collectableRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collectableRenderer_6), (void*)value);
	}

	inline static int32_t get_offset_of_information_7() { return static_cast<int32_t>(offsetof(Collectable_tB9D507F84CB6880BA5EE1A6A2589203264F29651, ___information_7)); }
	inline CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210 * get_information_7() const { return ___information_7; }
	inline CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210 ** get_address_of_information_7() { return &___information_7; }
	inline void set_information_7(CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210 * value)
	{
		___information_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___information_7), (void*)value);
	}

	inline static int32_t get_offset_of_propertyBlock_8() { return static_cast<int32_t>(offsetof(Collectable_tB9D507F84CB6880BA5EE1A6A2589203264F29651, ___propertyBlock_8)); }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * get_propertyBlock_8() const { return ___propertyBlock_8; }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 ** get_address_of_propertyBlock_8() { return &___propertyBlock_8; }
	inline void set_propertyBlock_8(MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * value)
	{
		___propertyBlock_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___propertyBlock_8), (void*)value);
	}
};


// EndGameScreen
struct EndGameScreen_tA1957EC7E20256B5515F2ABF8D752AD582C297FF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TextMeshProUGUI EndGameScreen::messageTxt
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___messageTxt_4;
	// System.Boolean EndGameScreen::sucessTemp
	bool ___sucessTemp_5;

public:
	inline static int32_t get_offset_of_messageTxt_4() { return static_cast<int32_t>(offsetof(EndGameScreen_tA1957EC7E20256B5515F2ABF8D752AD582C297FF, ___messageTxt_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_messageTxt_4() const { return ___messageTxt_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_messageTxt_4() { return &___messageTxt_4; }
	inline void set_messageTxt_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___messageTxt_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___messageTxt_4), (void*)value);
	}

	inline static int32_t get_offset_of_sucessTemp_5() { return static_cast<int32_t>(offsetof(EndGameScreen_tA1957EC7E20256B5515F2ABF8D752AD582C297FF, ___sucessTemp_5)); }
	inline bool get_sucessTemp_5() const { return ___sucessTemp_5; }
	inline bool* get_address_of_sucessTemp_5() { return &___sucessTemp_5; }
	inline void set_sucessTemp_5(bool value)
	{
		___sucessTemp_5 = value;
	}
};


// InteractableBase
struct InteractableBase_t5E0859747DB47E62279D12B7A14E157EE5DF9A08  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Rigidbody InteractableBase::rigidBody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___rigidBody_4;
	// System.String InteractableBase::interactableLayer
	String_t* ___interactableLayer_5;
	// System.Boolean InteractableBase::reachDestiny
	bool ___reachDestiny_6;

public:
	inline static int32_t get_offset_of_rigidBody_4() { return static_cast<int32_t>(offsetof(InteractableBase_t5E0859747DB47E62279D12B7A14E157EE5DF9A08, ___rigidBody_4)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_rigidBody_4() const { return ___rigidBody_4; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_rigidBody_4() { return &___rigidBody_4; }
	inline void set_rigidBody_4(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___rigidBody_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rigidBody_4), (void*)value);
	}

	inline static int32_t get_offset_of_interactableLayer_5() { return static_cast<int32_t>(offsetof(InteractableBase_t5E0859747DB47E62279D12B7A14E157EE5DF9A08, ___interactableLayer_5)); }
	inline String_t* get_interactableLayer_5() const { return ___interactableLayer_5; }
	inline String_t** get_address_of_interactableLayer_5() { return &___interactableLayer_5; }
	inline void set_interactableLayer_5(String_t* value)
	{
		___interactableLayer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interactableLayer_5), (void*)value);
	}

	inline static int32_t get_offset_of_reachDestiny_6() { return static_cast<int32_t>(offsetof(InteractableBase_t5E0859747DB47E62279D12B7A14E157EE5DF9A08, ___reachDestiny_6)); }
	inline bool get_reachDestiny_6() const { return ___reachDestiny_6; }
	inline bool* get_address_of_reachDestiny_6() { return &___reachDestiny_6; }
	inline void set_reachDestiny_6(bool value)
	{
		___reachDestiny_6 = value;
	}
};


// InteractionUpdater
struct InteractionUpdater_t22CFD12453B6ACA4814BAB4EEF6FB95D2C47C814  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct InteractionUpdater_t22CFD12453B6ACA4814BAB4EEF6FB95D2C47C814_StaticFields
{
public:
	// TMPro.TextMeshProUGUI InteractionUpdater::actionTMP
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___actionTMP_4;

public:
	inline static int32_t get_offset_of_actionTMP_4() { return static_cast<int32_t>(offsetof(InteractionUpdater_t22CFD12453B6ACA4814BAB4EEF6FB95D2C47C814_StaticFields, ___actionTMP_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_actionTMP_4() const { return ___actionTMP_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_actionTMP_4() { return &___actionTMP_4; }
	inline void set_actionTMP_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___actionTMP_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___actionTMP_4), (void*)value);
	}
};


// NextCameraPoint
struct NextCameraPoint_tDE95DD7250288FF86CA500ABBEBF1977EECE6ED2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Events.UnityEvent NextCameraPoint::TriggerEvent
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___TriggerEvent_4;

public:
	inline static int32_t get_offset_of_TriggerEvent_4() { return static_cast<int32_t>(offsetof(NextCameraPoint_tDE95DD7250288FF86CA500ABBEBF1977EECE6ED2, ___TriggerEvent_4)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_TriggerEvent_4() const { return ___TriggerEvent_4; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_TriggerEvent_4() { return &___TriggerEvent_4; }
	inline void set_TriggerEvent_4(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___TriggerEvent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TriggerEvent_4), (void*)value);
	}
};


// PauseController
struct PauseController_t60C0B1AD0DA3C383DDE9B236621896374C1AA990  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// PlayerComponent
struct PlayerComponent_t50D5545C539262D84FF8A719CAEA023A890F4839  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean PlayerComponent::initialized
	bool ___initialized_4;
	// Player PlayerComponent::player
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___player_5;
	// System.Boolean PlayerComponent::firstEnable
	bool ___firstEnable_6;

public:
	inline static int32_t get_offset_of_initialized_4() { return static_cast<int32_t>(offsetof(PlayerComponent_t50D5545C539262D84FF8A719CAEA023A890F4839, ___initialized_4)); }
	inline bool get_initialized_4() const { return ___initialized_4; }
	inline bool* get_address_of_initialized_4() { return &___initialized_4; }
	inline void set_initialized_4(bool value)
	{
		___initialized_4 = value;
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(PlayerComponent_t50D5545C539262D84FF8A719CAEA023A890F4839, ___player_5)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_player_5() const { return ___player_5; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___player_5), (void*)value);
	}

	inline static int32_t get_offset_of_firstEnable_6() { return static_cast<int32_t>(offsetof(PlayerComponent_t50D5545C539262D84FF8A719CAEA023A890F4839, ___firstEnable_6)); }
	inline bool get_firstEnable_6() const { return ___firstEnable_6; }
	inline bool* get_address_of_firstEnable_6() { return &___firstEnable_6; }
	inline void set_firstEnable_6(bool value)
	{
		___firstEnable_6 = value;
	}
};


// PlayerTrigger
struct PlayerTrigger_t42EAB7463A59B8DC10C15914485B2EDC5F2C3A7A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.LayerMask PlayerTrigger::layerIgnore
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___layerIgnore_4;
	// UnityEngine.Transform PlayerTrigger::rayOrigin
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___rayOrigin_5;

public:
	inline static int32_t get_offset_of_layerIgnore_4() { return static_cast<int32_t>(offsetof(PlayerTrigger_t42EAB7463A59B8DC10C15914485B2EDC5F2C3A7A, ___layerIgnore_4)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_layerIgnore_4() const { return ___layerIgnore_4; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_layerIgnore_4() { return &___layerIgnore_4; }
	inline void set_layerIgnore_4(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___layerIgnore_4 = value;
	}

	inline static int32_t get_offset_of_rayOrigin_5() { return static_cast<int32_t>(offsetof(PlayerTrigger_t42EAB7463A59B8DC10C15914485B2EDC5F2C3A7A, ___rayOrigin_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_rayOrigin_5() const { return ___rayOrigin_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_rayOrigin_5() { return &___rayOrigin_5; }
	inline void set_rayOrigin_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___rayOrigin_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rayOrigin_5), (void*)value);
	}
};


// ProgressPoint
struct ProgressPoint_tB6B11868518E3F543DB66FDDBB48669586D72341  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// ReachChecker
struct ReachChecker_t0D0BF213B48D49EA8186F575FC8475BBD03C556A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Grabbable ReachChecker::grabbable
	Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3 * ___grabbable_4;

public:
	inline static int32_t get_offset_of_grabbable_4() { return static_cast<int32_t>(offsetof(ReachChecker_t0D0BF213B48D49EA8186F575FC8475BBD03C556A, ___grabbable_4)); }
	inline Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3 * get_grabbable_4() const { return ___grabbable_4; }
	inline Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3 ** get_address_of_grabbable_4() { return &___grabbable_4; }
	inline void set_grabbable_4(Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3 * value)
	{
		___grabbable_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___grabbable_4), (void*)value);
	}
};


// Scenery
struct Scenery_tF2FD47C991A1BAE0F47D9DCAC99FD105988BD68A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Scenery::nextMap
	String_t* ___nextMap_4;

public:
	inline static int32_t get_offset_of_nextMap_4() { return static_cast<int32_t>(offsetof(Scenery_tF2FD47C991A1BAE0F47D9DCAC99FD105988BD68A, ___nextMap_4)); }
	inline String_t* get_nextMap_4() const { return ___nextMap_4; }
	inline String_t** get_address_of_nextMap_4() { return &___nextMap_4; }
	inline void set_nextMap_4(String_t* value)
	{
		___nextMap_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nextMap_4), (void*)value);
	}
};


// SingletonMonoBehaviour`1<GameManager>
struct SingletonMonoBehaviour_1_tF1716B7BA0400CA06A7D9DD40BCE6A23738B8FD7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct SingletonMonoBehaviour_1_tF1716B7BA0400CA06A7D9DD40BCE6A23738B8FD7_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::I
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___I_4;

public:
	inline static int32_t get_offset_of_I_4() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_tF1716B7BA0400CA06A7D9DD40BCE6A23738B8FD7_StaticFields, ___I_4)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_I_4() const { return ___I_4; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_I_4() { return &___I_4; }
	inline void set_I_4(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___I_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___I_4), (void*)value);
	}
};


// TargetPoint
struct TargetPoint_t5845B3F89662C4A8770F1F98FE94FCCF588DC66A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Collider[] TargetPoint::ignoreColliders
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* ___ignoreColliders_4;

public:
	inline static int32_t get_offset_of_ignoreColliders_4() { return static_cast<int32_t>(offsetof(TargetPoint_t5845B3F89662C4A8770F1F98FE94FCCF588DC66A, ___ignoreColliders_4)); }
	inline ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* get_ignoreColliders_4() const { return ___ignoreColliders_4; }
	inline ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252** get_address_of_ignoreColliders_4() { return &___ignoreColliders_4; }
	inline void set_ignoreColliders_4(ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* value)
	{
		___ignoreColliders_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ignoreColliders_4), (void*)value);
	}
};


// UnityStandardAssets.Water.Displace
struct Displace_tE71543D1BC659AE19223A155A9CE84FB1110DDE3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// UnityStandardAssets.Water.PlanarReflection
struct PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.LayerMask UnityStandardAssets.Water.PlanarReflection::reflectionMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___reflectionMask_4;
	// System.Boolean UnityStandardAssets.Water.PlanarReflection::reflectSkybox
	bool ___reflectSkybox_5;
	// UnityEngine.Color UnityStandardAssets.Water.PlanarReflection::clearColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___clearColor_6;
	// System.String UnityStandardAssets.Water.PlanarReflection::reflectionSampler
	String_t* ___reflectionSampler_7;
	// System.Single UnityStandardAssets.Water.PlanarReflection::clipPlaneOffset
	float ___clipPlaneOffset_8;
	// UnityEngine.Vector3 UnityStandardAssets.Water.PlanarReflection::m_Oldpos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Oldpos_9;
	// UnityEngine.Camera UnityStandardAssets.Water.PlanarReflection::m_ReflectionCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_ReflectionCamera_10;
	// UnityEngine.Material UnityStandardAssets.Water.PlanarReflection::m_SharedMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_SharedMaterial_11;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,System.Boolean> UnityStandardAssets.Water.PlanarReflection::m_HelperCameras
	Dictionary_2_tDB6E99559F505A01720155F4596CB8DC52E0E786 * ___m_HelperCameras_12;

public:
	inline static int32_t get_offset_of_reflectionMask_4() { return static_cast<int32_t>(offsetof(PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B, ___reflectionMask_4)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_reflectionMask_4() const { return ___reflectionMask_4; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_reflectionMask_4() { return &___reflectionMask_4; }
	inline void set_reflectionMask_4(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___reflectionMask_4 = value;
	}

	inline static int32_t get_offset_of_reflectSkybox_5() { return static_cast<int32_t>(offsetof(PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B, ___reflectSkybox_5)); }
	inline bool get_reflectSkybox_5() const { return ___reflectSkybox_5; }
	inline bool* get_address_of_reflectSkybox_5() { return &___reflectSkybox_5; }
	inline void set_reflectSkybox_5(bool value)
	{
		___reflectSkybox_5 = value;
	}

	inline static int32_t get_offset_of_clearColor_6() { return static_cast<int32_t>(offsetof(PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B, ___clearColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_clearColor_6() const { return ___clearColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_clearColor_6() { return &___clearColor_6; }
	inline void set_clearColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___clearColor_6 = value;
	}

	inline static int32_t get_offset_of_reflectionSampler_7() { return static_cast<int32_t>(offsetof(PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B, ___reflectionSampler_7)); }
	inline String_t* get_reflectionSampler_7() const { return ___reflectionSampler_7; }
	inline String_t** get_address_of_reflectionSampler_7() { return &___reflectionSampler_7; }
	inline void set_reflectionSampler_7(String_t* value)
	{
		___reflectionSampler_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reflectionSampler_7), (void*)value);
	}

	inline static int32_t get_offset_of_clipPlaneOffset_8() { return static_cast<int32_t>(offsetof(PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B, ___clipPlaneOffset_8)); }
	inline float get_clipPlaneOffset_8() const { return ___clipPlaneOffset_8; }
	inline float* get_address_of_clipPlaneOffset_8() { return &___clipPlaneOffset_8; }
	inline void set_clipPlaneOffset_8(float value)
	{
		___clipPlaneOffset_8 = value;
	}

	inline static int32_t get_offset_of_m_Oldpos_9() { return static_cast<int32_t>(offsetof(PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B, ___m_Oldpos_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Oldpos_9() const { return ___m_Oldpos_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Oldpos_9() { return &___m_Oldpos_9; }
	inline void set_m_Oldpos_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Oldpos_9 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCamera_10() { return static_cast<int32_t>(offsetof(PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B, ___m_ReflectionCamera_10)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_ReflectionCamera_10() const { return ___m_ReflectionCamera_10; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_ReflectionCamera_10() { return &___m_ReflectionCamera_10; }
	inline void set_m_ReflectionCamera_10(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_ReflectionCamera_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReflectionCamera_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_SharedMaterial_11() { return static_cast<int32_t>(offsetof(PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B, ___m_SharedMaterial_11)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_SharedMaterial_11() const { return ___m_SharedMaterial_11; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_SharedMaterial_11() { return &___m_SharedMaterial_11; }
	inline void set_m_SharedMaterial_11(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_SharedMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SharedMaterial_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_HelperCameras_12() { return static_cast<int32_t>(offsetof(PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B, ___m_HelperCameras_12)); }
	inline Dictionary_2_tDB6E99559F505A01720155F4596CB8DC52E0E786 * get_m_HelperCameras_12() const { return ___m_HelperCameras_12; }
	inline Dictionary_2_tDB6E99559F505A01720155F4596CB8DC52E0E786 ** get_address_of_m_HelperCameras_12() { return &___m_HelperCameras_12; }
	inline void set_m_HelperCameras_12(Dictionary_2_tDB6E99559F505A01720155F4596CB8DC52E0E786 * value)
	{
		___m_HelperCameras_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HelperCameras_12), (void*)value);
	}
};


// UnityStandardAssets.Water.SpecularLighting
struct SpecularLighting_t84EA6B1150A66C2D6D7395842F390FA7BFEC2A97  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform UnityStandardAssets.Water.SpecularLighting::specularLight
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___specularLight_4;
	// UnityStandardAssets.Water.WaterBase UnityStandardAssets.Water.SpecularLighting::m_WaterBase
	WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565 * ___m_WaterBase_5;

public:
	inline static int32_t get_offset_of_specularLight_4() { return static_cast<int32_t>(offsetof(SpecularLighting_t84EA6B1150A66C2D6D7395842F390FA7BFEC2A97, ___specularLight_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_specularLight_4() const { return ___specularLight_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_specularLight_4() { return &___specularLight_4; }
	inline void set_specularLight_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___specularLight_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___specularLight_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_WaterBase_5() { return static_cast<int32_t>(offsetof(SpecularLighting_t84EA6B1150A66C2D6D7395842F390FA7BFEC2A97, ___m_WaterBase_5)); }
	inline WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565 * get_m_WaterBase_5() const { return ___m_WaterBase_5; }
	inline WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565 ** get_address_of_m_WaterBase_5() { return &___m_WaterBase_5; }
	inline void set_m_WaterBase_5(WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565 * value)
	{
		___m_WaterBase_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_WaterBase_5), (void*)value);
	}
};


// UnityStandardAssets.Water.Water
struct Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::waterMode
	int32_t ___waterMode_4;
	// System.Boolean UnityStandardAssets.Water.Water::disablePixelLights
	bool ___disablePixelLights_5;
	// System.Int32 UnityStandardAssets.Water.Water::textureSize
	int32_t ___textureSize_6;
	// System.Single UnityStandardAssets.Water.Water::clipPlaneOffset
	float ___clipPlaneOffset_7;
	// UnityEngine.LayerMask UnityStandardAssets.Water.Water::reflectLayers
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___reflectLayers_8;
	// UnityEngine.LayerMask UnityStandardAssets.Water.Water::refractLayers
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___refractLayers_9;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera> UnityStandardAssets.Water.Water::m_ReflectionCameras
	Dictionary_2_tA412557CE01C74AD294901BEFE89672A83C176C7 * ___m_ReflectionCameras_10;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Camera> UnityStandardAssets.Water.Water::m_RefractionCameras
	Dictionary_2_tA412557CE01C74AD294901BEFE89672A83C176C7 * ___m_RefractionCameras_11;
	// UnityEngine.RenderTexture UnityStandardAssets.Water.Water::m_ReflectionTexture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___m_ReflectionTexture_12;
	// UnityEngine.RenderTexture UnityStandardAssets.Water.Water::m_RefractionTexture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___m_RefractionTexture_13;
	// UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::m_HardwareWaterSupport
	int32_t ___m_HardwareWaterSupport_14;
	// System.Int32 UnityStandardAssets.Water.Water::m_OldReflectionTextureSize
	int32_t ___m_OldReflectionTextureSize_15;
	// System.Int32 UnityStandardAssets.Water.Water::m_OldRefractionTextureSize
	int32_t ___m_OldRefractionTextureSize_16;

public:
	inline static int32_t get_offset_of_waterMode_4() { return static_cast<int32_t>(offsetof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D, ___waterMode_4)); }
	inline int32_t get_waterMode_4() const { return ___waterMode_4; }
	inline int32_t* get_address_of_waterMode_4() { return &___waterMode_4; }
	inline void set_waterMode_4(int32_t value)
	{
		___waterMode_4 = value;
	}

	inline static int32_t get_offset_of_disablePixelLights_5() { return static_cast<int32_t>(offsetof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D, ___disablePixelLights_5)); }
	inline bool get_disablePixelLights_5() const { return ___disablePixelLights_5; }
	inline bool* get_address_of_disablePixelLights_5() { return &___disablePixelLights_5; }
	inline void set_disablePixelLights_5(bool value)
	{
		___disablePixelLights_5 = value;
	}

	inline static int32_t get_offset_of_textureSize_6() { return static_cast<int32_t>(offsetof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D, ___textureSize_6)); }
	inline int32_t get_textureSize_6() const { return ___textureSize_6; }
	inline int32_t* get_address_of_textureSize_6() { return &___textureSize_6; }
	inline void set_textureSize_6(int32_t value)
	{
		___textureSize_6 = value;
	}

	inline static int32_t get_offset_of_clipPlaneOffset_7() { return static_cast<int32_t>(offsetof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D, ___clipPlaneOffset_7)); }
	inline float get_clipPlaneOffset_7() const { return ___clipPlaneOffset_7; }
	inline float* get_address_of_clipPlaneOffset_7() { return &___clipPlaneOffset_7; }
	inline void set_clipPlaneOffset_7(float value)
	{
		___clipPlaneOffset_7 = value;
	}

	inline static int32_t get_offset_of_reflectLayers_8() { return static_cast<int32_t>(offsetof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D, ___reflectLayers_8)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_reflectLayers_8() const { return ___reflectLayers_8; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_reflectLayers_8() { return &___reflectLayers_8; }
	inline void set_reflectLayers_8(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___reflectLayers_8 = value;
	}

	inline static int32_t get_offset_of_refractLayers_9() { return static_cast<int32_t>(offsetof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D, ___refractLayers_9)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_refractLayers_9() const { return ___refractLayers_9; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_refractLayers_9() { return &___refractLayers_9; }
	inline void set_refractLayers_9(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___refractLayers_9 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCameras_10() { return static_cast<int32_t>(offsetof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D, ___m_ReflectionCameras_10)); }
	inline Dictionary_2_tA412557CE01C74AD294901BEFE89672A83C176C7 * get_m_ReflectionCameras_10() const { return ___m_ReflectionCameras_10; }
	inline Dictionary_2_tA412557CE01C74AD294901BEFE89672A83C176C7 ** get_address_of_m_ReflectionCameras_10() { return &___m_ReflectionCameras_10; }
	inline void set_m_ReflectionCameras_10(Dictionary_2_tA412557CE01C74AD294901BEFE89672A83C176C7 * value)
	{
		___m_ReflectionCameras_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReflectionCameras_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_RefractionCameras_11() { return static_cast<int32_t>(offsetof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D, ___m_RefractionCameras_11)); }
	inline Dictionary_2_tA412557CE01C74AD294901BEFE89672A83C176C7 * get_m_RefractionCameras_11() const { return ___m_RefractionCameras_11; }
	inline Dictionary_2_tA412557CE01C74AD294901BEFE89672A83C176C7 ** get_address_of_m_RefractionCameras_11() { return &___m_RefractionCameras_11; }
	inline void set_m_RefractionCameras_11(Dictionary_2_tA412557CE01C74AD294901BEFE89672A83C176C7 * value)
	{
		___m_RefractionCameras_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RefractionCameras_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_ReflectionTexture_12() { return static_cast<int32_t>(offsetof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D, ___m_ReflectionTexture_12)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_m_ReflectionTexture_12() const { return ___m_ReflectionTexture_12; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_m_ReflectionTexture_12() { return &___m_ReflectionTexture_12; }
	inline void set_m_ReflectionTexture_12(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___m_ReflectionTexture_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReflectionTexture_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_RefractionTexture_13() { return static_cast<int32_t>(offsetof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D, ___m_RefractionTexture_13)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_m_RefractionTexture_13() const { return ___m_RefractionTexture_13; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_m_RefractionTexture_13() { return &___m_RefractionTexture_13; }
	inline void set_m_RefractionTexture_13(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___m_RefractionTexture_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RefractionTexture_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_HardwareWaterSupport_14() { return static_cast<int32_t>(offsetof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D, ___m_HardwareWaterSupport_14)); }
	inline int32_t get_m_HardwareWaterSupport_14() const { return ___m_HardwareWaterSupport_14; }
	inline int32_t* get_address_of_m_HardwareWaterSupport_14() { return &___m_HardwareWaterSupport_14; }
	inline void set_m_HardwareWaterSupport_14(int32_t value)
	{
		___m_HardwareWaterSupport_14 = value;
	}

	inline static int32_t get_offset_of_m_OldReflectionTextureSize_15() { return static_cast<int32_t>(offsetof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D, ___m_OldReflectionTextureSize_15)); }
	inline int32_t get_m_OldReflectionTextureSize_15() const { return ___m_OldReflectionTextureSize_15; }
	inline int32_t* get_address_of_m_OldReflectionTextureSize_15() { return &___m_OldReflectionTextureSize_15; }
	inline void set_m_OldReflectionTextureSize_15(int32_t value)
	{
		___m_OldReflectionTextureSize_15 = value;
	}

	inline static int32_t get_offset_of_m_OldRefractionTextureSize_16() { return static_cast<int32_t>(offsetof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D, ___m_OldRefractionTextureSize_16)); }
	inline int32_t get_m_OldRefractionTextureSize_16() const { return ___m_OldRefractionTextureSize_16; }
	inline int32_t* get_address_of_m_OldRefractionTextureSize_16() { return &___m_OldRefractionTextureSize_16; }
	inline void set_m_OldRefractionTextureSize_16(int32_t value)
	{
		___m_OldRefractionTextureSize_16 = value;
	}
};

struct Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D_StaticFields
{
public:
	// System.Boolean UnityStandardAssets.Water.Water::s_InsideWater
	bool ___s_InsideWater_17;

public:
	inline static int32_t get_offset_of_s_InsideWater_17() { return static_cast<int32_t>(offsetof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D_StaticFields, ___s_InsideWater_17)); }
	inline bool get_s_InsideWater_17() const { return ___s_InsideWater_17; }
	inline bool* get_address_of_s_InsideWater_17() { return &___s_InsideWater_17; }
	inline void set_s_InsideWater_17(bool value)
	{
		___s_InsideWater_17 = value;
	}
};


// UnityStandardAssets.Water.WaterBase
struct WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Material UnityStandardAssets.Water.WaterBase::sharedMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___sharedMaterial_4;
	// UnityStandardAssets.Water.WaterQuality UnityStandardAssets.Water.WaterBase::waterQuality
	int32_t ___waterQuality_5;
	// System.Boolean UnityStandardAssets.Water.WaterBase::edgeBlend
	bool ___edgeBlend_6;

public:
	inline static int32_t get_offset_of_sharedMaterial_4() { return static_cast<int32_t>(offsetof(WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565, ___sharedMaterial_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_sharedMaterial_4() const { return ___sharedMaterial_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_sharedMaterial_4() { return &___sharedMaterial_4; }
	inline void set_sharedMaterial_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___sharedMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sharedMaterial_4), (void*)value);
	}

	inline static int32_t get_offset_of_waterQuality_5() { return static_cast<int32_t>(offsetof(WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565, ___waterQuality_5)); }
	inline int32_t get_waterQuality_5() const { return ___waterQuality_5; }
	inline int32_t* get_address_of_waterQuality_5() { return &___waterQuality_5; }
	inline void set_waterQuality_5(int32_t value)
	{
		___waterQuality_5 = value;
	}

	inline static int32_t get_offset_of_edgeBlend_6() { return static_cast<int32_t>(offsetof(WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565, ___edgeBlend_6)); }
	inline bool get_edgeBlend_6() const { return ___edgeBlend_6; }
	inline bool* get_address_of_edgeBlend_6() { return &___edgeBlend_6; }
	inline void set_edgeBlend_6(bool value)
	{
		___edgeBlend_6 = value;
	}
};


// UnityStandardAssets.Water.WaterBasic
struct WaterBasic_t94B54D7222D89D61D4A2E0E8CE3CA4631BA11EEB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// UnityStandardAssets.Water.WaterTile
struct WaterTile_t9F836DCF3384AB4ACC5BFF1EC7C57ED3D7FCFBC7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.Water.PlanarReflection UnityStandardAssets.Water.WaterTile::reflection
	PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B * ___reflection_4;
	// UnityStandardAssets.Water.WaterBase UnityStandardAssets.Water.WaterTile::waterBase
	WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565 * ___waterBase_5;

public:
	inline static int32_t get_offset_of_reflection_4() { return static_cast<int32_t>(offsetof(WaterTile_t9F836DCF3384AB4ACC5BFF1EC7C57ED3D7FCFBC7, ___reflection_4)); }
	inline PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B * get_reflection_4() const { return ___reflection_4; }
	inline PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B ** get_address_of_reflection_4() { return &___reflection_4; }
	inline void set_reflection_4(PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B * value)
	{
		___reflection_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reflection_4), (void*)value);
	}

	inline static int32_t get_offset_of_waterBase_5() { return static_cast<int32_t>(offsetof(WaterTile_t9F836DCF3384AB4ACC5BFF1EC7C57ED3D7FCFBC7, ___waterBase_5)); }
	inline WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565 * get_waterBase_5() const { return ___waterBase_5; }
	inline WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565 ** get_address_of_waterBase_5() { return &___waterBase_5; }
	inline void set_waterBase_5(WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565 * value)
	{
		___waterBase_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___waterBase_5), (void*)value);
	}
};


// UpdaterInteractionHUD
struct UpdaterInteractionHUD_t0095EF4C07DC1491DE8223358AFA965931DA0717  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 UpdaterInteractionHUD::count
	int32_t ___count_4;

public:
	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(UpdaterInteractionHUD_t0095EF4C07DC1491DE8223358AFA965931DA0717, ___count_4)); }
	inline int32_t get_count_4() const { return ___count_4; }
	inline int32_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int32_t value)
	{
		___count_4 = value;
	}
};


// WinScreen
struct WinScreen_t3859A82ACD171227DDD08CA3F462A8D75B46E869  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TextMeshProUGUI WinScreen::coinTMP
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___coinTMP_4;

public:
	inline static int32_t get_offset_of_coinTMP_4() { return static_cast<int32_t>(offsetof(WinScreen_t3859A82ACD171227DDD08CA3F462A8D75B46E869, ___coinTMP_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_coinTMP_4() const { return ___coinTMP_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_coinTMP_4() { return &___coinTMP_4; }
	inline void set_coinTMP_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___coinTMP_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coinTMP_4), (void*)value);
	}
};


// Cinemachine.CinemachineCollisionImpulseSource
struct CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78  : public CinemachineImpulseSource_t0939CD50852A448FDE8E55DDB3050358180B10ED
{
public:
	// UnityEngine.LayerMask Cinemachine.CinemachineCollisionImpulseSource::m_LayerMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___m_LayerMask_5;
	// System.String Cinemachine.CinemachineCollisionImpulseSource::m_IgnoreTag
	String_t* ___m_IgnoreTag_6;
	// System.Boolean Cinemachine.CinemachineCollisionImpulseSource::m_UseImpactDirection
	bool ___m_UseImpactDirection_7;
	// System.Boolean Cinemachine.CinemachineCollisionImpulseSource::m_ScaleImpactWithMass
	bool ___m_ScaleImpactWithMass_8;
	// System.Boolean Cinemachine.CinemachineCollisionImpulseSource::m_ScaleImpactWithSpeed
	bool ___m_ScaleImpactWithSpeed_9;
	// UnityEngine.Rigidbody Cinemachine.CinemachineCollisionImpulseSource::mRigidBody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___mRigidBody_10;
	// UnityEngine.Rigidbody2D Cinemachine.CinemachineCollisionImpulseSource::mRigidBody2D
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___mRigidBody2D_11;

public:
	inline static int32_t get_offset_of_m_LayerMask_5() { return static_cast<int32_t>(offsetof(CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78, ___m_LayerMask_5)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_m_LayerMask_5() const { return ___m_LayerMask_5; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_m_LayerMask_5() { return &___m_LayerMask_5; }
	inline void set_m_LayerMask_5(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___m_LayerMask_5 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTag_6() { return static_cast<int32_t>(offsetof(CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78, ___m_IgnoreTag_6)); }
	inline String_t* get_m_IgnoreTag_6() const { return ___m_IgnoreTag_6; }
	inline String_t** get_address_of_m_IgnoreTag_6() { return &___m_IgnoreTag_6; }
	inline void set_m_IgnoreTag_6(String_t* value)
	{
		___m_IgnoreTag_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_IgnoreTag_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_UseImpactDirection_7() { return static_cast<int32_t>(offsetof(CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78, ___m_UseImpactDirection_7)); }
	inline bool get_m_UseImpactDirection_7() const { return ___m_UseImpactDirection_7; }
	inline bool* get_address_of_m_UseImpactDirection_7() { return &___m_UseImpactDirection_7; }
	inline void set_m_UseImpactDirection_7(bool value)
	{
		___m_UseImpactDirection_7 = value;
	}

	inline static int32_t get_offset_of_m_ScaleImpactWithMass_8() { return static_cast<int32_t>(offsetof(CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78, ___m_ScaleImpactWithMass_8)); }
	inline bool get_m_ScaleImpactWithMass_8() const { return ___m_ScaleImpactWithMass_8; }
	inline bool* get_address_of_m_ScaleImpactWithMass_8() { return &___m_ScaleImpactWithMass_8; }
	inline void set_m_ScaleImpactWithMass_8(bool value)
	{
		___m_ScaleImpactWithMass_8 = value;
	}

	inline static int32_t get_offset_of_m_ScaleImpactWithSpeed_9() { return static_cast<int32_t>(offsetof(CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78, ___m_ScaleImpactWithSpeed_9)); }
	inline bool get_m_ScaleImpactWithSpeed_9() const { return ___m_ScaleImpactWithSpeed_9; }
	inline bool* get_address_of_m_ScaleImpactWithSpeed_9() { return &___m_ScaleImpactWithSpeed_9; }
	inline void set_m_ScaleImpactWithSpeed_9(bool value)
	{
		___m_ScaleImpactWithSpeed_9 = value;
	}

	inline static int32_t get_offset_of_mRigidBody_10() { return static_cast<int32_t>(offsetof(CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78, ___mRigidBody_10)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_mRigidBody_10() const { return ___mRigidBody_10; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_mRigidBody_10() { return &___mRigidBody_10; }
	inline void set_mRigidBody_10(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___mRigidBody_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mRigidBody_10), (void*)value);
	}

	inline static int32_t get_offset_of_mRigidBody2D_11() { return static_cast<int32_t>(offsetof(CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78, ___mRigidBody2D_11)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_mRigidBody2D_11() const { return ___mRigidBody2D_11; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_mRigidBody2D_11() { return &___mRigidBody2D_11; }
	inline void set_mRigidBody2D_11(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___mRigidBody2D_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mRigidBody2D_11), (void*)value);
	}
};


// Cinemachine.CinemachineImpulseListener
struct CinemachineImpulseListener_tB33F46B5F828297AAC0EDFF5515FD7D49051DFDD  : public CinemachineExtension_t397048E40D5EB90A0A06031DCF9E1B52306D06AE
{
public:
	// System.Int32 Cinemachine.CinemachineImpulseListener::m_ChannelMask
	int32_t ___m_ChannelMask_7;
	// System.Single Cinemachine.CinemachineImpulseListener::m_Gain
	float ___m_Gain_8;
	// System.Boolean Cinemachine.CinemachineImpulseListener::m_Use2DDistance
	bool ___m_Use2DDistance_9;

public:
	inline static int32_t get_offset_of_m_ChannelMask_7() { return static_cast<int32_t>(offsetof(CinemachineImpulseListener_tB33F46B5F828297AAC0EDFF5515FD7D49051DFDD, ___m_ChannelMask_7)); }
	inline int32_t get_m_ChannelMask_7() const { return ___m_ChannelMask_7; }
	inline int32_t* get_address_of_m_ChannelMask_7() { return &___m_ChannelMask_7; }
	inline void set_m_ChannelMask_7(int32_t value)
	{
		___m_ChannelMask_7 = value;
	}

	inline static int32_t get_offset_of_m_Gain_8() { return static_cast<int32_t>(offsetof(CinemachineImpulseListener_tB33F46B5F828297AAC0EDFF5515FD7D49051DFDD, ___m_Gain_8)); }
	inline float get_m_Gain_8() const { return ___m_Gain_8; }
	inline float* get_address_of_m_Gain_8() { return &___m_Gain_8; }
	inline void set_m_Gain_8(float value)
	{
		___m_Gain_8 = value;
	}

	inline static int32_t get_offset_of_m_Use2DDistance_9() { return static_cast<int32_t>(offsetof(CinemachineImpulseListener_tB33F46B5F828297AAC0EDFF5515FD7D49051DFDD, ___m_Use2DDistance_9)); }
	inline bool get_m_Use2DDistance_9() const { return ___m_Use2DDistance_9; }
	inline bool* get_address_of_m_Use2DDistance_9() { return &___m_Use2DDistance_9; }
	inline void set_m_Use2DDistance_9(bool value)
	{
		___m_Use2DDistance_9 = value;
	}
};


// Coin
struct Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59  : public Collectable_tB9D507F84CB6880BA5EE1A6A2589203264F29651
{
public:

public:
};


// EnemyBehaviour
struct EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552  : public CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039
{
public:
	// UnityEngine.Transform[] EnemyBehaviour::targetPoints
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ___targetPoints_10;
	// System.Int32 EnemyBehaviour::timeInPatrol
	int32_t ___timeInPatrol_11;
	// System.Int32 EnemyBehaviour::currentTarget
	int32_t ___currentTarget_12;
	// System.Boolean EnemyBehaviour::waiting
	bool ___waiting_13;

public:
	inline static int32_t get_offset_of_targetPoints_10() { return static_cast<int32_t>(offsetof(EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552, ___targetPoints_10)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get_targetPoints_10() const { return ___targetPoints_10; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of_targetPoints_10() { return &___targetPoints_10; }
	inline void set_targetPoints_10(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		___targetPoints_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetPoints_10), (void*)value);
	}

	inline static int32_t get_offset_of_timeInPatrol_11() { return static_cast<int32_t>(offsetof(EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552, ___timeInPatrol_11)); }
	inline int32_t get_timeInPatrol_11() const { return ___timeInPatrol_11; }
	inline int32_t* get_address_of_timeInPatrol_11() { return &___timeInPatrol_11; }
	inline void set_timeInPatrol_11(int32_t value)
	{
		___timeInPatrol_11 = value;
	}

	inline static int32_t get_offset_of_currentTarget_12() { return static_cast<int32_t>(offsetof(EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552, ___currentTarget_12)); }
	inline int32_t get_currentTarget_12() const { return ___currentTarget_12; }
	inline int32_t* get_address_of_currentTarget_12() { return &___currentTarget_12; }
	inline void set_currentTarget_12(int32_t value)
	{
		___currentTarget_12 = value;
	}

	inline static int32_t get_offset_of_waiting_13() { return static_cast<int32_t>(offsetof(EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552, ___waiting_13)); }
	inline bool get_waiting_13() const { return ___waiting_13; }
	inline bool* get_address_of_waiting_13() { return &___waiting_13; }
	inline void set_waiting_13(bool value)
	{
		___waiting_13 = value;
	}
};


// GameManager
struct GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89  : public SingletonMonoBehaviour_1_tF1716B7BA0400CA06A7D9DD40BCE6A23738B8FD7
{
public:
	// CollectableData GameManager::collectableData
	CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7 * ___collectableData_5;
	// UnityEngine.GameObject GameManager::loadingImage
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___loadingImage_6;
	// UnityEngine.GameObject GameManager::pauseScreen
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pauseScreen_7;
	// EndGameScreen GameManager::endGameScreen
	EndGameScreen_tA1957EC7E20256B5515F2ABF8D752AD582C297FF * ___endGameScreen_8;
	// PoolSystem GameManager::poolSystem
	PoolSystem_t1805A8BEF518DCC1DA9D16E7932112C3692507A4 * ___poolSystem_9;
	// InputSettings GameManager::inputMap
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * ___inputMap_10;

public:
	inline static int32_t get_offset_of_collectableData_5() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___collectableData_5)); }
	inline CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7 * get_collectableData_5() const { return ___collectableData_5; }
	inline CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7 ** get_address_of_collectableData_5() { return &___collectableData_5; }
	inline void set_collectableData_5(CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7 * value)
	{
		___collectableData_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collectableData_5), (void*)value);
	}

	inline static int32_t get_offset_of_loadingImage_6() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___loadingImage_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_loadingImage_6() const { return ___loadingImage_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_loadingImage_6() { return &___loadingImage_6; }
	inline void set_loadingImage_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___loadingImage_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loadingImage_6), (void*)value);
	}

	inline static int32_t get_offset_of_pauseScreen_7() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___pauseScreen_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pauseScreen_7() const { return ___pauseScreen_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pauseScreen_7() { return &___pauseScreen_7; }
	inline void set_pauseScreen_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pauseScreen_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pauseScreen_7), (void*)value);
	}

	inline static int32_t get_offset_of_endGameScreen_8() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___endGameScreen_8)); }
	inline EndGameScreen_tA1957EC7E20256B5515F2ABF8D752AD582C297FF * get_endGameScreen_8() const { return ___endGameScreen_8; }
	inline EndGameScreen_tA1957EC7E20256B5515F2ABF8D752AD582C297FF ** get_address_of_endGameScreen_8() { return &___endGameScreen_8; }
	inline void set_endGameScreen_8(EndGameScreen_tA1957EC7E20256B5515F2ABF8D752AD582C297FF * value)
	{
		___endGameScreen_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___endGameScreen_8), (void*)value);
	}

	inline static int32_t get_offset_of_poolSystem_9() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___poolSystem_9)); }
	inline PoolSystem_t1805A8BEF518DCC1DA9D16E7932112C3692507A4 * get_poolSystem_9() const { return ___poolSystem_9; }
	inline PoolSystem_t1805A8BEF518DCC1DA9D16E7932112C3692507A4 ** get_address_of_poolSystem_9() { return &___poolSystem_9; }
	inline void set_poolSystem_9(PoolSystem_t1805A8BEF518DCC1DA9D16E7932112C3692507A4 * value)
	{
		___poolSystem_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___poolSystem_9), (void*)value);
	}

	inline static int32_t get_offset_of_inputMap_10() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___inputMap_10)); }
	inline InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * get_inputMap_10() const { return ___inputMap_10; }
	inline InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE ** get_address_of_inputMap_10() { return &___inputMap_10; }
	inline void set_inputMap_10(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * value)
	{
		___inputMap_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputMap_10), (void*)value);
	}
};


// Grabbable
struct Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3  : public InteractableBase_t5E0859747DB47E62279D12B7A14E157EE5DF9A08
{
public:
	// UnityEngine.GameObject Grabbable::destiny
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___destiny_7;
	// UnityEngine.Transform Grabbable::subTarget
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___subTarget_8;
	// Player Grabbable::playerScript
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___playerScript_9;

public:
	inline static int32_t get_offset_of_destiny_7() { return static_cast<int32_t>(offsetof(Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3, ___destiny_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_destiny_7() const { return ___destiny_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_destiny_7() { return &___destiny_7; }
	inline void set_destiny_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___destiny_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___destiny_7), (void*)value);
	}

	inline static int32_t get_offset_of_subTarget_8() { return static_cast<int32_t>(offsetof(Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3, ___subTarget_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_subTarget_8() const { return ___subTarget_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_subTarget_8() { return &___subTarget_8; }
	inline void set_subTarget_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___subTarget_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___subTarget_8), (void*)value);
	}

	inline static int32_t get_offset_of_playerScript_9() { return static_cast<int32_t>(offsetof(Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3, ___playerScript_9)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_playerScript_9() const { return ___playerScript_9; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_playerScript_9() { return &___playerScript_9; }
	inline void set_playerScript_9(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___playerScript_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerScript_9), (void*)value);
	}
};


// Player
struct Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873  : public CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039
{
public:
	// PlayerInput Player::playerInput
	PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB * ___playerInput_10;
	// System.Single Player::horizontal
	float ___horizontal_11;
	// System.Single Player::vertical
	float ___vertical_12;
	// System.Boolean Player::pushing
	bool ___pushing_13;
	// System.Boolean Player::crounched
	bool ___crounched_14;
	// System.Single Player::timerToPush
	float ___timerToPush_15;
	// System.Boolean Player::movimentPermission
	bool ___movimentPermission_16;
	// IPlayerComponent[] Player::allComponents
	IPlayerComponentU5BU5D_t17127376879D8403CFB92FCDF78A0AA888B39222* ___allComponents_17;
	// Grabbable Player::currentGrab
	Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3 * ___currentGrab_18;

public:
	inline static int32_t get_offset_of_playerInput_10() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___playerInput_10)); }
	inline PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB * get_playerInput_10() const { return ___playerInput_10; }
	inline PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB ** get_address_of_playerInput_10() { return &___playerInput_10; }
	inline void set_playerInput_10(PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB * value)
	{
		___playerInput_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerInput_10), (void*)value);
	}

	inline static int32_t get_offset_of_horizontal_11() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___horizontal_11)); }
	inline float get_horizontal_11() const { return ___horizontal_11; }
	inline float* get_address_of_horizontal_11() { return &___horizontal_11; }
	inline void set_horizontal_11(float value)
	{
		___horizontal_11 = value;
	}

	inline static int32_t get_offset_of_vertical_12() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___vertical_12)); }
	inline float get_vertical_12() const { return ___vertical_12; }
	inline float* get_address_of_vertical_12() { return &___vertical_12; }
	inline void set_vertical_12(float value)
	{
		___vertical_12 = value;
	}

	inline static int32_t get_offset_of_pushing_13() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___pushing_13)); }
	inline bool get_pushing_13() const { return ___pushing_13; }
	inline bool* get_address_of_pushing_13() { return &___pushing_13; }
	inline void set_pushing_13(bool value)
	{
		___pushing_13 = value;
	}

	inline static int32_t get_offset_of_crounched_14() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___crounched_14)); }
	inline bool get_crounched_14() const { return ___crounched_14; }
	inline bool* get_address_of_crounched_14() { return &___crounched_14; }
	inline void set_crounched_14(bool value)
	{
		___crounched_14 = value;
	}

	inline static int32_t get_offset_of_timerToPush_15() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___timerToPush_15)); }
	inline float get_timerToPush_15() const { return ___timerToPush_15; }
	inline float* get_address_of_timerToPush_15() { return &___timerToPush_15; }
	inline void set_timerToPush_15(float value)
	{
		___timerToPush_15 = value;
	}

	inline static int32_t get_offset_of_movimentPermission_16() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___movimentPermission_16)); }
	inline bool get_movimentPermission_16() const { return ___movimentPermission_16; }
	inline bool* get_address_of_movimentPermission_16() { return &___movimentPermission_16; }
	inline void set_movimentPermission_16(bool value)
	{
		___movimentPermission_16 = value;
	}

	inline static int32_t get_offset_of_allComponents_17() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___allComponents_17)); }
	inline IPlayerComponentU5BU5D_t17127376879D8403CFB92FCDF78A0AA888B39222* get_allComponents_17() const { return ___allComponents_17; }
	inline IPlayerComponentU5BU5D_t17127376879D8403CFB92FCDF78A0AA888B39222** get_address_of_allComponents_17() { return &___allComponents_17; }
	inline void set_allComponents_17(IPlayerComponentU5BU5D_t17127376879D8403CFB92FCDF78A0AA888B39222* value)
	{
		___allComponents_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allComponents_17), (void*)value);
	}

	inline static int32_t get_offset_of_currentGrab_18() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___currentGrab_18)); }
	inline Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3 * get_currentGrab_18() const { return ___currentGrab_18; }
	inline Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3 ** get_address_of_currentGrab_18() { return &___currentGrab_18; }
	inline void set_currentGrab_18(Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3 * value)
	{
		___currentGrab_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentGrab_18), (void*)value);
	}
};


// PlayerCrounch
struct PlayerCrounch_t5C13902ED3E9F51C9FD9471DB75276BD977A6D00  : public PlayerComponent_t50D5545C539262D84FF8A719CAEA023A890F4839
{
public:
	// UnityEngine.Collider PlayerCrounch::normalCollider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___normalCollider_7;
	// UnityEngine.Collider PlayerCrounch::CrounchCollider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___CrounchCollider_8;

public:
	inline static int32_t get_offset_of_normalCollider_7() { return static_cast<int32_t>(offsetof(PlayerCrounch_t5C13902ED3E9F51C9FD9471DB75276BD977A6D00, ___normalCollider_7)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_normalCollider_7() const { return ___normalCollider_7; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_normalCollider_7() { return &___normalCollider_7; }
	inline void set_normalCollider_7(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___normalCollider_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___normalCollider_7), (void*)value);
	}

	inline static int32_t get_offset_of_CrounchCollider_8() { return static_cast<int32_t>(offsetof(PlayerCrounch_t5C13902ED3E9F51C9FD9471DB75276BD977A6D00, ___CrounchCollider_8)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_CrounchCollider_8() const { return ___CrounchCollider_8; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_CrounchCollider_8() { return &___CrounchCollider_8; }
	inline void set_CrounchCollider_8(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___CrounchCollider_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CrounchCollider_8), (void*)value);
	}
};


// PlayerInput
struct PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB  : public PlayerComponent_t50D5545C539262D84FF8A719CAEA023A890F4839
{
public:
	// InputSettings PlayerInput::inputMap
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * ___inputMap_7;
	// System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext> PlayerInput::JumpAction
	Action_1_tFCECCF366288B4AB33F727560383950494F520DE * ___JumpAction_8;
	// System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext> PlayerInput::CrounchAction
	Action_1_tFCECCF366288B4AB33F727560383950494F520DE * ___CrounchAction_9;
	// System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext> PlayerInput::UnCrounchAction
	Action_1_tFCECCF366288B4AB33F727560383950494F520DE * ___UnCrounchAction_10;
	// System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext> PlayerInput::GrabAction
	Action_1_tFCECCF366288B4AB33F727560383950494F520DE * ___GrabAction_11;
	// System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext> PlayerInput::UnGrabAction
	Action_1_tFCECCF366288B4AB33F727560383950494F520DE * ___UnGrabAction_12;

public:
	inline static int32_t get_offset_of_inputMap_7() { return static_cast<int32_t>(offsetof(PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB, ___inputMap_7)); }
	inline InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * get_inputMap_7() const { return ___inputMap_7; }
	inline InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE ** get_address_of_inputMap_7() { return &___inputMap_7; }
	inline void set_inputMap_7(InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE * value)
	{
		___inputMap_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputMap_7), (void*)value);
	}

	inline static int32_t get_offset_of_JumpAction_8() { return static_cast<int32_t>(offsetof(PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB, ___JumpAction_8)); }
	inline Action_1_tFCECCF366288B4AB33F727560383950494F520DE * get_JumpAction_8() const { return ___JumpAction_8; }
	inline Action_1_tFCECCF366288B4AB33F727560383950494F520DE ** get_address_of_JumpAction_8() { return &___JumpAction_8; }
	inline void set_JumpAction_8(Action_1_tFCECCF366288B4AB33F727560383950494F520DE * value)
	{
		___JumpAction_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___JumpAction_8), (void*)value);
	}

	inline static int32_t get_offset_of_CrounchAction_9() { return static_cast<int32_t>(offsetof(PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB, ___CrounchAction_9)); }
	inline Action_1_tFCECCF366288B4AB33F727560383950494F520DE * get_CrounchAction_9() const { return ___CrounchAction_9; }
	inline Action_1_tFCECCF366288B4AB33F727560383950494F520DE ** get_address_of_CrounchAction_9() { return &___CrounchAction_9; }
	inline void set_CrounchAction_9(Action_1_tFCECCF366288B4AB33F727560383950494F520DE * value)
	{
		___CrounchAction_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CrounchAction_9), (void*)value);
	}

	inline static int32_t get_offset_of_UnCrounchAction_10() { return static_cast<int32_t>(offsetof(PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB, ___UnCrounchAction_10)); }
	inline Action_1_tFCECCF366288B4AB33F727560383950494F520DE * get_UnCrounchAction_10() const { return ___UnCrounchAction_10; }
	inline Action_1_tFCECCF366288B4AB33F727560383950494F520DE ** get_address_of_UnCrounchAction_10() { return &___UnCrounchAction_10; }
	inline void set_UnCrounchAction_10(Action_1_tFCECCF366288B4AB33F727560383950494F520DE * value)
	{
		___UnCrounchAction_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UnCrounchAction_10), (void*)value);
	}

	inline static int32_t get_offset_of_GrabAction_11() { return static_cast<int32_t>(offsetof(PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB, ___GrabAction_11)); }
	inline Action_1_tFCECCF366288B4AB33F727560383950494F520DE * get_GrabAction_11() const { return ___GrabAction_11; }
	inline Action_1_tFCECCF366288B4AB33F727560383950494F520DE ** get_address_of_GrabAction_11() { return &___GrabAction_11; }
	inline void set_GrabAction_11(Action_1_tFCECCF366288B4AB33F727560383950494F520DE * value)
	{
		___GrabAction_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GrabAction_11), (void*)value);
	}

	inline static int32_t get_offset_of_UnGrabAction_12() { return static_cast<int32_t>(offsetof(PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB, ___UnGrabAction_12)); }
	inline Action_1_tFCECCF366288B4AB33F727560383950494F520DE * get_UnGrabAction_12() const { return ___UnGrabAction_12; }
	inline Action_1_tFCECCF366288B4AB33F727560383950494F520DE ** get_address_of_UnGrabAction_12() { return &___UnGrabAction_12; }
	inline void set_UnGrabAction_12(Action_1_tFCECCF366288B4AB33F727560383950494F520DE * value)
	{
		___UnGrabAction_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UnGrabAction_12), (void*)value);
	}
};


// PlayerInteraction
struct PlayerInteraction_t7BE361C67931CA1C21B195DD20AEBAC3EE777A16  : public PlayerComponent_t50D5545C539262D84FF8A719CAEA023A890F4839
{
public:
	// UnityEngine.LayerMask PlayerInteraction::interactionLayer
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___interactionLayer_7;
	// UnityEngine.Collider[] PlayerInteraction::grabPossibilities
	ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* ___grabPossibilities_8;

public:
	inline static int32_t get_offset_of_interactionLayer_7() { return static_cast<int32_t>(offsetof(PlayerInteraction_t7BE361C67931CA1C21B195DD20AEBAC3EE777A16, ___interactionLayer_7)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_interactionLayer_7() const { return ___interactionLayer_7; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_interactionLayer_7() { return &___interactionLayer_7; }
	inline void set_interactionLayer_7(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___interactionLayer_7 = value;
	}

	inline static int32_t get_offset_of_grabPossibilities_8() { return static_cast<int32_t>(offsetof(PlayerInteraction_t7BE361C67931CA1C21B195DD20AEBAC3EE777A16, ___grabPossibilities_8)); }
	inline ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* get_grabPossibilities_8() const { return ___grabPossibilities_8; }
	inline ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252** get_address_of_grabPossibilities_8() { return &___grabPossibilities_8; }
	inline void set_grabPossibilities_8(ColliderU5BU5D_t70D1FDAE17E4359298B2BAA828048D1B7CFFE252* value)
	{
		___grabPossibilities_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___grabPossibilities_8), (void*)value);
	}
};


// PlayerJump
struct PlayerJump_t17597109FBE3C3969284027CBAA038DC6ECBF20D  : public PlayerComponent_t50D5545C539262D84FF8A719CAEA023A890F4839
{
public:
	// System.Single PlayerJump::jumpForce
	float ___jumpForce_7;
	// System.Single PlayerJump::jumpForceHigh
	float ___jumpForceHigh_8;
	// PlayerInteraction PlayerJump::playerInteraction
	PlayerInteraction_t7BE361C67931CA1C21B195DD20AEBAC3EE777A16 * ___playerInteraction_9;

public:
	inline static int32_t get_offset_of_jumpForce_7() { return static_cast<int32_t>(offsetof(PlayerJump_t17597109FBE3C3969284027CBAA038DC6ECBF20D, ___jumpForce_7)); }
	inline float get_jumpForce_7() const { return ___jumpForce_7; }
	inline float* get_address_of_jumpForce_7() { return &___jumpForce_7; }
	inline void set_jumpForce_7(float value)
	{
		___jumpForce_7 = value;
	}

	inline static int32_t get_offset_of_jumpForceHigh_8() { return static_cast<int32_t>(offsetof(PlayerJump_t17597109FBE3C3969284027CBAA038DC6ECBF20D, ___jumpForceHigh_8)); }
	inline float get_jumpForceHigh_8() const { return ___jumpForceHigh_8; }
	inline float* get_address_of_jumpForceHigh_8() { return &___jumpForceHigh_8; }
	inline void set_jumpForceHigh_8(float value)
	{
		___jumpForceHigh_8 = value;
	}

	inline static int32_t get_offset_of_playerInteraction_9() { return static_cast<int32_t>(offsetof(PlayerJump_t17597109FBE3C3969284027CBAA038DC6ECBF20D, ___playerInteraction_9)); }
	inline PlayerInteraction_t7BE361C67931CA1C21B195DD20AEBAC3EE777A16 * get_playerInteraction_9() const { return ___playerInteraction_9; }
	inline PlayerInteraction_t7BE361C67931CA1C21B195DD20AEBAC3EE777A16 ** get_address_of_playerInteraction_9() { return &___playerInteraction_9; }
	inline void set_playerInteraction_9(PlayerInteraction_t7BE361C67931CA1C21B195DD20AEBAC3EE777A16 * value)
	{
		___playerInteraction_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerInteraction_9), (void*)value);
	}
};


// UnityStandardAssets.Water.GerstnerDisplace
struct GerstnerDisplace_tE6CD37BD54D62274FF32AA2693237AE91731F01F  : public Displace_tE71543D1BC659AE19223A155A9CE84FB1110DDE3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3705;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3705 = { sizeof (NoiseSettingsPropertyAttribute_tBCFE5EE5B22D5A7D8D44BC1F4D329B7A65F7B503), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3706;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3706 = { sizeof (CinemachineEmbeddedAssetPropertyAttribute_t6A5ABA333EA100382FB10CCDA663908F397DCC14), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3706[1] = 
{
	CinemachineEmbeddedAssetPropertyAttribute_t6A5ABA333EA100382FB10CCDA663908F397DCC14::get_offset_of_WarnIfNull_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3707;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3707 = { sizeof (DocumentationSortingAttribute_tA3DD9DD04F77DEE0CF0FB7409F17C2A5A9920A87), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3707[1] = 
{
	DocumentationSortingAttribute_tA3DD9DD04F77DEE0CF0FB7409F17C2A5A9920A87::get_offset_of_U3CCategoryU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3708;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3708 = { sizeof (Level_tFCA1BDCF2D24439BC272A8BEE1A8EBBABA21F29A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3708[4] = 
{
	Level_tFCA1BDCF2D24439BC272A8BEE1A8EBBABA21F29A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3709;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3709 = { sizeof (CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3709[14] = 
{
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD::get_offset_of_m_ExcludedPropertiesInInspector_4(),
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD::get_offset_of_m_LockStageInInspector_5(),
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD::get_offset_of_m_ValidatingStreamVersion_6(),
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD::get_offset_of_m_OnValidateCalled_7(),
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD::get_offset_of_m_StreamingVersion_8(),
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD::get_offset_of_m_Priority_9(),
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD::get_offset_of_m_StandbyUpdate_10(),
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD::get_offset_of_mExtensions_11(),
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD::get_offset_of_m_previousStateIsValid_12(),
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD::get_offset_of_m_previousLookAtTarget_13(),
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD::get_offset_of_m_previousFollowTarget_14(),
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD::get_offset_of_mSlaveStatusUpdated_15(),
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD::get_offset_of_m_parentVcam_16(),
	CinemachineVirtualCameraBase_t654338913BD33E53DE4B401067876DFD098B93FD::get_offset_of_m_QueuePriority_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3710;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3710 = { sizeof (StandbyUpdateMode_tDD387031AFF8EB8516338BC71517029CB6F1BE18)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3710[4] = 
{
	StandbyUpdateMode_tDD387031AFF8EB8516338BC71517029CB6F1BE18::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3711;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3711 = { sizeof (BlendHint_t7D5604B2298076677193EF9C12950B17EA52ACA5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3711[5] = 
{
	BlendHint_t7D5604B2298076677193EF9C12950B17EA52ACA5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3712;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3712 = { sizeof (TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3712[3] = 
{
	TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8::get_offset_of_m_BlendHint_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8::get_offset_of_m_InheritPosition_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransitionParams_t0D13B0B638EA59591102682563B16676247E7AE8::get_offset_of_m_OnCameraLive_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3713;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3713 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3714;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3714 = { sizeof (LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC)+ sizeof (RuntimeObject), sizeof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC_marshaled_pinvoke), sizeof(LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3714[10] = 
{
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC_StaticFields::get_offset_of_Default_0(),
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC::get_offset_of_FieldOfView_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC::get_offset_of_OrthographicSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC::get_offset_of_NearClipPlane_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC::get_offset_of_FarClipPlane_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC::get_offset_of_Dutch_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC::get_offset_of_U3COrthographicU3Ek__BackingField_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC::get_offset_of_U3CIsPhysicalCameraU3Ek__BackingField_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC::get_offset_of_U3CSensorSizeU3Ek__BackingField_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LensSettings_tB9EF0C940E2889C6750565CEAA61CD62A81811AC::get_offset_of_LensShift_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3715;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3715 = { sizeof (NoiseSettings_t2F4032D427ECE5DF595F6E6BFC3881368E798EC5), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3715[2] = 
{
	NoiseSettings_t2F4032D427ECE5DF595F6E6BFC3881368E798EC5::get_offset_of_PositionNoise_4(),
	NoiseSettings_t2F4032D427ECE5DF595F6E6BFC3881368E798EC5::get_offset_of_OrientationNoise_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3716;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3716 = { sizeof (NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229)+ sizeof (RuntimeObject), sizeof(NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3716[3] = 
{
	NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229::get_offset_of_Frequency_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229::get_offset_of_Amplitude_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NoiseParams_t81EA0C016CA17F39EE25A9A7EEC333974CCC5229::get_offset_of_Constant_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3717;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3717 = { sizeof (TransformNoiseParams_t426C9F41209D45543A8DCACA9C6194ECE590C27D)+ sizeof (RuntimeObject), sizeof(TransformNoiseParams_t426C9F41209D45543A8DCACA9C6194ECE590C27D_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3717[3] = 
{
	TransformNoiseParams_t426C9F41209D45543A8DCACA9C6194ECE590C27D::get_offset_of_X_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformNoiseParams_t426C9F41209D45543A8DCACA9C6194ECE590C27D::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformNoiseParams_t426C9F41209D45543A8DCACA9C6194ECE590C27D::get_offset_of_Z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3718;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3718 = { sizeof (RuntimeUtility_t3A27FF884D04BFB4D5001F66D89928026AFED18F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3719;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3719 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3720;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3720 = { sizeof (SignalSourceAsset_t543AC1ED7A03A17D509FDBD53782B72CC58F1140), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3721;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3721 = { sizeof (UpdateTracker_tD9C9B2CB1F30AB0EAF2E743C107291FBBDACE8FE), -1, sizeof(UpdateTracker_tD9C9B2CB1F30AB0EAF2E743C107291FBBDACE8FE_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3721[3] = 
{
	UpdateTracker_tD9C9B2CB1F30AB0EAF2E743C107291FBBDACE8FE_StaticFields::get_offset_of_mUpdateStatus_0(),
	UpdateTracker_tD9C9B2CB1F30AB0EAF2E743C107291FBBDACE8FE_StaticFields::get_offset_of_sToDelete_1(),
	UpdateTracker_tD9C9B2CB1F30AB0EAF2E743C107291FBBDACE8FE_StaticFields::get_offset_of_mLastUpdateTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3722;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3722 = { sizeof (UpdateClock_tC6995F64381C6E9302A218387A9805FA1680EBED)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3722[3] = 
{
	UpdateClock_tC6995F64381C6E9302A218387A9805FA1680EBED::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3723;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3723 = { sizeof (UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3723[8] = 
{
	0,
	UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5::get_offset_of_windowStart_1(),
	UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5::get_offset_of_numWindowLateUpdateMoves_2(),
	UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5::get_offset_of_numWindowFixedUpdateMoves_3(),
	UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5::get_offset_of_numWindows_4(),
	UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5::get_offset_of_lastFrameUpdated_5(),
	UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5::get_offset_of_lastPos_6(),
	UpdateStatus_t11FACC32F65F1A7CA8A06879BEDA10893A69EAD5::get_offset_of_U3CPreferredUpdateU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3724;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3724 = { sizeof (CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3724[8] = 
{
	CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071::get_offset_of_m_LayerMask_4(),
	CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071::get_offset_of_m_WithTag_5(),
	CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071::get_offset_of_m_WithoutTag_6(),
	CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071::get_offset_of_m_SkipFirst_7(),
	CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071::get_offset_of_m_Repeating_8(),
	CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071::get_offset_of_m_OnObjectEnter_9(),
	CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071::get_offset_of_m_OnObjectExit_10(),
	CinemachineTriggerAction_t3189242873D3B637D5F877C85CE7D77ED5926071::get_offset_of_m_ActiveTriggerObjects_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3725;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3725 = { sizeof (ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3725[6] = 
{
	ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2::get_offset_of_m_Action_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2::get_offset_of_m_Target_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2::get_offset_of_m_BoostAmount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2::get_offset_of_m_StartTime_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2::get_offset_of_m_Mode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ActionSettings_t377E5679242CD69C57DF2F99E3919F8B9B2065E2::get_offset_of_m_Event_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3726;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3726 = { sizeof (Mode_t4B3F51421E88722B3CBE2723C07642D5CFD4B637)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3726[9] = 
{
	Mode_t4B3F51421E88722B3CBE2723C07642D5CFD4B637::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3727;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3727 = { sizeof (TriggerEvent_t4D34422A1066EEC819DFFEA8DD88FA76A44A9CA7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3728;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3728 = { sizeof (TimeMode_t45986AB4C1F762E22CAFC62432578E5343596E08)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3728[5] = 
{
	TimeMode_t45986AB4C1F762E22CAFC62432578E5343596E08::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3729;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3729 = { sizeof (CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3729[7] = 
{
	CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78::get_offset_of_m_LayerMask_5(),
	CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78::get_offset_of_m_IgnoreTag_6(),
	CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78::get_offset_of_m_UseImpactDirection_7(),
	CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78::get_offset_of_m_ScaleImpactWithMass_8(),
	CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78::get_offset_of_m_ScaleImpactWithSpeed_9(),
	CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78::get_offset_of_mRigidBody_10(),
	CinemachineCollisionImpulseSource_tFC61347391B88C5E6D53A1A67BCFE969832CFC78::get_offset_of_mRigidBody2D_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3730;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3730 = { sizeof (CinemachineFixedSignal_t74D185C6DC7771E44E97DB9EB33FB2914659E0B3), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3730[3] = 
{
	CinemachineFixedSignal_t74D185C6DC7771E44E97DB9EB33FB2914659E0B3::get_offset_of_m_XCurve_4(),
	CinemachineFixedSignal_t74D185C6DC7771E44E97DB9EB33FB2914659E0B3::get_offset_of_m_YCurve_5(),
	CinemachineFixedSignal_t74D185C6DC7771E44E97DB9EB33FB2914659E0B3::get_offset_of_m_ZCurve_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3731;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3731 = { sizeof (CinemachineImpulseDefinitionPropertyAttribute_tE39EAFF04AFBA44050B1F0B5EF3CED4653C67D24), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3732;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3732 = { sizeof (CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3732[11] = 
{
	CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF::get_offset_of_m_ImpulseChannel_0(),
	CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF::get_offset_of_m_RawSignal_1(),
	CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF::get_offset_of_m_AmplitudeGain_2(),
	CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF::get_offset_of_m_FrequencyGain_3(),
	CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF::get_offset_of_m_RepeatMode_4(),
	CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF::get_offset_of_m_Randomize_5(),
	CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF::get_offset_of_m_TimeEnvelope_6(),
	CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF::get_offset_of_m_ImpactRadius_7(),
	CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF::get_offset_of_m_DirectionMode_8(),
	CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF::get_offset_of_m_DissipationMode_9(),
	CinemachineImpulseDefinition_t8805EF3193242DE9EF2F60B6B29C92F8BD9625BF::get_offset_of_m_DissipationDistance_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3733;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3733 = { sizeof (RepeatMode_t3EE130ABD9F284BF06DE68C86E74709D724803FD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3733[3] = 
{
	RepeatMode_t3EE130ABD9F284BF06DE68C86E74709D724803FD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3734;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3734 = { sizeof (SignalSource_t9CDE1AB5C86593ACD3888A3C580CE5F19358125E), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3734[3] = 
{
	SignalSource_t9CDE1AB5C86593ACD3888A3C580CE5F19358125E::get_offset_of_m_Def_0(),
	SignalSource_t9CDE1AB5C86593ACD3888A3C580CE5F19358125E::get_offset_of_m_Velocity_1(),
	SignalSource_t9CDE1AB5C86593ACD3888A3C580CE5F19358125E::get_offset_of_m_StartTimeOffset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3735;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3735 = { sizeof (CinemachineImpulseListener_tB33F46B5F828297AAC0EDFF5515FD7D49051DFDD), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3735[3] = 
{
	CinemachineImpulseListener_tB33F46B5F828297AAC0EDFF5515FD7D49051DFDD::get_offset_of_m_ChannelMask_7(),
	CinemachineImpulseListener_tB33F46B5F828297AAC0EDFF5515FD7D49051DFDD::get_offset_of_m_Gain_8(),
	CinemachineImpulseListener_tB33F46B5F828297AAC0EDFF5515FD7D49051DFDD::get_offset_of_m_Use2DDistance_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3736;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3736 = { sizeof (CinemachineImpulseEnvelopePropertyAttribute_tC41425006E83643EA2F259F979894D82B6FC0A0E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3737;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3737 = { sizeof (CinemachineImpulseChannelPropertyAttribute_t6BF9C93F9B8D1B758BABD2BDE3DD84D5D6368C0A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3738;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3738 = { sizeof (CinemachineImpulseManager_tC1F7AA07F19AE36A04DB0AD50E19B7E256BEEF67), -1, sizeof(CinemachineImpulseManager_tC1F7AA07F19AE36A04DB0AD50E19B7E256BEEF67_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3738[4] = 
{
	CinemachineImpulseManager_tC1F7AA07F19AE36A04DB0AD50E19B7E256BEEF67_StaticFields::get_offset_of_sInstance_0(),
	0,
	CinemachineImpulseManager_tC1F7AA07F19AE36A04DB0AD50E19B7E256BEEF67::get_offset_of_m_ExpiredEvents_2(),
	CinemachineImpulseManager_tC1F7AA07F19AE36A04DB0AD50E19B7E256BEEF67::get_offset_of_m_ActiveEvents_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3739;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3739 = { sizeof (EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E)+ sizeof (RuntimeObject), sizeof(EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3739[7] = 
{
	EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E::get_offset_of_m_AttackShape_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E::get_offset_of_m_DecayShape_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E::get_offset_of_m_AttackTime_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E::get_offset_of_m_SustainTime_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E::get_offset_of_m_DecayTime_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E::get_offset_of_m_ScaleWithImpact_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EnvelopeDefinition_t7C1D4ACDAD754B6A430786BEDA402AACE97CEA2E::get_offset_of_m_HoldForever_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3740;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3740 = { sizeof (ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3740[9] = 
{
	ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C::get_offset_of_m_StartTime_0(),
	ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C::get_offset_of_m_Envelope_1(),
	ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C::get_offset_of_m_SignalSource_2(),
	ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C::get_offset_of_m_Position_3(),
	ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C::get_offset_of_m_Radius_4(),
	ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C::get_offset_of_m_DirectionMode_5(),
	ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C::get_offset_of_m_Channel_6(),
	ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C::get_offset_of_m_DissipationMode_7(),
	ImpulseEvent_t705C828C8321B3F5BE7AFFEA15ED6311BEDC8E5C::get_offset_of_m_DissipationDistance_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3741;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3741 = { sizeof (DirectionMode_t1C0606ECAAD200158217A6BE23476C6FDF0178F6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3741[3] = 
{
	DirectionMode_t1C0606ECAAD200158217A6BE23476C6FDF0178F6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3742;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3742 = { sizeof (DissipationMode_t768C05D9CFC3FE4D58AEE5AAC147A88E30DC160A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3742[4] = 
{
	DissipationMode_t768C05D9CFC3FE4D58AEE5AAC147A88E30DC160A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3743;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3743 = { sizeof (CinemachineImpulseSource_t0939CD50852A448FDE8E55DDB3050358180B10ED), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3743[1] = 
{
	CinemachineImpulseSource_t0939CD50852A448FDE8E55DDB3050358180B10ED::get_offset_of_m_ImpulseDefinition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3744;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3744 = { sizeof (CinemachineDebug_t0D9E17BEA16D2F423F05274824D6BBB6A2B4993E), -1, sizeof(CinemachineDebug_t0D9E17BEA16D2F423F05274824D6BBB6A2B4993E_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3744[3] = 
{
	CinemachineDebug_t0D9E17BEA16D2F423F05274824D6BBB6A2B4993E_StaticFields::get_offset_of_mClients_0(),
	CinemachineDebug_t0D9E17BEA16D2F423F05274824D6BBB6A2B4993E_StaticFields::get_offset_of_OnGUIHandlers_1(),
	CinemachineDebug_t0D9E17BEA16D2F423F05274824D6BBB6A2B4993E_StaticFields::get_offset_of_mAvailableStringBuilders_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3745;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3745 = { sizeof (OnGUIDelegate_tF9317001AB4E703C8439EFD7987552E90C476F72), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3746;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3746 = { 0, 0, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3746[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3747;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3747 = { sizeof (GaussianWindow1D_Vector3_t3D2CA98CAA335A6CA30F2F3E51CEC6FA69C1EC48), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3748;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3748 = { sizeof (GaussianWindow1D_Quaternion_t7B6418660E5E1E7E1CDC2E2AD08FADD17C4EECC3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3749;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3749 = { sizeof (GaussianWindow1D_CameraRotation_t88B75023ED9125C4BE06144C20215CD10666641D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3750;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3750 = { sizeof (PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3750[6] = 
{
	PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223::get_offset_of_m_Position_0(),
	PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223::get_offset_of_m_Velocity_1(),
	PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223::get_offset_of_m_Accel_2(),
	PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223::get_offset_of_mLastVelAddedTime_3(),
	0,
	PositionPredictor_t8A79BF0A72B44EF8AC2F3B0C0D51A399C5C7E223::get_offset_of_mSmoothing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3751;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3751 = { sizeof (Damper_t9BB3B28320C2E100B659D1237F6064551C846A91), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3751[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3752;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3752 = { sizeof (HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4), -1, sizeof(HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3752[9] = 
{
	HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4::get_offset_of_mHistory_0(),
	HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4::get_offset_of_mTop_1(),
	HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4::get_offset_of_mBottom_2(),
	HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4::get_offset_of_mCount_3(),
	HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4::get_offset_of_mHeadingSum_4(),
	HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4::get_offset_of_mWeightSum_5(),
	HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4::get_offset_of_mWeightTime_6(),
	HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4::get_offset_of_mLastGoodHeading_7(),
	HeadingTracker_t5957804552F409AFD9CCBEA177E09CC3753873D4_StaticFields::get_offset_of_mDecayExponent_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3753;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3753 = { sizeof (Item_t3B6B467A1D39665462BC0A703E543496169303B4)+ sizeof (RuntimeObject), sizeof(Item_t3B6B467A1D39665462BC0A703E543496169303B4 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3753[3] = 
{
	Item_t3B6B467A1D39665462BC0A703E543496169303B4::get_offset_of_velocity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Item_t3B6B467A1D39665462BC0A703E543496169303B4::get_offset_of_weight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Item_t3B6B467A1D39665462BC0A703E543496169303B4::get_offset_of_time_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3754;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3754 = { sizeof (SplineHelpers_tC88AED5435AA4C67E6039B93997091214A2E0858), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3755;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3755 = { sizeof (UnityVectorExtensions_t8B7D79BD392965E48AEA9B91EE25FD09244E69B3), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3755[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3756;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3756 = { sizeof (UnityQuaternionExtensions_tCED4E5F8D5E7C212E7CE30310E4123D534075ABB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3757;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3757 = { sizeof (UnityRectExtensions_tD2D2C115AA54BE180CE78EF2BB37D1EC18A36847), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3758;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3758 = { sizeof (U3CPrivateImplementationDetailsU3E_tF5473A948C7ED035EB072C0E8916F6AC30F9643E), -1, sizeof(U3CPrivateImplementationDetailsU3E_tF5473A948C7ED035EB072C0E8916F6AC30F9643E_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3758[1] = 
{
	U3CPrivateImplementationDetailsU3E_tF5473A948C7ED035EB072C0E8916F6AC30F9643E_StaticFields::get_offset_of_U378517443912BB49729313EC23065D9970ABC80E3_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3759;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3759 = { sizeof (__StaticArrayInitTypeSizeU3D12_t424CC67AAA71D9952D514D413907E974A287EC65)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t424CC67AAA71D9952D514D413907E974A287EC65 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3760;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3760 = { sizeof (U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3761;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3761 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3762;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3762 = { sizeof (InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3762[14] = 
{
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE::get_offset_of_U3CassetU3Ek__BackingField_0(),
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE::get_offset_of_m_Moviment_1(),
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE::get_offset_of_m_MovimentActionsCallbackInterface_2(),
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE::get_offset_of_m_Moviment_HorizontalMoviment_3(),
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE::get_offset_of_m_Moviment_Vertical_4(),
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE::get_offset_of_m_Moviment_Jump_5(),
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE::get_offset_of_m_Moviment_Crounch_6(),
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE::get_offset_of_m_Interaction_7(),
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE::get_offset_of_m_InteractionActionsCallbackInterface_8(),
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE::get_offset_of_m_Interaction_BasicInteraction_9(),
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE::get_offset_of_m_Pause_10(),
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE::get_offset_of_m_PauseActionsCallbackInterface_11(),
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE::get_offset_of_m_Pause_PauseTrigger_12(),
	InputSettings_tD8704B21157A8113329571D3D084EFA906AB23AE::get_offset_of_m_PCSchemeIndex_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3763;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3763 = { sizeof (MovimentActions_t043B55B8C65750A7ED088DFE82D773D6F757DDFA)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3763[1] = 
{
	MovimentActions_t043B55B8C65750A7ED088DFE82D773D6F757DDFA::get_offset_of_m_Wrapper_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3764;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3764 = { sizeof (InteractionActions_tA55A474A3250F62C19F1B26508F484D589100F02)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3764[1] = 
{
	InteractionActions_tA55A474A3250F62C19F1B26508F484D589100F02::get_offset_of_m_Wrapper_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3765;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3765 = { sizeof (PauseActions_tC24EA586A943A15DF870FD096027ED46912BC0AE)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3765[1] = 
{
	PauseActions_tC24EA586A943A15DF870FD096027ED46912BC0AE::get_offset_of_m_Wrapper_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3766;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3766 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3767;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3767 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3768;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3768 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3769;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3769 = { sizeof (CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3769[6] = 
{
	CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039::get_offset_of_characterRB_4(),
	CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039::get_offset_of_characterRenderer_5(),
	CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039::get_offset_of_characterAnimator_6(),
	CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039::get_offset_of_velocity_7(),
	CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039::get_offset_of_grabVelocity_8(),
	CharacterBase_t977B913DC310877BB378E2D9388F7C56CCBAC039::get_offset_of_duringMoviment_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3770;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3770 = { sizeof (EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3770[4] = 
{
	EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552::get_offset_of_targetPoints_10(),
	EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552::get_offset_of_timeInPatrol_11(),
	EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552::get_offset_of_currentTarget_12(),
	EnemyBehaviour_t648E850E05E6F6005CF986434049774DFF8ED552::get_offset_of_waiting_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3771;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3771 = { sizeof (U3CSetNextTargetU3Ed__8_tA8539BE570080CC8BD09B106B403201FF4864AC9)+ sizeof (RuntimeObject), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3771[4] = 
{
	U3CSetNextTargetU3Ed__8_tA8539BE570080CC8BD09B106B403201FF4864AC9::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSetNextTargetU3Ed__8_tA8539BE570080CC8BD09B106B403201FF4864AC9::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSetNextTargetU3Ed__8_tA8539BE570080CC8BD09B106B403201FF4864AC9::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSetNextTargetU3Ed__8_tA8539BE570080CC8BD09B106B403201FF4864AC9::get_offset_of_U3CU3Eu__1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3772;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3772 = { sizeof (PlayerTrigger_t42EAB7463A59B8DC10C15914485B2EDC5F2C3A7A), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3772[2] = 
{
	PlayerTrigger_t42EAB7463A59B8DC10C15914485B2EDC5F2C3A7A::get_offset_of_layerIgnore_4(),
	PlayerTrigger_t42EAB7463A59B8DC10C15914485B2EDC5F2C3A7A::get_offset_of_rayOrigin_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3773;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3773 = { sizeof (TargetPoint_t5845B3F89662C4A8770F1F98FE94FCCF588DC66A), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3773[1] = 
{
	TargetPoint_t5845B3F89662C4A8770F1F98FE94FCCF588DC66A::get_offset_of_ignoreColliders_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3774;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3774 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3775;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3775 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3776;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3776 = { sizeof (Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3776[9] = 
{
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_playerInput_10(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_horizontal_11(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_vertical_12(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_pushing_13(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_crounched_14(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_timerToPush_15(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_movimentPermission_16(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_allComponents_17(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_currentGrab_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3777;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3777 = { sizeof (PlayerComponent_t50D5545C539262D84FF8A719CAEA023A890F4839), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3777[3] = 
{
	PlayerComponent_t50D5545C539262D84FF8A719CAEA023A890F4839::get_offset_of_initialized_4(),
	PlayerComponent_t50D5545C539262D84FF8A719CAEA023A890F4839::get_offset_of_player_5(),
	PlayerComponent_t50D5545C539262D84FF8A719CAEA023A890F4839::get_offset_of_firstEnable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3778;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3778 = { sizeof (PlayerCrounch_t5C13902ED3E9F51C9FD9471DB75276BD977A6D00), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3778[2] = 
{
	PlayerCrounch_t5C13902ED3E9F51C9FD9471DB75276BD977A6D00::get_offset_of_normalCollider_7(),
	PlayerCrounch_t5C13902ED3E9F51C9FD9471DB75276BD977A6D00::get_offset_of_CrounchCollider_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3779;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3779 = { sizeof (PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3779[6] = 
{
	PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB::get_offset_of_inputMap_7(),
	PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB::get_offset_of_JumpAction_8(),
	PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB::get_offset_of_CrounchAction_9(),
	PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB::get_offset_of_UnCrounchAction_10(),
	PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB::get_offset_of_GrabAction_11(),
	PlayerInput_t8466968C2917EA5461AA1309B184C50B425448DB::get_offset_of_UnGrabAction_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3780;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3780 = { sizeof (PlayerInteraction_t7BE361C67931CA1C21B195DD20AEBAC3EE777A16), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3780[2] = 
{
	PlayerInteraction_t7BE361C67931CA1C21B195DD20AEBAC3EE777A16::get_offset_of_interactionLayer_7(),
	PlayerInteraction_t7BE361C67931CA1C21B195DD20AEBAC3EE777A16::get_offset_of_grabPossibilities_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3781;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3781 = { sizeof (PlayerJump_t17597109FBE3C3969284027CBAA038DC6ECBF20D), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3781[3] = 
{
	PlayerJump_t17597109FBE3C3969284027CBAA038DC6ECBF20D::get_offset_of_jumpForce_7(),
	PlayerJump_t17597109FBE3C3969284027CBAA038DC6ECBF20D::get_offset_of_jumpForceHigh_8(),
	PlayerJump_t17597109FBE3C3969284027CBAA038DC6ECBF20D::get_offset_of_playerInteraction_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3782;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3782 = { sizeof (Coin_tE4CAC42A9D372B0FC66B4739E35B4CBD0F0CDA59), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3783;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3783 = { sizeof (Collectable_tB9D507F84CB6880BA5EE1A6A2589203264F29651), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3783[5] = 
{
	Collectable_tB9D507F84CB6880BA5EE1A6A2589203264F29651::get_offset_of_id_4(),
	Collectable_tB9D507F84CB6880BA5EE1A6A2589203264F29651::get_offset_of_type_5(),
	Collectable_tB9D507F84CB6880BA5EE1A6A2589203264F29651::get_offset_of_collectableRenderer_6(),
	Collectable_tB9D507F84CB6880BA5EE1A6A2589203264F29651::get_offset_of_information_7(),
	Collectable_tB9D507F84CB6880BA5EE1A6A2589203264F29651::get_offset_of_propertyBlock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3784;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3784 = { sizeof (Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3784[3] = 
{
	Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3::get_offset_of_destiny_7(),
	Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3::get_offset_of_subTarget_8(),
	Grabbable_tEF4468E2138DBAAE6284C2C7DF75E81576044BB3::get_offset_of_playerScript_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3785;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3785 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3786;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3786 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3787;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3787 = { sizeof (InteractableBase_t5E0859747DB47E62279D12B7A14E157EE5DF9A08), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3787[3] = 
{
	InteractableBase_t5E0859747DB47E62279D12B7A14E157EE5DF9A08::get_offset_of_rigidBody_4(),
	InteractableBase_t5E0859747DB47E62279D12B7A14E157EE5DF9A08::get_offset_of_interactableLayer_5(),
	InteractableBase_t5E0859747DB47E62279D12B7A14E157EE5DF9A08::get_offset_of_reachDestiny_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3788;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3788 = { sizeof (ReachChecker_t0D0BF213B48D49EA8186F575FC8475BBD03C556A), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3788[1] = 
{
	ReachChecker_t0D0BF213B48D49EA8186F575FC8475BBD03C556A::get_offset_of_grabbable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3789;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3789 = { sizeof (CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3789[3] = 
{
	CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142::get_offset_of_points_4(),
	CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142::get_offset_of_cameraVelocity_5(),
	CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142::get_offset_of_current_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3790;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3790 = { sizeof (GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3790[6] = 
{
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_collectableData_5(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_loadingImage_6(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_pauseScreen_7(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_endGameScreen_8(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_poolSystem_9(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_inputMap_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3791;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3791 = { sizeof (U3CLoadSceneU3Ed__16_t72200372913778B5D70C4802329F3D4396CC3A32), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3791[4] = 
{
	U3CLoadSceneU3Ed__16_t72200372913778B5D70C4802329F3D4396CC3A32::get_offset_of_U3CU3E1__state_0(),
	U3CLoadSceneU3Ed__16_t72200372913778B5D70C4802329F3D4396CC3A32::get_offset_of_U3CU3E2__current_1(),
	U3CLoadSceneU3Ed__16_t72200372913778B5D70C4802329F3D4396CC3A32::get_offset_of_U3CU3E4__this_2(),
	U3CLoadSceneU3Ed__16_t72200372913778B5D70C4802329F3D4396CC3A32::get_offset_of_nextMap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3792;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3792 = { sizeof (NextCameraPoint_tDE95DD7250288FF86CA500ABBEBF1977EECE6ED2), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3792[1] = 
{
	NextCameraPoint_tDE95DD7250288FF86CA500ABBEBF1977EECE6ED2::get_offset_of_TriggerEvent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3793;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3793 = { sizeof (PoolSystem_t1805A8BEF518DCC1DA9D16E7932112C3692507A4), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3793[1] = 
{
	PoolSystem_t1805A8BEF518DCC1DA9D16E7932112C3692507A4::get_offset_of_collectablePoolList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3794;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3794 = { sizeof (Scenery_tF2FD47C991A1BAE0F47D9DCAC99FD105988BD68A), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3794[1] = 
{
	Scenery_tF2FD47C991A1BAE0F47D9DCAC99FD105988BD68A::get_offset_of_nextMap_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3795;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3795 = { sizeof (PauseController_t60C0B1AD0DA3C383DDE9B236621896374C1AA990), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3796;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3796 = { sizeof (Informations_tADA074FFFA53270CEFD43C54FEE73B957720E1C2), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3796[1] = 
{
	Informations_tADA074FFFA53270CEFD43C54FEE73B957720E1C2::get_offset_of_coins_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3797;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3797 = { sizeof (PlayerInformations_t0E38B6119ADAB2C0A72D9F803B7555346F386895), -1, sizeof(PlayerInformations_t0E38B6119ADAB2C0A72D9F803B7555346F386895_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3797[2] = 
{
	PlayerInformations_t0E38B6119ADAB2C0A72D9F803B7555346F386895_StaticFields::get_offset_of_informations_0(),
	PlayerInformations_t0E38B6119ADAB2C0A72D9F803B7555346F386895_StaticFields::get_offset_of_completedFirstPuzzle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3798;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3798 = { sizeof (ProgressPoint_tB6B11868518E3F543DB66FDDBB48669586D72341), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3799;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3799 = { sizeof (ServiceLocator_t40441F4445217D8FAEE52BA1EA2E598D93FE8E5A), -1, sizeof(ServiceLocator_t40441F4445217D8FAEE52BA1EA2E598D93FE8E5A_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3799[1] = 
{
	ServiceLocator_t40441F4445217D8FAEE52BA1EA2E598D93FE8E5A_StaticFields::get_offset_of_Services_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3800;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3800 = { sizeof (CoinUpdater_t8B38B35C3D29397BA3F025A713AB6D953621A7ED), -1, sizeof(CoinUpdater_t8B38B35C3D29397BA3F025A713AB6D953621A7ED_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3800[1] = 
{
	CoinUpdater_t8B38B35C3D29397BA3F025A713AB6D953621A7ED_StaticFields::get_offset_of_coinTMP_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3801;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3801 = { sizeof (EndGameScreen_tA1957EC7E20256B5515F2ABF8D752AD582C297FF), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3801[2] = 
{
	EndGameScreen_tA1957EC7E20256B5515F2ABF8D752AD582C297FF::get_offset_of_messageTxt_4(),
	EndGameScreen_tA1957EC7E20256B5515F2ABF8D752AD582C297FF::get_offset_of_sucessTemp_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3802;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3802 = { sizeof (InteractionUpdater_t22CFD12453B6ACA4814BAB4EEF6FB95D2C47C814), -1, sizeof(InteractionUpdater_t22CFD12453B6ACA4814BAB4EEF6FB95D2C47C814_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3802[1] = 
{
	InteractionUpdater_t22CFD12453B6ACA4814BAB4EEF6FB95D2C47C814_StaticFields::get_offset_of_actionTMP_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3803;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3803 = { sizeof (UpdaterInteractionHUD_t0095EF4C07DC1491DE8223358AFA965931DA0717), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3803[1] = 
{
	UpdaterInteractionHUD_t0095EF4C07DC1491DE8223358AFA965931DA0717::get_offset_of_count_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3804;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3804 = { sizeof (WinScreen_t3859A82ACD171227DDD08CA3F462A8D75B46E869), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3804[1] = 
{
	WinScreen_t3859A82ACD171227DDD08CA3F462A8D75B46E869::get_offset_of_coinTMP_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3805;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3805 = { 0, 0, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3805[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3806;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3806 = { sizeof (CollectableType_t0EDB217F53835832065358E8C732FE7953BF4294)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3806[5] = 
{
	CollectableType_t0EDB217F53835832065358E8C732FE7953BF4294::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3807;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3807 = { sizeof (CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3807[4] = 
{
	CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210::get_offset_of_useFixedColor_0(),
	CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210::get_offset_of_fixedColor_1(),
	CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210::get_offset_of_type_2(),
	CollectableConfiguration_t32A0DCEF57CDBE376F4A8B86A820A944DC456210::get_offset_of_value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3808;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3808 = { sizeof (CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3808[5] = 
{
	CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7::get_offset_of_collectableCoin_4(),
	CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7::get_offset_of_collectablePowerUp_5(),
	CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7::get_offset_of_collectableHealth_6(),
	CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7::get_offset_of_collectableMunition_7(),
	CollectableData_tD3A160DA95EC63E92C96C4ABB3C650A6C12189E7::get_offset_of_colorPossibilities_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3809;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3809 = { sizeof (SaveSystem_t3C4D994BCE04F61BE8CAD1966E2A334BBCF5DFCF), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3809[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3810;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3810 = { sizeof (U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3811;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3811 = { sizeof (WaterBasic_t94B54D7222D89D61D4A2E0E8CE3CA4631BA11EEB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3812;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3812 = { sizeof (Displace_tE71543D1BC659AE19223A155A9CE84FB1110DDE3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3813;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3813 = { sizeof (GerstnerDisplace_tE6CD37BD54D62274FF32AA2693237AE91731F01F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3814;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3814 = { sizeof (MeshContainer_tAFAB4888889EABF26999DFC66A4122B9C5A084EE), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3814[3] = 
{
	MeshContainer_tAFAB4888889EABF26999DFC66A4122B9C5A084EE::get_offset_of_mesh_0(),
	MeshContainer_tAFAB4888889EABF26999DFC66A4122B9C5A084EE::get_offset_of_vertices_1(),
	MeshContainer_tAFAB4888889EABF26999DFC66A4122B9C5A084EE::get_offset_of_normals_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3815;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3815 = { sizeof (PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3815[9] = 
{
	PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B::get_offset_of_reflectionMask_4(),
	PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B::get_offset_of_reflectSkybox_5(),
	PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B::get_offset_of_clearColor_6(),
	PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B::get_offset_of_reflectionSampler_7(),
	PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B::get_offset_of_clipPlaneOffset_8(),
	PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B::get_offset_of_m_Oldpos_9(),
	PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B::get_offset_of_m_ReflectionCamera_10(),
	PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B::get_offset_of_m_SharedMaterial_11(),
	PlanarReflection_t7D370C031AE2ED0AE680B534E64D211B7C43739B::get_offset_of_m_HelperCameras_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3816;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3816 = { sizeof (SpecularLighting_t84EA6B1150A66C2D6D7395842F390FA7BFEC2A97), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3816[2] = 
{
	SpecularLighting_t84EA6B1150A66C2D6D7395842F390FA7BFEC2A97::get_offset_of_specularLight_4(),
	SpecularLighting_t84EA6B1150A66C2D6D7395842F390FA7BFEC2A97::get_offset_of_m_WaterBase_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3817;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3817 = { sizeof (Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D), -1, sizeof(Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3817[14] = 
{
	Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D::get_offset_of_waterMode_4(),
	Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D::get_offset_of_disablePixelLights_5(),
	Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D::get_offset_of_textureSize_6(),
	Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D::get_offset_of_clipPlaneOffset_7(),
	Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D::get_offset_of_reflectLayers_8(),
	Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D::get_offset_of_refractLayers_9(),
	Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D::get_offset_of_m_ReflectionCameras_10(),
	Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D::get_offset_of_m_RefractionCameras_11(),
	Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D::get_offset_of_m_ReflectionTexture_12(),
	Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D::get_offset_of_m_RefractionTexture_13(),
	Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D::get_offset_of_m_HardwareWaterSupport_14(),
	Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D::get_offset_of_m_OldReflectionTextureSize_15(),
	Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D::get_offset_of_m_OldRefractionTextureSize_16(),
	Water_t93B591EE42BFF9D9DBBDF0148B6E24551AF8300D_StaticFields::get_offset_of_s_InsideWater_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3818;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3818 = { sizeof (WaterMode_t1799A7D5CF5C687BBCB577D86D980CE1E42AE102)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3818[4] = 
{
	WaterMode_t1799A7D5CF5C687BBCB577D86D980CE1E42AE102::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3819;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3819 = { sizeof (WaterQuality_t06C96FA87F57ACDA33F4DE38CADF3BB167ED3FAF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3819[4] = 
{
	WaterQuality_t06C96FA87F57ACDA33F4DE38CADF3BB167ED3FAF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3820;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3820 = { sizeof (WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3820[3] = 
{
	WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565::get_offset_of_sharedMaterial_4(),
	WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565::get_offset_of_waterQuality_5(),
	WaterBase_t1C79083337563E3551C69C4375ACE84C2B7EE565::get_offset_of_edgeBlend_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3821;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize3821 = { sizeof (WaterTile_t9F836DCF3384AB4ACC5BFF1EC7C57ED3D7FCFBC7), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3821[2] = 
{
	WaterTile_t9F836DCF3384AB4ACC5BFF1EC7C57ED3D7FCFBC7::get_offset_of_reflection_4(),
	WaterTile_t9F836DCF3384AB4ACC5BFF1EC7C57ED3D7FCFBC7::get_offset_of_waterBase_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
