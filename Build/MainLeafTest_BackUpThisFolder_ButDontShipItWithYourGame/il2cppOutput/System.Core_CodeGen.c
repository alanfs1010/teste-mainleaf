﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000003 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000004 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000006 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000008 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000009 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000A TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000B System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000C TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000D TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000E TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x00000010 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000011 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000012 System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000013 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000014 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000015 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000016 System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x00000017 TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x00000018 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x00000019 System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x0000001A System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x0000001B System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x0000001C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000001D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001E System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000001F System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000020 System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000021 System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000022 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x00000023 System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x00000024 System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x00000025 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000026 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000027 System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000028 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x00000029 System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x0000002A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000002B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002C System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000002D System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x0000002E System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x0000002F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000030 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000031 System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000032 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Clone()
// 0x00000033 System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Dispose()
// 0x00000034 System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000037 System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000038 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Clone()
// 0x00000039 System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2::MoveNext()
// 0x0000003A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003C System.Void System.Linq.Enumerable/WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003D System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Clone()
// 0x0000003E System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2::MoveNext()
// 0x0000003F System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000041 System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x00000042 System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000043 System.Void System.Linq.Enumerable/<>c__DisplayClass7_0`3::.ctor()
// 0x00000044 TResult System.Linq.Enumerable/<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000045 System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x00000046 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000047 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000048 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000049 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000004A System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000004B System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x0000004C System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::.ctor(System.Int32)
// 0x0000004D System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x0000004E System.Boolean System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::MoveNext()
// 0x0000004F TElement System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000050 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000051 System.Object System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000052 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000053 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000054 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000055 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000056 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x00000057 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000058 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000059 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x0000005A System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x0000005B System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x0000005C System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x0000005D TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000005E System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000005F System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000060 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000061 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000062 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000063 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000064 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000065 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000066 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000067 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000068 System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000069 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000006A System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000006B System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000006C System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000006D System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000006E System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000006F System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000070 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000071 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000072 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000073 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000074 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000075 System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000076 System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x00000077 System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x00000078 T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x00000079 System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000007A System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[122] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[122] = 
{
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[43] = 
{
	{ 0x02000004, { 66, 4 } },
	{ 0x02000005, { 70, 9 } },
	{ 0x02000006, { 81, 7 } },
	{ 0x02000007, { 90, 10 } },
	{ 0x02000008, { 102, 11 } },
	{ 0x02000009, { 116, 9 } },
	{ 0x0200000A, { 128, 12 } },
	{ 0x0200000B, { 143, 1 } },
	{ 0x0200000C, { 144, 2 } },
	{ 0x0200000D, { 146, 2 } },
	{ 0x0200000F, { 148, 3 } },
	{ 0x02000010, { 153, 5 } },
	{ 0x02000011, { 158, 7 } },
	{ 0x02000012, { 165, 3 } },
	{ 0x02000013, { 168, 7 } },
	{ 0x02000014, { 175, 4 } },
	{ 0x02000015, { 179, 21 } },
	{ 0x02000017, { 200, 2 } },
	{ 0x06000004, { 0, 10 } },
	{ 0x06000005, { 10, 10 } },
	{ 0x06000006, { 20, 5 } },
	{ 0x06000007, { 25, 5 } },
	{ 0x06000008, { 30, 2 } },
	{ 0x06000009, { 32, 1 } },
	{ 0x0600000A, { 33, 3 } },
	{ 0x0600000B, { 36, 2 } },
	{ 0x0600000C, { 38, 4 } },
	{ 0x0600000D, { 42, 4 } },
	{ 0x0600000E, { 46, 3 } },
	{ 0x0600000F, { 49, 1 } },
	{ 0x06000010, { 50, 1 } },
	{ 0x06000011, { 51, 3 } },
	{ 0x06000012, { 54, 3 } },
	{ 0x06000013, { 57, 2 } },
	{ 0x06000014, { 59, 2 } },
	{ 0x06000015, { 61, 5 } },
	{ 0x06000025, { 79, 2 } },
	{ 0x0600002A, { 88, 2 } },
	{ 0x0600002F, { 100, 2 } },
	{ 0x06000035, { 113, 3 } },
	{ 0x0600003A, { 125, 3 } },
	{ 0x0600003F, { 140, 3 } },
	{ 0x0600004A, { 151, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[202] = 
{
	{ (Il2CppRGCTXDataType)2, 25248 },
	{ (Il2CppRGCTXDataType)3, 22790 },
	{ (Il2CppRGCTXDataType)2, 25249 },
	{ (Il2CppRGCTXDataType)2, 25250 },
	{ (Il2CppRGCTXDataType)3, 22791 },
	{ (Il2CppRGCTXDataType)2, 25251 },
	{ (Il2CppRGCTXDataType)2, 25252 },
	{ (Il2CppRGCTXDataType)3, 22792 },
	{ (Il2CppRGCTXDataType)2, 25253 },
	{ (Il2CppRGCTXDataType)3, 22793 },
	{ (Il2CppRGCTXDataType)2, 25254 },
	{ (Il2CppRGCTXDataType)3, 22794 },
	{ (Il2CppRGCTXDataType)2, 25255 },
	{ (Il2CppRGCTXDataType)2, 25256 },
	{ (Il2CppRGCTXDataType)3, 22795 },
	{ (Il2CppRGCTXDataType)2, 25257 },
	{ (Il2CppRGCTXDataType)2, 25258 },
	{ (Il2CppRGCTXDataType)3, 22796 },
	{ (Il2CppRGCTXDataType)2, 25259 },
	{ (Il2CppRGCTXDataType)3, 22797 },
	{ (Il2CppRGCTXDataType)2, 25260 },
	{ (Il2CppRGCTXDataType)3, 22798 },
	{ (Il2CppRGCTXDataType)3, 22799 },
	{ (Il2CppRGCTXDataType)2, 18197 },
	{ (Il2CppRGCTXDataType)3, 22800 },
	{ (Il2CppRGCTXDataType)2, 25261 },
	{ (Il2CppRGCTXDataType)3, 22801 },
	{ (Il2CppRGCTXDataType)3, 22802 },
	{ (Il2CppRGCTXDataType)2, 18204 },
	{ (Il2CppRGCTXDataType)3, 22803 },
	{ (Il2CppRGCTXDataType)2, 25262 },
	{ (Il2CppRGCTXDataType)3, 22804 },
	{ (Il2CppRGCTXDataType)3, 22805 },
	{ (Il2CppRGCTXDataType)2, 25263 },
	{ (Il2CppRGCTXDataType)3, 22806 },
	{ (Il2CppRGCTXDataType)3, 22807 },
	{ (Il2CppRGCTXDataType)2, 18219 },
	{ (Il2CppRGCTXDataType)3, 22808 },
	{ (Il2CppRGCTXDataType)2, 25264 },
	{ (Il2CppRGCTXDataType)2, 25265 },
	{ (Il2CppRGCTXDataType)2, 18220 },
	{ (Il2CppRGCTXDataType)2, 25266 },
	{ (Il2CppRGCTXDataType)2, 25267 },
	{ (Il2CppRGCTXDataType)2, 25268 },
	{ (Il2CppRGCTXDataType)2, 18222 },
	{ (Il2CppRGCTXDataType)2, 25269 },
	{ (Il2CppRGCTXDataType)2, 18224 },
	{ (Il2CppRGCTXDataType)2, 25270 },
	{ (Il2CppRGCTXDataType)3, 22809 },
	{ (Il2CppRGCTXDataType)2, 25271 },
	{ (Il2CppRGCTXDataType)2, 18229 },
	{ (Il2CppRGCTXDataType)2, 18231 },
	{ (Il2CppRGCTXDataType)2, 25272 },
	{ (Il2CppRGCTXDataType)3, 22810 },
	{ (Il2CppRGCTXDataType)2, 18234 },
	{ (Il2CppRGCTXDataType)2, 25273 },
	{ (Il2CppRGCTXDataType)3, 22811 },
	{ (Il2CppRGCTXDataType)2, 25274 },
	{ (Il2CppRGCTXDataType)2, 18237 },
	{ (Il2CppRGCTXDataType)2, 25275 },
	{ (Il2CppRGCTXDataType)3, 22812 },
	{ (Il2CppRGCTXDataType)3, 22813 },
	{ (Il2CppRGCTXDataType)2, 25276 },
	{ (Il2CppRGCTXDataType)2, 18241 },
	{ (Il2CppRGCTXDataType)2, 25277 },
	{ (Il2CppRGCTXDataType)2, 18243 },
	{ (Il2CppRGCTXDataType)3, 22814 },
	{ (Il2CppRGCTXDataType)3, 22815 },
	{ (Il2CppRGCTXDataType)2, 18246 },
	{ (Il2CppRGCTXDataType)3, 22816 },
	{ (Il2CppRGCTXDataType)3, 22817 },
	{ (Il2CppRGCTXDataType)2, 18258 },
	{ (Il2CppRGCTXDataType)2, 25278 },
	{ (Il2CppRGCTXDataType)3, 22818 },
	{ (Il2CppRGCTXDataType)3, 22819 },
	{ (Il2CppRGCTXDataType)2, 18260 },
	{ (Il2CppRGCTXDataType)2, 25133 },
	{ (Il2CppRGCTXDataType)3, 22820 },
	{ (Il2CppRGCTXDataType)3, 22821 },
	{ (Il2CppRGCTXDataType)2, 25279 },
	{ (Il2CppRGCTXDataType)3, 22822 },
	{ (Il2CppRGCTXDataType)3, 22823 },
	{ (Il2CppRGCTXDataType)2, 18270 },
	{ (Il2CppRGCTXDataType)2, 25280 },
	{ (Il2CppRGCTXDataType)3, 22824 },
	{ (Il2CppRGCTXDataType)3, 22825 },
	{ (Il2CppRGCTXDataType)3, 21707 },
	{ (Il2CppRGCTXDataType)3, 22826 },
	{ (Il2CppRGCTXDataType)2, 25281 },
	{ (Il2CppRGCTXDataType)3, 22827 },
	{ (Il2CppRGCTXDataType)3, 22828 },
	{ (Il2CppRGCTXDataType)2, 18282 },
	{ (Il2CppRGCTXDataType)2, 25282 },
	{ (Il2CppRGCTXDataType)3, 22829 },
	{ (Il2CppRGCTXDataType)3, 22830 },
	{ (Il2CppRGCTXDataType)3, 22831 },
	{ (Il2CppRGCTXDataType)3, 22832 },
	{ (Il2CppRGCTXDataType)3, 22833 },
	{ (Il2CppRGCTXDataType)3, 21713 },
	{ (Il2CppRGCTXDataType)3, 22834 },
	{ (Il2CppRGCTXDataType)2, 25283 },
	{ (Il2CppRGCTXDataType)3, 22835 },
	{ (Il2CppRGCTXDataType)3, 22836 },
	{ (Il2CppRGCTXDataType)2, 18295 },
	{ (Il2CppRGCTXDataType)2, 25284 },
	{ (Il2CppRGCTXDataType)3, 22837 },
	{ (Il2CppRGCTXDataType)3, 22838 },
	{ (Il2CppRGCTXDataType)2, 18297 },
	{ (Il2CppRGCTXDataType)2, 25285 },
	{ (Il2CppRGCTXDataType)3, 22839 },
	{ (Il2CppRGCTXDataType)3, 22840 },
	{ (Il2CppRGCTXDataType)2, 25286 },
	{ (Il2CppRGCTXDataType)3, 22841 },
	{ (Il2CppRGCTXDataType)3, 22842 },
	{ (Il2CppRGCTXDataType)2, 25287 },
	{ (Il2CppRGCTXDataType)3, 22843 },
	{ (Il2CppRGCTXDataType)3, 22844 },
	{ (Il2CppRGCTXDataType)2, 18312 },
	{ (Il2CppRGCTXDataType)2, 25288 },
	{ (Il2CppRGCTXDataType)3, 22845 },
	{ (Il2CppRGCTXDataType)3, 22846 },
	{ (Il2CppRGCTXDataType)3, 22847 },
	{ (Il2CppRGCTXDataType)3, 21724 },
	{ (Il2CppRGCTXDataType)2, 25289 },
	{ (Il2CppRGCTXDataType)3, 22848 },
	{ (Il2CppRGCTXDataType)3, 22849 },
	{ (Il2CppRGCTXDataType)2, 25290 },
	{ (Il2CppRGCTXDataType)3, 22850 },
	{ (Il2CppRGCTXDataType)3, 22851 },
	{ (Il2CppRGCTXDataType)2, 18328 },
	{ (Il2CppRGCTXDataType)2, 25291 },
	{ (Il2CppRGCTXDataType)3, 22852 },
	{ (Il2CppRGCTXDataType)3, 22853 },
	{ (Il2CppRGCTXDataType)3, 22854 },
	{ (Il2CppRGCTXDataType)3, 22855 },
	{ (Il2CppRGCTXDataType)3, 22856 },
	{ (Il2CppRGCTXDataType)3, 22857 },
	{ (Il2CppRGCTXDataType)3, 21730 },
	{ (Il2CppRGCTXDataType)2, 25292 },
	{ (Il2CppRGCTXDataType)3, 22858 },
	{ (Il2CppRGCTXDataType)3, 22859 },
	{ (Il2CppRGCTXDataType)2, 25293 },
	{ (Il2CppRGCTXDataType)3, 22860 },
	{ (Il2CppRGCTXDataType)3, 22861 },
	{ (Il2CppRGCTXDataType)3, 22862 },
	{ (Il2CppRGCTXDataType)3, 22863 },
	{ (Il2CppRGCTXDataType)2, 25294 },
	{ (Il2CppRGCTXDataType)2, 25295 },
	{ (Il2CppRGCTXDataType)2, 25296 },
	{ (Il2CppRGCTXDataType)3, 22864 },
	{ (Il2CppRGCTXDataType)3, 22865 },
	{ (Il2CppRGCTXDataType)2, 25297 },
	{ (Il2CppRGCTXDataType)3, 22866 },
	{ (Il2CppRGCTXDataType)2, 25298 },
	{ (Il2CppRGCTXDataType)3, 22867 },
	{ (Il2CppRGCTXDataType)3, 22868 },
	{ (Il2CppRGCTXDataType)3, 22869 },
	{ (Il2CppRGCTXDataType)2, 18378 },
	{ (Il2CppRGCTXDataType)3, 22870 },
	{ (Il2CppRGCTXDataType)2, 18386 },
	{ (Il2CppRGCTXDataType)3, 22871 },
	{ (Il2CppRGCTXDataType)2, 25299 },
	{ (Il2CppRGCTXDataType)2, 25300 },
	{ (Il2CppRGCTXDataType)3, 22872 },
	{ (Il2CppRGCTXDataType)3, 22873 },
	{ (Il2CppRGCTXDataType)3, 22874 },
	{ (Il2CppRGCTXDataType)3, 22875 },
	{ (Il2CppRGCTXDataType)3, 22876 },
	{ (Il2CppRGCTXDataType)3, 22877 },
	{ (Il2CppRGCTXDataType)2, 18402 },
	{ (Il2CppRGCTXDataType)2, 25301 },
	{ (Il2CppRGCTXDataType)3, 22878 },
	{ (Il2CppRGCTXDataType)3, 22879 },
	{ (Il2CppRGCTXDataType)2, 18406 },
	{ (Il2CppRGCTXDataType)3, 22880 },
	{ (Il2CppRGCTXDataType)2, 25302 },
	{ (Il2CppRGCTXDataType)2, 18416 },
	{ (Il2CppRGCTXDataType)2, 18414 },
	{ (Il2CppRGCTXDataType)2, 25303 },
	{ (Il2CppRGCTXDataType)3, 22881 },
	{ (Il2CppRGCTXDataType)2, 25304 },
	{ (Il2CppRGCTXDataType)3, 22882 },
	{ (Il2CppRGCTXDataType)3, 22883 },
	{ (Il2CppRGCTXDataType)3, 22884 },
	{ (Il2CppRGCTXDataType)2, 18420 },
	{ (Il2CppRGCTXDataType)3, 22885 },
	{ (Il2CppRGCTXDataType)3, 22886 },
	{ (Il2CppRGCTXDataType)2, 18423 },
	{ (Il2CppRGCTXDataType)3, 22887 },
	{ (Il2CppRGCTXDataType)1, 25305 },
	{ (Il2CppRGCTXDataType)2, 18422 },
	{ (Il2CppRGCTXDataType)3, 22888 },
	{ (Il2CppRGCTXDataType)1, 18422 },
	{ (Il2CppRGCTXDataType)1, 18420 },
	{ (Il2CppRGCTXDataType)2, 25306 },
	{ (Il2CppRGCTXDataType)2, 18422 },
	{ (Il2CppRGCTXDataType)3, 22889 },
	{ (Il2CppRGCTXDataType)3, 22890 },
	{ (Il2CppRGCTXDataType)3, 22891 },
	{ (Il2CppRGCTXDataType)2, 18421 },
	{ (Il2CppRGCTXDataType)3, 22892 },
	{ (Il2CppRGCTXDataType)2, 18434 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	122,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	43,
	s_rgctxIndices,
	202,
	s_rgctxValues,
	NULL,
};
